BEGIN;

CREATE TABLE tb_user (
	id INTEGER GENERATED ALWAYS AS IDENTITY,
	name TEXT UNIQUE NOT NULL, -- max. 32
	password TEXT NOT NULL, -- hashed
	pfp_hash BIGINT, -- do we need this?
	deleted_on TIMESTAMPTZ NULL DEFAULT NULL
);

CREATE TABLE tb_addon (
	id INTEGER UNIQUE GENERATED ALWAYS AS IDENTITY,
	name TEXT UNIQUE NOT NULL, -- max. 64
	synopsis TEXT NOT NULL,
	description TEXT NULL,
	--
	author TEXT NOT NULL,
	associated_author_id INTEGER NULL, -- tb_user.id
	--
	upload_date TIMESTAMPTZ DEFAULT NOW() NOT NULL,
	last_updated TIMESTAMPTZ DEFAULT NOW() NOT NULL,
	--
	edit_code TEXT NOT NULL -- hashed
);

CREATE TABLE tb_review (
	id INTEGER GENERATED ALWAYS AS IDENTITY,
	addon_id INTEGER NOT NULL,
	parent_id INTEGER NULL, -- if non-empty, then
	                        -- this is a reply that can't have ratings
	--
	reviewer TEXT NOT NULL,
	associated_reviewer_id INTEGER NULL, -- tb_user.id
	--
	rating INTEGER NULL,
	review_date TIMESTAMPTZ DEFAULT NOW() NOT NULL,
	content TEXT NOT NULL
);

-- currently not used
CREATE TABLE tb_addon_gallery_image (
	addon_id INTEGER NOT NULL, -- tb_addon.id
	name TEXT NOT NULL,
	description TEXT NULL,
	--
	image_hash BIGINT NOT NULL,
	--
	CHECK(LENGTH(name) <= 128)
);

CREATE TYPE addon_type
AS ENUM (
	'wad',
	'zwad',
	'pk3'
);

CREATE TABLE tb_addon_version (
	hash BIGINT PRIMARY KEY,
	addon_id INTEGER NOT NULL , -- tb_addon.id
	-- what the file is actually called in the storage medium
	physical_file_name TEXT NOT NULL,
	-- what the file will be downloaded as
	virtual_file_name TEXT NOT NULL,
	file_type addon_type NOT NULL,
	version_string TEXT NOT NULL,
	file_size INTEGER DEFAULT 0 NOT NULL,
	release_date TIMESTAMPTZ DEFAULT NOW() NOT NULL,
	uploader_id INTEGER NULL, -- tb_user.id
							  -- for i.e. archival purposes
							  -- when the author is not around but people
							  -- have found other versions of an add-on. 
	release_notes TEXT DEFAULT '' NOT NULL, -- wanted to use NULL, but quirks of
											-- a type-safe thing won't let me
	--
	flats INTEGER DEFAULT 0 NOT NULL,
	texs INTEGER DEFAULT 0 NOT NULL,
	sprites INTEGER DEFAULT 0 NOT NULL,
	socs INTEGER DEFAULT 0 NOT NULL,
	luas INTEGER DEFAULT 0 NOT NULL,
	maps INTEGER DEFAULT 0 NOT NULL,
	gfxs INTEGER DEFAULT 0 NOT NULL,
	snds INTEGER DEFAULT 0 NOT NULL,
	mscs INTEGER DEFAULT 0 NOT NULL,
	readme TEXT DEFAULT '' NOT NULL
);

-- levels from the latest version of an add-on
CREATE TABLE tb_addon_latest_levels (
	addon_id INTEGER NOT NULL,
	level_number INTEGER NOT NULL, -- internal level number
	level_name TEXT NOT NULL,
	act INTEGER NOT NULL,
	--
	FOREIGN KEY (addon_id) REFERENCES tb_addon (id)
);

CREATE TABLE tb_tag (
	id INTEGER UNIQUE GENERATED ALWAYS AS IDENTITY,
	tag TEXT NOT NULL
	--
	CHECK(LENGTH(tag) <= 32)
);

CREATE TABLE tb_addon_tags (
	addon_id INTEGER NOT NULL,
	tag_id INTEGER NOT NULL,
	--
	FOREIGN KEY (addon_id) REFERENCES tb_addon (id),
	FOREIGN KEY (tag_id) REFERENCES tb_tag (id)
);

CREATE TABLE tb_download_counter (
	addon_id INTEGER,
	version_hash BIGINT,
	ip_address TEXT, -- ?
	date TIMESTAMPTZ DEFAULT NOW() NOT NULL
);

COMMIT;
