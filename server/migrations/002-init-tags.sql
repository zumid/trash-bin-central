BEGIN;

INSERT INTO tb_tag
(
	tag
)
VALUES
	-- game type
	('srb2'),
	('kart'),
	('drrr'),

	-- game version
	('srb2-2.2'),
	('srb2-2.1'),
	('srb2-2.0'),
	('srb2-1.09'),
	('srb2-1.07'),
	('srb2-1.01'),
	('srb2-demo4'),
	('srb2-demo3'),
	('srb2-demo2'),
	('srb2-demo1'),
	('srb2k-1.0'),
	('drrr-2.0'),
	
	-- base tags
	('single'),
	('race'),
	('battle'),
	('match'),
	('ctf'),
	('characters'),
	('lua'),
	('tutorial'),
	('follower'),
	('persona')
;

COMMIT;
