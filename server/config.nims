const isRelease = defined(release) or defined(danger)
const onlyTodos = true

when onlyTodos:
  switch("warning", "all:off")
  switch("warning", "User:on")

when not defined(nimsuggest):
  switch("define", "threadsafe")
  switch("errorMax", "1")

  switch("define", "chroniclesLineNumbers=true")
  switch("define", "chroniclesSinks=textlines[nocolors,stdout]")

  switch("mm", "atomicArc") # will this segfault again?
  #switch("define", "useMalloc")

  # the lack of this line is probably what's ballooning compile
  # time even further.
  switch("define", "chroniclesDisabledTopics=guildenstern")

  if isRelease:
    switch("define", "noSignalHandler")
    switch("opt", "speed")
    switch("assertions", "off")
    switch("define", "chroniclesLogLevel=DEBUG")
  else:
    # logging options
    switch("define", "chroniclesLogLevel=TRACE")
    switch("define", "chroniclesTimestamps=None")
    switch("panics", "on")

  # enable Nim source lines in backtrace output
  when not defined(clientSide):
    switch("debugger", "native")
    switch("passC", "-ggdb")

  when defined(musl):
    # musl shenanigans!
    switch("gcc.exe", "/home/zumi/.local/bin/musl-gcc")
    switch("gcc.linkerexe", "/home/zumi/.local/bin/musl-gcc")
    switch("passL", "-static")
    {.
      warning:
        "Requires std/private/threadtypes.nim to be patched with type SysThread* = object"
    .}

  # use libbacktrace
  when isRelease:
    switch("stacktrace", "off")
    when not defined(clientSide):
      switch("define", "nimStackTraceOverride")
      switch("import", "libbacktrace")
      switch("gcc.options.debug", "-g1")
      switch("warning", "ResultShadowed:off")
      switch("passC", "-fno-inline -fno-optimize-sibling-calls -flto")
      switch("passL", "-flto")
