import dom

type
  ProgressEvent* {.importjs.} = ref object of Event
    lengthComputable: bool
    loaded: uint64
    total: uint64

  ReadyState* {.size: sizeof(cint).} = enum
    Unsent = 0
    Opened = 1
    HeadersReceived = 2
    Loading = 3
    Done = 4

  XmlHttpRequest* {.importjs.} = ref object
    readyState*: ReadyState
    status*: int
    responseText*: cstring
    statusText*: cstring

  HttpMethods* = enum
    Get = "GET"
    Post = "POST"
    Put = "PUT"
    Delete = "DELETE"

proc newXhr*(): XmlHttpRequest {.importjs: "new XMLHttpRequest(@)".}

proc onLoad*(
  r: XmlHttpRequest, callback: proc(e: ProgressEvent)
) {.importjs: "#.onload = #".}

proc onReadyState*(
  r: XmlHttpRequest, callback: proc(e: Event)
) {.importjs: "#.onreadystatechange = #".}

proc onError*(r: XmlHttpRequest, callback: proc(e: Event)) {.importjs: "#.onerror = #".}

proc open*(
  r: XmlHttpRequest, mtd: cstring, url: cstring, async: bool = true
) {.importjs: "#.open(@)".}

template open*(r: XmlHttpRequest, mtd: HttpMethods, url: cstring, async: bool = true) =
  r.open(cstring($mtd), url, async)

proc send*(r: XmlHttpRequest) {.importjs: "#.send()".}
