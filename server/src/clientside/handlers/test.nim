import dom
import std/jsconsole
import ./helper/xhr
import ../config

from nimja/parser import compileTemplateFile

proc testThis*(lol: cstring): cstring =
  compileTemplateFile("test_client.html", baseDir = config.HtmlTemplateDir)

let x = newXhr()

x.onLoad:
  proc(e: xhr.ProgressEvent) =
    console.log("Nice")
    console.log(testThis("something"))

x.onReadyState:
  proc(e: Event) =
    if x.readyState == Done:
      if x.status == 200:
        console.log("Done")
      else:
        console.error("Error!")

x.open(xhr.Get, "/test")

x.send()

document.getElementById("x").addEventListener("submit"):
  proc(e: Event) =
    discard
