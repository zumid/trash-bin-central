import std/dom
import std/jsconsole
import ../helper/xhr

proc addReviewHandlers*(): void =
  let
    addReviewForm = block:
      let x = document.querySelector("#add-review-form")
      if x == nil:
        return
      else:
        x
    addReviewButton = block:
      let x = addReviewForm.querySelector("input[type='submit']")
      if x == nil:
        return
      else:
        x
  addReviewButton.addEventListener("click"):
    proc(e: Event) =
      e.preventDefault()
      let x = newXhr()
      x.onReadyState:
        proc(e: Event) =
          if x.readyState != Done:
            return
          if x.status != 200:
            console.error("error fetching request")
            return
          let
            p = newDomParser()
            parsed = p.parseFromString(x.responseText, "text/html".cstring)
            newReview = block:
              let i = parsed.body.firstChild
              if i == nil:
                return
              i
          if addReviewForm.parentNode == nil:
            return
          addReviewForm.parentNode.insertBefore(newReview, addReviewForm.nextSibling)
      x.open(Get, "/api/html/latestReview")
      x.send()

when isMainModule:
  addReviewHandlers()
