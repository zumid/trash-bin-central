##[

Compile-time server configuration. This is **not** to be confused with
``../config.nims``, which affects *how* the server is built.

The ``*EnvString`` constants affect runtime.

]##

# Partially-qualified imports
from std/times import nil
from nimja/parser import getScriptDir, `/`, parentDir

# environment variables
const
  PortNumberEnvString* = "USE_PORT"
  ListenAddressEnvString* = "LISTEN_ADDR"
  NumThreadsEnvString* = "NUM_THREADS"
  BacktracesEnvString* = "GET_BACKTRACES"
  DbConnEnvString* = "USE_CONNSTRING"
  SqDbConnEnvString* = "USE_SQLITE_CONNSTRING"
  ServerUrlEnvString* = "SERVER_BASEURL"
  DomainEnvString* = "DOMAIN"

# configurables
const
  DefaultPortNumber* = 8198
  (TrashBinCentralVersionMajor*, TrashBinCentralVersionMinor*) = (0, 2)
  TrashBinCentralVersionSuffix* = block:
    let (`out`, ecode) =
      gorgeEx("git show -s --format=\"%h-%cd\" --date=format:\"%Y.%m.%d\"")
    if ecode == 0: `out` else: ""
  TrashBinCentralGitLink* = "https://gitlab.com/zumid/trash-bin-central"
    ## This link is shown on the footer.
  HtmlTemplateDir* = getScriptDir() / "html_templates"
    ## Directory to look for Nimja templates.
  SqlQueryDir* = getScriptDir() / "queries"
  StdHumanFormat* = times.initTimeFormat("ddd'. 'd' 'MMM' 'UUUU")
    ## Format used to display dates throughout the website.
  MaxUploadFileSize* =
    128 * (
      1024 * 1024 #[ MiB ]#
    ) ## File size in bytes.
  SessionCleanupInterval* = times.initDuration(seconds = 30)
    ## How often to clean up session IDs
  DefaultSessionExpiry* = times.minutes(5)
    ## A session expires after this amount of time has passed, used to 
    ## set the expiration column.
  UploadBucketDir* = "_uploads" ## Where to place uploads, for disk implementations
  AssetsDir* = "_assets"
  PreviewsDir* = "_previews"

  # limits
  AddonNameLimit* = 48

# cookie names
const SessionCookieName* = "tb_session"

# table names
const
  SessionTableName* = "tb_session"
  FlashMessageTableName* = "tb_flash"
  UserTableName* = "tb_user"
  AddonTableName* = "tb_addon"
  AddonVersionTableName* = "tb_addon_version"
  ReviewTableName* = "tb_review"
  AddonTagAssocTableName* = "tb_addon_tags"
  TagTableName* = "tb_tag"
  AddonLevelTableName* = "tb_addon_latest_levels"
  DownloadCounterTableName* = "tb_download_counter"
  AcsrfTableName* = "tb_acsrf_tokens"

# connection strings
const
  DefaultPostgresConnString* =
    "postgresql://tbdb:tbdb@trashbin:1999/trashbin?application_name=trash-bin-central"
    ## Used for main database. Prefer URLs,
    ## see https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING
  DefaultSqliteConnString* = ":memory:"
    ## Used for session database, often just a file name.

# generated
const TrashBinCentralVersion* =
  $TrashBinCentralVersionMajor & "." & $TrashBinCentralVersionMinor & (
    if TrashBinCentralVersionSuffix == "":
      ""
    else:
      "-" & TrashBinCentralVersionSuffix
  ) ## Generated from `TrashBinCentralVersionMajor`_ and `TrashBinCentralVersionMinor`_.

# things that don't need changing
const StdHttpFormat* = times.initTimeFormat("ddd' 'd' 'MMM' 'UUUU' 'HH:mm:ss' GMT'")
  ## The HTTP time format. This shouldn't need any changing.
const PostgresIsoFormat* = times.initTimeFormat("YYYY-MM-dd HH:mm:sszz")

func PostgresFormatTime*(column: string): string {.compiletime.} =
  ## Function to ensure uniform time format representation between the server
  ## and Postgres. This should match `PostgresIsoFormat`_.
  return "to_char(" & column & ", 'YYYY-MM-DD HH24:MI:SSOF')"

func SqliteFormatTime*(column: string): string {.compiletime.} =
  ## Function to ensure uniform time format representation between the server
  ## and SQLite. This should also match `PostgresIsoFormat`_ as best as
  ## possible.
  {.warning: "TODO: Timezone hardcoded here...".}
  return "STRFTIME('%Y-%m-%d %H:%M:%S+07', " & column & ")"
