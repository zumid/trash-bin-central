##
## Path parsing
##
## References:
##   - https://datatracker.ietf.org/doc/html/rfc3986
##   - https://www.fpcomplete.com/blog/pains-path-parsing/
##
## This does not parse a complete URI, but just the part on and after the /,
## which is what HTTP requests will contain.
## 
## Therefore, this library will only concern itself with paths and URL-encoded
## parameters. Fragments are out of scope, as it is the client's (e.g. browser's)
## responsibility.

import std/strutils
import std/strtabs
import ../log
import ../resutils

from ../helper/strtab_to_list import nil

type
  PathParts* = seq[string]
  Path* = object
    path*: PathParts
    query*: StringTableRef

const
  permittedCharas = {'A' .. 'Z', 'a' .. 'z', '0' .. '9', '-', '.', '_', '~', '+', '*'}
  hexCharas = {'A' .. 'F', 'a' .. 'f', '0' .. '9'}

{.push gcsafe, raises: [].}

proc parseParamsTo*(
    params: string, queries: var StringTableRef, charaDiagPosition: var int
): Failable =
  type ParseKind = enum
    Key
    Value

  var
    isParsingHex = false
    whichBuffer = Key
    keyBuffer = newStringOfCap(32)
    valBuffer = newStringOfCap(32)
    hexBuffer = newStringOfCap(2)
  # ?test=123&foo=bar -> {"test": "123", "foo": "bar"}
  for character in params[0 ..^ 1]:
    charaDiagPosition += 1
    case character
    of permittedCharas: # characters
      if not isParsingHex: # fill either the key or value buffer
        # application/x-www-form-urlencoded :/
        let appendedCharacter =
          if character == '+':
            # log.trace("encountered + character, automatically changing to space")
            ' '
          else:
            character
        case whichBuffer
        of Value:
          valBuffer.add(appendedCharacter)
        of Key:
          keyBuffer.add(appendedCharacter)
      else:
        if len(hexBuffer) < 2:
          case character
          of hexCharas: # in hex range
            hexBuffer.add(character)
            if len(hexBuffer) == 2: # done parsing hex
              isParsingHex = false
              let i =
                # integer value of parsed hex number
                try:
                  hexBuffer.parseHexInt()
                except ValueError as e:
                  return err("at position " & $charaDiagPosition & ": " & e.msg)
              # insert value into string
              case whichBuffer
              of Value:
                valBuffer.add chr(i)
              of Key:
                keyBuffer.add chr(i)
              hexBuffer.setLen(0)
          else: # abandon current attempt
            isParsingHex = false
            hexBuffer.setLen(0)
        else: # unreachable point
          return err(
            "parse hex in param at position " & $charaDiagPosition &
              ": unreachable point"
          )
    of '&', '?': # marks a new parameter
      if len(keyBuffer) > 0 and len(valBuffer) > 0: # insert previous param to table
        queries[keyBuffer] = valBuffer
      # reset tracking variables
      isParsingHex = false
      whichBuffer = Key
      keyBuffer.setLen(0)
      valBuffer.setLen(0)
      hexBuffer.setLen(0)
    of '=': # marks a value
      whichBuffer = Value
      valBuffer.setLen(0)
    of '%': # marks the beginning of a hex-escaped character
      isParsingHex = true
      hexBuffer.setLen(0)
    else: # unknown character
      return err($character & " is not allowed at position " & $charaDiagPosition)
  # add the last straggler if any
  if len(keyBuffer) > 0 and len(valBuffer) > 0:
    queries[keyBuffer] = valBuffer
  return ok()

proc parseParamsTo*(params: string, queries: var StringTableRef): Failable =
  var position = 0
  parseParamsTo(params, queries, position)

proc parseParamsFrom*(params: string): Res[StringTableRef] =
  var queries = newStringTable()
  if (let i = parseParamsTo(params, queries); not i.isOk()):
    return i.convertErr()
  # log.info("queries", q = strtab_to_list.asList(queries))
  ok(queries)

proc parse*(url: string, alsoParseParams: bool = true): Res[Path] {.raises: [].} =
  var
    retval: Path
    parsedPath: seq[string] = @[]
    partBuffer = newStringOfCap(32)
    hexBuffer = newStringOfCap(2)
    parseHexMode = false
    hasParams = false

  if len(url) < 1:
    return "path is blank".err()

  if url[0] notIn ['/', '\\']:
    return "path must begin with slash (/ or \\)".err()

  retval.query = newStringTable()

  # parse/part/parts -> ["parse", "path", "parts"]
  var position = 0
  for character in url[1 ..^ 1]:
    position += 1
    case character
    of permittedCharas:
      if not parseHexMode:
        partBuffer.add(character)
      else:
        if len(hexBuffer) < 2:
          case character
          of hexCharas:
            hexBuffer.add(character)
            if len(hexBuffer) == 2:
              parseHexMode = false
              let i =
                try:
                  hexBuffer.parseHexInt()
                except ValueError as e:
                  return ("at position " & $position & ": " & e.msg).err()
              partBuffer.add chr(i)
              hexBuffer.setLen(0)
          else:
            # abandon current attempt
            parseHexMode = false
            hexBuffer.setLen(0)
        else:
          return ("parse hex at position " & $position & ": unreachable point").err()
    of '/', '\\':
      if len(partBuffer) > 0:
        parsedPath.add(partBuffer)
        partBuffer.setLen(0)
    of '%':
      parseHexMode = true
      hexBuffer.setLen(0)
    of '?':
      if len(partBuffer) > 0:
        parsedPath.add(partBuffer)
        partBuffer.setLen(0)
      hasParams = true
      break
    else:
      return ($character & " is not allowed at position " & $position).err()

  # straggler
  if len(partBuffer) > 0:
    parsedPath.add(partBuffer)

  retval.path = PathParts(parsedPath)

  if hasParams and alsoParseParams:
    if (let i = url[position ..^ 1].parseParamsTo(retval.query, position); not i.isOk()):
      return i.convertErr()

  return ok(retval)

proc pop*(s: var PathParts): Opt[string] =
  if len(s) > 0:
    let val = s[0]
    s = PathParts(s[1 ..^ 1])
    return Opt[string].some(val)
  return Opt.none(string)

proc urlFromPath*(s: Path): string =
  for i in 0 ..< len(s.path):
    result.add(s.path[i])
    if i != len(s.path) - 1:
      result.add('/')
  if len(s.query) > 0:
    var queriesSoFar = 0
    result.add('?')
    for k, v in pairs(s.query):
      {.warning: "TODO: URLize k and v".}
      result.add(k)
      result.add('=')
      result.add(v)
      if queriesSoFar < len(s.query) - 1:
        result.add('&')
      inc(queriesSoFar)
