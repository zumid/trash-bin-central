##[

This does not support MULTIPLE ranges yet!

How many download managers even use this kinda thing?

]##

import ../resutils
from std/strutils import nil

type ByteRange* = tuple[partStart: Opt[Natural], partEnd: Opt[Natural]]

proc rangeFromRequest*(req: string): Res[ByteRange] {.raises: [].} =
  let r = strutils.strip(req)

  let kv = strutils.split(r, '=', 2)
  if len(kv) < 2:
    return err("malformed range request")
  if strutils.strip(kv[0]) != "bytes":
    return err("supported range is only \'bytes\'")

  var startNumStr: string
  var endNumStr: string

  type WhichNumber = enum
    Start
    End

  var whereAmI = Start

  for i in kv[1]:
    case i
    of '0' .. '9':
      case whereAmI
      of Start:
        startNumStr.add i
      of End:
        endNumStr.add i
    of '-':
      whereAmI = End
    of ' ', '\t', '\n':
      discard
    else:
      if whereAmI == End:
        break

  let
    partStart =
      try:
        # Gotta love type games :p
        Opt[Natural].some(Natural(strutils.parseInt(startNumStr)))
      except ValueError:
        Opt.none(Natural)
    partEnd =
      try:
        Opt[Natural].some(Natural(strutils.parseInt(endNumStr)))
      except ValueError:
        Opt.none(Natural)

  # verify the ranges
  if partStart.isSome() and partEnd.isSome():
    if partEnd.unwrap() < partStart.unwrap():
      return err("end range cannot be less than start range")
  elif (not partStart.isSome()) and (not partEnd.isSome()):
    return err("start and end cannot be empty")

  return ok((partStart: partStart, partEnd: partEnd))
