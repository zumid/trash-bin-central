import std/tables
import std/strutils

from ../datatypes as dt import nil
from ./levelnum import decodeLevelNumber

func parseSoc*(s: string, l: var dt.AddonAnalysisInfo): void =
  var levelNumber: int
  for line in s.split('\n'):
    let lineStripped = line.strip()
    if len(lineStripped) == 0:
      # empty
      continue
    elif lineStripped[0] == '#':
      # comment line
      continue

    let lineIsConfig = lineStripped.split('=', 2)
    case len(lineIsConfig)
    of 2:
      # key-value data
      let k = lineIsConfig[0].strip().toLowerAscii()
      let v = lineIsConfig[1].strip()
      case k
      of "typeoflevel":
        for e in v.split(','):
          let element = e.strip().toLowerAscii()
          case element
          of "singleplayer", "solo", "sp", "co-op", "coop", "competition":
            l.features = l.features + {dt.SinglePlayer}
          of "race":
            l.features = l.features + {dt.Race}
          of "match":
            l.features = l.features + {dt.Match}
          of "ctf":
            l.features = l.features + {dt.Ctf}
          of "battle":
            l.features = l.features + {dt.Battle}
          of "tutorial":
            l.features = l.features + {dt.Tutorial}
      of "levelname":
        try:
          l.levels[levelNumber].name = v
        except CatchableError:
          discard
      of "act":
        try:
          l.levels[levelNumber].act = v.parseInt()
        except CatchableError:
          discard
      else:
        discard
    of 1:
      # single word
      let k = lineIsConfig[0].strip().toLowerAscii()
      if k.startsWith("level"):
        let levelLine = k.split("level", 2)
        case len(levelLine)
        of 2:
          levelNumber = levelLine[1].strip().align(2, '0').decodeLevelNumber()
          # reserve a level slot
          if levelNumber > -1:
            try:
              l.levels[levelNumber] = dt.LevelHeading()
            except CatchableError:
              discard
          else:
            when defined(traceResults):
              log.trace("tripped at line", line = lineStripped)
        else:
          discard
      else:
        case k
        of "character":
          l.features = l.features + {dt.Character}
        of "follower":
          l.features = l.features + {dt.Follower}
        else:
          discard
    else:
      discard
