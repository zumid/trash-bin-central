##[

An extremely basic BBcode parser

]##

import ../resutils

template pseudoAssert(cond: bool, whatToSay: string): untyped =
  if not cond:
    return err(whatToSay)

proc bbCode2HtmlResult*(text: string): Res[string] =
  var rendered: string
  var inBracket = false
  var isEndCommand = false
  var commandBuffer = ""
  var bracketStack: seq[string] = @[]
  var numCharactersEncountered = 0
  var blankLinesEncountered = 0
  const commandCharaRanges = {'a' .. 'z'}
  var charaIndex = -1
  while true:
    inc(charaIndex)
    if charaIndex >= len(text):
      break

    let curChara = text[charaIndex]
    if curChara == '\n':
      inc(blankLinesEncountered)
      if blankLinesEncountered > 1:
        rendered.add "<br>"
    else:
      blankLinesEncountered = 0
    case curChara
    of '[':
      # lookahead one character
      let lookaheadIndex = numCharactersEncountered + 1
      if lookaheadIndex > (len(text) - 1):
        # there is no other character!
        rendered.add '['
      else:
        if text[lookaheadIndex] == '[':
          # this is an escaped [
          rendered.add '['
          inc(charaIndex)
          inc(numCharactersEncountered)
        elif text[lookaheadIndex] in commandCharaRanges:
          # we have a command
          inBracket = true
        elif text[lookaheadIndex] == '/':
          # we have an ending command
          inBracket = true
          isEndCommand = true
    of ']':
      if inBracket:
        if isEndCommand:
          isEndCommand = false
          let commandTerm = bracketStack.pop()
          pseudoAssert(
            commandTerm == commandBuffer,
            "expected tag '" & commandTerm & "' to be closed, encountered '" &
              commandBuffer & "' instead",
          )
          rendered.add(
            case commandTerm
            of "center": "</div>"
            of "big": "</span>"
            of "ul": "</ul>"
            of "li": "</li>"
            of "b": "</b>"
            of "i": "</i>"
            of "u": "</u>"
            else: ""
          )
        else:
          bracketStack.add commandBuffer
          rendered.add(
            case commandBuffer
            of "center": "<div class=\"bbcode-center\">"
            of "big": "<span class=\"bbcode-big\">"
            of "ul": "<ul class=\"proper-list\">"
            of "li": "<li>"
            of "b": "<b>"
            of "i": "<i>"
            of "u": "<u>"
            else: ""
          )
        commandBuffer.setLen(0)
        inBracket = false
      else:
        # lookahead one character
        let lookaheadIndex = numCharactersEncountered + 1
        if lookaheadIndex > (len(text) - 1):
          # there is no other character!
          if inBracket:
            # clean up
            inBracket = false
          else:
            rendered.add ']'
        else:
          if text[lookaheadIndex] == ']':
            # this is an escaped ]
            rendered.add ']'
            inc(charaIndex)
            inc(numCharactersEncountered)
    of '<':
      rendered.add "&lt;"
    of '>':
      rendered.add "&gt;"
    of '&':
      rendered.add "&amp;"
    else:
      if inBracket:
        if curChara in commandCharaRanges:
          commandBuffer.add curChara
      else:
        rendered.add curChara
    inc(numCharactersEncountered)

  pseudoAssert(not inBracket, "unclosed bracket")
  pseudoAssert(not isEndCommand, "closing tag possibly not terminated yet")
  pseudoAssert(len(bracketStack) < 1, "dangling tag stack")
  pseudoAssert(
    numCharactersEncountered == len(text),
    "either overcount or not all text has been processed yet",
  )
  return ok(rendered)
