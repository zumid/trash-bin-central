##
## Cookie parsing
## 
## References:
##  - https://datatracker.ietf.org/doc/html/rfc6265

import std/strutils
import std/strtabs
import ../resutils

export strtabs

const
  ValidTokens =
    # <any CHAR except CTLs or separators>
    {chr(0) .. chr(127)} - # CHAR
    {chr(0) .. chr(31)} - # CTL (1)
    {chr(127)} - # CTL (2)
    {
      '(', ')', '<', '>', '@', ',', ';', ':', '\\', '"', '/', '[', ']', '?', '=', '{',
      '}', ' ', '\x09',
    } # separators
  ValidCookieOctets =
    {'\x21'} + {'\x23' .. '\x2b'} + {'\x2d' .. '\x3a'} + {'\x3c' .. '\x5b'} +
    {'\x5d' .. '\x7e'}
  WhiteSpace = {' ', '\x09', '\x0a', '\x0d'}
  HexCharas = {'A' .. 'F', 'a' .. 'f', '0' .. '9'}

proc parse*(s: string): Res[StringTableRef] =
  ## This parses ONLY cookie-string, i.e. after "Cookie: ".
  type ParseKind = enum
    Key
    Value

  var
    keyBuffer = newStringOfCap(32)
    valBuffer = newStringOfCap(32)
    hexBuffer = newStringOfCap(32)
    whichBuffer = Key
    res = newStringTable()
    position = -1
    skipWs = false
    inHexMode = false
  var quoteStartPosition = Opt.none(int)
  for character in s:
    position += 1
    if inHexMode:
      case character
      of HexCharas:
        hexBuffer.add(character)
        if len(hexBuffer) == 2:
          let i =
            try:
              hexBuffer.parseHexInt()
            except ValueError as e:
              return err("at position " & $position & ": " & e.msg)
          hexBuffer.setLen(0)
          inHexMode = false
          case whichBuffer
          of Key:
            return err("unexpected hexchara in key")
          of Value:
            valBuffer.add(chr(i))
        else:
          continue
      else:
        return err("invalid hex chara " & $character & " at position " & $position)
    else:
      case character
      of '=':
        if len(keyBuffer) < 1:
          return err("no key defined near position " & $position)
        whichBuffer = Value
      of ';':
        if len(valBuffer) < 1:
          return err("empty value for key " & keyBuffer & " near position " & $position)
        res[keyBuffer] = valBuffer
        keyBuffer.setLen(0)
        valBuffer.setLen(0)
        whichBuffer = Key
        skipWs = true
      of WhiteSpace:
        if skipWs:
          continue
        else:
          if whichBuffer == Value and quoteStartPosition.isSome():
            valBuffer.add(character)
          else:
            return
              err("whitespace at position " & $position & " while not a quoted value")
      else:
        skipWs = false
        case whichBuffer
        of Key:
          case character
          of ValidTokens:
            keyBuffer.add(character)
          else:
            return
              err("invalid key character '" & $character & "' at position " & $position)
        of Value:
          case character
          of ValidCookieOctets:
            if character == '%':
              inHexMode = true
              continue
            else:
              valBuffer.add(character)
          of '"':
            quoteStartPosition =
              if quoteStartPosition.isSome():
                Opt.none(int)
              else:
                Opt[int].some(position)
          else:
            return err(
              "invalid value character '" & $character & "' at position " & $position
            )
  if quoteStartPosition.isSome():
    let pos = quoteStartPosition.unwrap()
    return err("unmatched quote, started at " & $pos)
  # straggler
  if len(keyBuffer) > 0 and len(valBuffer) > 0:
    res[keyBuffer] = valBuffer
  return ok(res)

proc cookieExists*(headers: StringTableRef, cookieName: string): Res[Opt[string]] =
  let cookies = block:
    let cookieString =
      try:
        headers["cookie"]
      except KeyError:
        # There really is nothing here, so just return
        return ok(Opt.none(string))
    parse(cookieString)

  if not cookies.isOk():
    return err("cannot get cookie: " & cookies.unwrapErr())

  try:
    ok(Opt[string].some(cookies.unwrap()[cookieName]))
  except KeyError:
    ok(Opt.none(string))
