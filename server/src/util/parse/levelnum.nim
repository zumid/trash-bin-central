from std/strutils import nil

func decodeLevelNumber*(n: string): int =
  const
    AsciiNumbers = {'0' .. '9'}
    AsciiLetters = {'a' .. 'z'}
    ValidRange = AsciiLetters + AsciiNumbers

  # "Level 1000"
  if len(n) == 4:
    # every one of them should be numbers
    if (n[0] notIn AsciiNumbers) or (n[1] notIn AsciiNumbers) or
        (n[2] notIn AsciiNumbers) or (n[3] notIn AsciiNumbers):
      return -1
    let
      d1 = ord(n[0]) - ord('0')
      d2 = ord(n[1]) - ord('0')
      d3 = ord(n[2]) - ord('0')
      d4 = ord(n[3]) - ord('0')
    return (1000 * d1) + (100 * d2) + (10 * d3) + d4

  # "Level 100"
  if len(n) == 3:
    # every one of them should be numbers
    if (n[0] notIn AsciiNumbers) or (n[1] notIn AsciiNumbers) or
        (n[2] notIn AsciiNumbers):
      return -1
    let
      d1 = ord(n[0]) - ord('0')
      d2 = ord(n[1]) - ord('0')
      d3 = ord(n[2]) - ord('0')
    return (100 * d1) + (10 * d2) + d3

  # "Level A0"
  if len(n) != 2:
    return -1

  let
    xNorm = strutils.toLowerAscii(n[0])
    yNorm = strutils.toLowerAscii(n[1])

  if (xNorm notIn ValidRange) or (yNorm notIn ValidRange):
    # invalid
    return -1

  if (xNorm in AsciiNumbers):
    if (yNorm notIn AsciiNumbers):
      # invalid combo
      return -1
    # both are ascii numbers
    let
      x = ord(xNorm) - ord('0')
      y = ord(yNorm) - ord('0')
    return (10 * x) + y
  else:
    # extended map number
    let
      x = ord(xNorm) - ord('a')
      y =
        if yNorm in AsciiNumbers:
          ord(yNorm) - ord('0')
        else:
          ord(yNorm) - ord('a') + 10
    return 100 + (x * 36) + y
