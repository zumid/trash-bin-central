import std/tables
import std/strutils
import std/strformat
import std/sets
import ./resutils

from std/os import nil
from db_connector/db_postgres as psql import nil
from db_connector/db_sqlite as sqlite import nil
from ../config import nil
from ./datatypes as dt import nil

export psql
export sqlite

# std/sets
export contains, incl

{.push gcsafe, raises: [].}

template makeQuery*(fileName: static string): string =
  staticRead(config.SqlQueryDir & "/" & fileName).fmt()

template prepareQuery*(
    s: ref dt.Database,
    queryName: static string,
    fileName: static string,
    numParameters: static int,
): psql.SqlPrepared {.dirty.} =
  if queryName in s.prepStmts:
    psql.SqlPrepared(queryName)
  else:
    let prepStmt =
      psql.prepare(s.conn, queryName, psql.SqlQuery(makeQuery(filename)), numParameters)
    s.prepStmts.incl(queryName)
    prepStmt

proc dbDoubleQuote(s: string): string =
  ## DB quotes the string.
  result = "\""
  for c in items(s):
    case c
    of '"':
      add(result, "\"\"")
    of '\0':
      add(result, "\\0")
    else:
      add(result, c)
  add(result, '"')

template unprepareQuery*(s: ref dt.Database, queryName: static string) =
  ## Sometimes, prepared queries can have dynamic arguments that DO change
  ## over the course of the program. This just ensures they are refreshed.
  ##
  ## I would change them to .exec but h
  if queryName in s.prepStmts:
    psql.exec(s.conn, psql.SqlQuery("DEALLOCATE " & dbDoubleQuote(queryName)))
    s.prepStmts.excl(queryName)

template prepareQuery*(fileName: static string): sqlite.SqlQuery {.dirty.} =
  sqlite.SqlQuery(makeQuery(filename))

proc getPgConnString*(): string =
  result = config.DefaultPostgresConnString
  # set connstring
  if (let envConnString = os.getEnv(config.DbConnEnvString); len(envConnString) > 0):
    result = envConnString

proc getSqConnString*(): string =
  result = config.DefaultSqliteConnString
  # set connstring
  if (let envConnString = os.getEnv(config.SqDbConnEnvString); len(envConnString) > 0):
    result = envConnString

# Connect functions

proc connectToPgDb*(): Res[ref dt.Database] =
  var newDb = new(dt.Database)
  try:
    newDb.conn = psql.open("", "", "", getPgConnString())
    return ok(newDb)
  except psql.DbError as e:
    return err(e.message())

proc connectToSqDb*(): Res[sqlite.DbConn] =
  try:
    return ok(sqlite.open(getSqConnString(), "", "", ""))
  except sqlite.DbError as e:
    return err(e.message())

# Transaction management
proc begin*(s: ref dt.Database): Failable =
  try:
    psql.exec(s.conn, psql.SqlQuery("BEGIN"))
    ok()
  except psql.DbError as e:
    return err(e.message())

proc commit*(s: ref dt.Database): Failable =
  try:
    psql.exec(s.conn, psql.SqlQuery("COMMIT"))
    ok()
  except psql.DbError as e:
    return err(e.message())

proc rollback*(s: ref dt.Database): Failable =
  try:
    psql.exec(s.conn, psql.SqlQuery("ROLLBACK"))
    ok()
  except psql.DbError as e:
    return err(e.message())

# Close DB without error handling

proc justCloseTheDb*(s: sqlite.DbConn): void =
  try:
    sqlite.close(s)
  except sqlite.DbError:
    discard

# Connect or error

template connectToPgDbOrErr*(contextMsg: string): dt.Database =
  ## Should be used inside of a Res[T] function, as it
  ## `returns` something.
  # None better illustrating the difference between an
  # implicit return and an explicit return... if this
  # template is used inside of a `let`...
  match connectToPgDb():
    Ok(v):
      # This sets the `let` variable to this value.
      v
      # And ok yeah I can't just use `defer` here, because
      # the defer will run as soon as this template
      # "exits", which will render the DbConn value
      # useless.
    Err(e):
      # This exits the entire function!!
      return err(contextMsg & ": " & e)

template connectToPgDbOrErr*(retval: untyped): dt.Database =
  match connectToPgDb():
    Ok(v):
      v
    Err:
      return err(retval)

template connectToSqDbOrErr*(contextMsg: string): sqlite.DbConn =
  let i = connectToSqDb()
  if i.isOk():
    i.unwrap()
  else:
    return err(contextMsg & ": " & i.unwrapErr())

template connectToSqDbOrErr*(retval: untyped): sqlite.DbConn =
  if (let i = connectToSqDb(); i.isOk()):
    i.unwrap()
  else:
    return err(retval)

proc dbQuoteDirectly*(s: string): string =
  for c in items(s):
    case c
    of '\'':
      add(result, "''")
    of '\0':
      add(result, "\\0")
    else:
      add(result, c)

proc toPgDbArray*(s: seq[string]): string =
  ## Must be called with dbQuote or inside an exec() / etc. function
  result = "{\""
  for i in 0 ..< len(s):
    # reimplement dbQuote
    for c in s[i]:
      case c
      of '"':
        result.add("\\\"")
      else:
        result.add(c)
    if (len(s) > 0) and (i != len(s) - 1):
      result.add("\",\"")
  result.add("\"}")

proc toPgDbArray*(s: seq[Opt[string]]): string =
  ## Must be called with dbQuote or inside an exec() / etc. function
  result = "{"
  for i in 0 ..< len(s):
    # reimplement dbQuote
    if not s[i].isSome():
      result &= "NULL"
    else:
      let e = s[i].unwrap()
      result.add('"')
      for c in e:
        case c
        of '"':
          result.add("\\\"")
        else:
          result.add(c)
      result.add('"')
    if (len(s) > 0) and (i != len(s) - 1):
      result.add(',')
  result.add('}')

template stringFromNullable*(which: string): Opt[string] =
  if len(which) > 0:
    Opt[string].some(which)
  else:
    Opt.none(string)

template intFromNullable*(which: string): Opt[int] =
  if len(which) > 0:
    try:
      Opt[int].some(strutils.parseInt(which))
    except CatchableError as e:
      return err(e.message())
  else:
    Opt.none(int)

template psqlNullableFrom*[T](which: Opt[T]): string =
  ## Generates a string that contains either `NULL` or the *quoted* string rep
  ## of the argument which can be passed in safely to fmt(query)
  when false:
    # Invalid matching clause: NONE
    # Error: Expected a node of kind among @[nnkIdent, nnkSym], got nnkOpenSymChoice
    # hitting some obscure errors here! such is the price of power...
    match which:
      Some(v):
        psql.dbQuote($v)
      None:
        "NULL"
  else:
    if which.isSome():
      psql.dbQuote($which.unwrap())
    else:
      "NULL"

template sqliteNullableFrom*[T](which: Opt[T]): string =
  ## Generates a string that contains either `NULL` or the *quoted* string rep
  ## of the argument which can be passed in safely to fmt(query)
  when false:
    match which:
      Some(v):
        sqlite.dbQuote($v)
      None:
        "NULL"
  else:
    if which.isSome():
      sqlite.dbQuote($which.unwrap())
    else:
      "NULL"
