# Unqualified imports
import ./log
import ./resutils

# Qualified imports
from std/httpcore as hc import nil
from ../render import nil
from ./datatypes as dt import nil

template replyAndTerminateWithError*(
    code: hc.HttpCode, what, userError, internalError: string, sess: Res[dt.SessionRef]
): untyped =
  log.error(what, message = internalError)
  let msg = render.Error(code, userError, sess)
  http.reply(code, msg)
  return

proc makeErrorTuple*(
    code: hc.HttpCode,
    what: static string,
    userError, internalError: string,
    sess: Res[dt.SessionRef],
): dt.RespType =
  log.error(what, message = internalError)
  let msg = render.Error(code, userError, sess)
  (code: code, body: msg)

template makeDbErrorTuple*(internError: string, sess: Res[dt.SessionRef]): dt.RespType =
  makeErrorTuple(
    hc.Http500,
    "Database error",
    "Database operational error",
    "Something went wrong with the database: " & internError,
    sess,
  )
