from std/strutils import nil
from std/strtabs import nil

proc ellipsize*(s: string, max: Natural): string =
  if len(s) > max:
    var n = newString(max + 3)
    copyMem(n[0].addr, s[0].addr, max)
    # …
    n[max] = '\xe2'
    n[max + 1] = '\x80'
    n[max + 2] = '\xa6'
    n
  else:
    s

proc filenameize*(s: string): string =
  result = newStringOfCap(len(s))
  for i in s:
    result.add(
      case i
      of 'A' .. 'Z', 'a' .. 'z':
        $i
      of '0' .. '9':
        $i
      of '-':
        $i
      of '.':
        "."
      else:
        "-"
    )

proc escape*(s: string): string =
  ## HTML-escapes a string.
  # the string will be of at least the length of s
  result = newStringOfCap(len(s))
  for i in s:
    result.add(
      case i
      of '<':
        "&lt;"
      of '>':
        "&gt;"
      of '&':
        "&amp;"
      of '\'':
        "&#39;"
      of '"':
        "&#34;"
      else:
        $i
    )

proc anchorize*(s: string): string =
  ## Converts a string into a form suitable for use as
  ## HTML anchors
  result = newStringOfCap(len(s))
  for i in s:
    result.add(
      case i
      of 'a' .. 'z':
        $i
      of '0' .. '9':
        $i
      of 'A' .. 'Z':
        $(strutils.toLowerAscii(i))
      else:
        "-"
    )

proc getFormInput*(t: strtabs.StringTableRef, k: string): string =
  strutils.strip(strtabs.getOrDefault(t, k))

func formatSize*(bytes: int64, precision: Natural = 3): string =
  ## Copy of strutils.formatSize().
  ##
  ## Rounds and formats `bytes`. Unlike the standard library proc, the precision
  ## is configurable. However, the decimal separator, the prefix and spacing
  ## preference are not.
  var
    xb: int64 = bytes
    fbytes: float
    lastXb: int64 = bytes
    matchedIndex = 0
    prefixes = ["", "k", "M", "G", "T", "P", "E", "Z", "Y"]
    divisor: int64 = 1000

  # Iterate through prefixes seeing if value will be greater than
  # 0 in each case
  for index in 1 ..< prefixes.len:
    lastXb = xb
    xb = bytes div divisor
    matchedIndex = index
    divisor *= 1000
    if xb == 0:
      xb = lastXb
      matchedIndex = index - 1
      divisor = divisor div 1_000_000
      break
  # xb has the integer number for the latest value; index should be correct
  fbytes = bytes.float / divisor.float
  result = strutils.formatFloat(
    fbytes, format = strutils.ffDecimal, precision = precision, decimalSep = '.'
  )
  strutils.trimZeros(result, '.')
  result &= " "
  result &= prefixes[matchedIndex]
  result &= "B"
