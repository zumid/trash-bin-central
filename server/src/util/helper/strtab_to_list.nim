import std/strtabs

template asList*(s: StringTableRef): seq[string] =
  var r: seq[string]
  for key in s.keys():
    r.add(key & ": " & s.getOrDefault(key))
  r
