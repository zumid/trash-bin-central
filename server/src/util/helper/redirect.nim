from std/httpcore as hc import nil
from ../../render import nil
from ../datatypes as dt import nil

proc redirect*(location: string, responseHeaders: var seq[string]): dt.RespType =
  let body = render.RedirectPlaceholder(location)
  when false:
    return (code: hc.Http200, body: body)
  else:
    responseHeaders.add("Location: " & location)
    return (code: hc.Http303, body: body)

proc redirect*(location: string, responseHeaders: ptr seq[string]): dt.RespType =
  return redirect(location, responseHeaders[])
