import ../resutils
import ../dbmanager
import ../datatypes

from ../../config import nil # used by dbmanager

{.push gcsafe, raises: [].}

proc insertAddonLevel*(
    s: ref Database, addon: Addon, number: int, heading: LevelHeading
): Failable =
  let id = %addon.id
  try:
    let newAddonLevelQuery =
      s.prepareQuery("new addon level", "new_level_of_addon_id.sql", 4)
    s.conn.exec(newAddonLevelQuery, $id, $number, heading.name, $heading.act)
    return ok()
  except DbError as e:
    return err(e.message())

proc getLevelsFromAddonLatest*(s: ref Database, addon: Addon): Res[seq[string]] =
  let id = %addon.id
  try:
    let getLevelsQuery =
      s.prepareQuery("levels from addon ID", "get_levels_from_addon_id_latest.sql", 1)
    var levels: seq[string]
    for i in s.conn.fastRows(getLevelsQuery, id):
      levels.add(i)
    return ok(levels)
  except DbError as e:
    return err(e.message())

proc deleteAllLevels*(s: ref Database, addon: Addon): Failable =
  let id = %addon.id
  try:
    let deleteLevelsQuery =
      s.prepareQuery("delete all levels", "delete_all_levels_of_addon_id.sql", 1)
    s.conn.exec(deleteLevelsQuery, $id)
    return ok()
  except DbError as e:
    return err(e.message())
