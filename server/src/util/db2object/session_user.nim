import ../resutils
import ../datatypes

from ./sessions import nil
from ./users import nil

proc getSessionUser*(db: ref Database, sessionId: string): Opt[User] =
  let idRes = sessions.getUserIdFromSession(sessionId)
  if not idRes.isOk():
    return Opt.none(User)

  let idOpt = idRes.unwrap()
  if not idOpt.isSome():
    return Opt.none(User)

  let gotUser = users.getUserById(db, idOpt.unwrap())
  if not gotUser.isOk():
    return Opt.none(User)

  return Opt[User].some(gotUser.unwrap())

proc getSessionUser*(db: ref Database, session: SessionRef): Opt[User] =
  return getSessionUser(db, session.id)

proc getSessionUser*(db: ref Database, session: Res[SessionRef]): Opt[User] =
  if not session.isOk():
    return Opt.none(User)
  return getSessionUser(db, session.unwrap())
