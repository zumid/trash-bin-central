import ../resutils
import ../dbmanager
import ../log

from std/times import nil
from std/strutils import nil
from libsodium/sodium import nil
from ../../config import nil
from ../datatypes as dt import nil

{.push gcsafe, raises: [].}

proc getPasswordHash(s: string): Res[string] =
  let hash =
    try:
      sodium.cryptoPwHashStr(s)
    except sodium.SodiumError as e:
      return err(e.message())
    except ValueError as e:
      return err(e.message())
  return ok(hash)

# not using the User object yet because it's still so tiny
proc insertUser*(s: ref dt.Database, name: string, password: string): Res[dt.User] =
  let hashedpw = ?password.getPasswordHash()
  let newUserQuery =
    try:
      s.prepareQuery("new user", "new_user.sql", 2)
    except DbError as e:
      return err(e.message())
  let newUserId =
    try:
      let id = s.conn.getValue(newUserQuery, name, hashedpw)
      strutils.parseInt(id)
    except DbError as e:
      return err(e.message())
    except ValueError as e:
      return err(e.message())
  return ok(dt.User(id: Opt[int].ok(newUserId), name: name))

proc toUser(row: psql.Row): Res[dt.User] =
  try:
    return ok(
      dt.User(
        id: Opt[int].ok(strutils.parseInt(row[0])),
        name: row[1],
        pfpHash: (
          if len(row[2]) < 1:
            Opt.none(int64)
          else:
            Opt[int64].ok(strutils.parseInt(row[2]).int64)
        ),
        joinDate: times.parse(row[3], config.PostgresIsoFormat),
        lastSeen: times.parse(row[4], config.PostgresIsoFormat),
      )
    )
  except ValueError as e:
    return err(e.message())

proc getUserByName*(s: ref dt.Database, name: string): Res[dt.User] =
  let getUserQuery =
    try:
      s.prepareQuery("get user by name", "get_user_by_name.sql", 1)
    except DbError as e:
      return err(e.message())
  let userRow =
    try:
      s.conn.getRow(getUserQuery, name)
    except DbError as e:
      return err(e.message())
  return userRow.toUser()

proc userExists*(s: ref dt.Database, name: string): Res[bool] =
  let userExistQuery =
    try:
      s.prepareQuery("user exists", "check_user_name_exists.sql", 1)
    except DbError as e:
      return err(e.message())
  try:
    let exists = s.conn.getValue(userExistQuery, name)
    if len(exists) < 1:
      return ok(false)
    return ok(true)
  except DbError as e:
    return err(e.message())

proc getUserByAuth*(s: ref dt.Database, name: string, password: string): Res[dt.User] =
  let hashedPwQuery =
    try:
      s.prepareQuery("get user password", "get_user_password.sql", 1)
    except DbError as e:
      return err(e.message())
  let hashedPw =
    try:
      let i = s.conn.getValue(hashedPwQuery, name)
      if i == "":
        return err("user does not exist or has no password")
      i
    except DbError as e:
      return err(e.message())

  if not sodium.cryptoPwHashStrVerify(hashedPw, password):
    log.trace("failed verify pw hash")
    return err("incorrect password")

  let needsRehash = sodium.cryptoPwHashStrNeedsRehash(hashedPw)
  case needsRehash
  of 0:
    log.trace("pw doesn't need rehash")
    discard
  else:
    if needsRehash == -1:
      log.error("needsRehash error, rehashing anyway")
    log.trace("rehash pw")
    let updatePwQuery =
      try:
        s.prepareQuery("update password", "update_user_password.sql", 2)
      except DbError as e:
        return err(e.message())
    let rehash = ?password.getPasswordHash()
    try:
      s.conn.exec(updatePwQuery, rehash, name)
      log.trace("pw rehashed")
    except DbError as e:
      return err(e.message())

  block updateLastSeen:
    let updateLastSeenQuery =
      try:
        s.prepareQuery("update last seen status", "update_user_login.sql", 1)
      except DbError as e:
        log.error("couldn't prepare query for update status", e = e.message())
        break updateLastSeen
    try:
      s.conn.exec(updateLastSeenQuery, name)
      log.trace("login status updated")
    except DbError as e:
      log.error("cannot set login status", e = e.message())
      break updateLastSeen

  log.trace("login success")
  return getUserByName(s, name)

proc getUserById*(s: ref dt.Database, id: int): Res[dt.User] =
  let getUserQuery =
    try:
      s.prepareQuery("get user by id", "get_user_by_id.sql", 1)
    except DbError as e:
      return err(e.message())
  let userRow =
    try:
      s.conn.getRow(getUserQuery, $id)
    except DbError as e:
      return err(e.message())
  return userRow.toUser()

proc getUserStats*(s: ref dt.Database, id: int): Res[dt.UserStats] =
  let getUserStatQuery =
    try:
      s.prepareQuery("get user stats", "get_user_one_statistic.sql", 1)
    except DbError as e:
      return err(e.message())
  let uRow =
    try:
      s.conn.getRow(getUserStatQuery, $id)
    except DbError as e:
      return err(e.message())
  let uStats =
    try:
      dt.UserStats(
        addonCount: strutils.parseInt(uRow[0]), reviewCount: strutils.parseInt(uRow[1])
      )
    except ValueError as e:
      return err(e.message())
  return ok(uStats)

proc getUserStats*(s: ref dt.Database, user: dt.User): Res[dt.UserStats] =
  let id = %user.id
  return getUserStats(s, id)
