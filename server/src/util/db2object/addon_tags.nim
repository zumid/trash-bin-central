import ../resutils
import ../dbmanager
import ../datatypes

from std/strutils import nil
from ../../config import nil # used by dbmanager

{.push gcsafe, raises: [].}

proc assignTag*(
    s: ref Database, addon: Addon, tag: string, createIfNotExists: bool = false
): Failable =
  let addonId = %addon.id # get or create tag ID
  try:
    let getTagIdQuery = s.prepareQuery("get tag id", "get_tag_id_by_name.sql", 1)
    let row = s.conn.getRow(getTagIdQuery, tag)
    let tagIdResult =
      if row[0] == "":
        if createIfNotExists:
          let newTagQuery = s.prepareQuery("new tag", "new_tag.sql", 1)
          let ntqRow = s.conn.getRow(newTagQuery, tag)
          Opt[int].some(strutils.parseInt(ntqRow[0]))
        else:
          return err("no such tag exists")
      else:
        Opt[int].some(strutils.parseInt(row[0]))
    let tagId = tagIdResult.unwrap()
    let assocAddonTagQuery = s.prepareQuery(
      "associate addon with tag id", "associate_addon_id_with_tag_id.sql", 2
    )
    let aatqRow = s.conn.getRow(assocAddonTagQuery, addonId, tagId)
    if aatqRow[0] == "":
      return err("cannot associate addon with tag")
    return ok()
  except DbError as e:
    return err(e.message())
  except ValueError as e:
    return err(e.message())

proc getTagsForAddon*(s: ref Database, addon: Addon): Res[seq[string]] =
  let addonId = %addon.id
  var res: seq[string] = @[]
  try:
    let addonTagNamesQuery =
      s.prepareQuery("addon tag names", "get_tag_names_for_addon.sql", 1)
    for i in s.conn.fastRows(addonTagNamesQuery, addonId):
      res.add(i[0])
    return ok(res)
  except DbError as e:
    return err(e.message())
