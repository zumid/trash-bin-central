import ../resutils
import ../dbmanager
import ../log
import ../datatypes

from std/paths import `/`

from std/files import nil
from std/strutils import nil
from std/times import nil
from std/random import nil
from ../../config import nil
from ../helper/strhelper import nil
from ./addon_versions import nil

type AddonDateSortBy* = enum
  UploadDate
  LastUpdated

{.push gcsafe, raises: [].}

proc toAddon(row: psql.Row): Res[Addon] =
  try:
    return ok(
      Addon(
        id: intFromNullable(row[0]),
        name: strhelper.ellipsize(row[1], config.AddonNameLimit),
        synopsis: row[2],
        description: stringFromNullable(row[3]),
        author: row[4],
        authorId: intFromNullable(row[5]),
        uploadDate: times.parse(row[6], config.PostgresIsoFormat),
        lastUpdated: times.parse(row[7], config.PostgresIsoFormat),
        previewHash: 0, # todo
      )
    )
  except CatchableError as e:
    return err(e.message())

proc generateEditCode*(): Res[string] =
  var r =
    try:
      random.initRand()
    except Exception as e:
      return err(e.message())
  var s = newString(16)
  {.warning: "TODO this shares the same pool as makeSession; should it be different?".}
  for i in 0 ..< len(s):
    s[i] = random.sample(
      r,
      {'A' .. 'Z'} + {'a' .. 'z'} + {'0' .. '9'} +
        {'!', '#', '$', '+', '-', '.', '/', '~'},
    )
  return ok(s)

proc checkEditCode*(s: ref Database, addon: Addon, editCode: string): Failable =
  let id = %addon.id
  try:
    let checkECQuery =
      s.prepareQuery("check addon edit code", "check_edit_code_for_addon.sql", 1)
    let val = s.conn.getValue(checkECQuery, id, editCode)
    if val == "1":
      return ok()
    return err("edit code invalid")
  except CatchableError as e:
    return err(e.message())

proc insertAddonToDb*(s: ref Database, addon: Addon, editCode: string): Res[Addon] =
  if addon.id.isSome():
    return err("cannot insert addon with ID")
  try:
    # make a copy of the addon
    var newAddon = addon
    let
      descriptionSanitized = psqlNullableFrom(addon.description)
      assocAuthorIdSanitized = psqlNullableFrom(addon.authorId)
      md5CommandSanitized = if len(editCode) < 1: "$4" else: "md5($4)"
      newAddonQuery = s.prepareQuery("new addon", "new_addon.sql", 4)
      insertResult =
        s.conn.getRow(newAddonQuery, addon.name, addon.author, addon.synopsis, editCode)
    s.unprepareQuery("new addon")
    # modify copy
    newAddon.id = typeof(newAddon.id).some(strutils.parseInt(insertResult[0]))
    newAddon.uploadDate = times.parse(insertResult[1], config.PostgresIsoFormat)
    newAddon.lastUpdated = times.parse(insertResult[2], config.PostgresIsoFormat)
    # and return it
    return ok(newAddon)
  except CatchableError as e:
    return err(e.message())

proc addonExistsByName*(s: ref Database, name: string): Res[bool] =
  try:
    let checkAddonNameExists =
      s.prepareQuery("check addon name", "check_addon_name_exists.sql", 1)
    let val = s.conn.getValue(checkAddonNameExists, name)
    if val == "1":
      return ok(true)
    return ok(false)
  except CatchableError as e:
    return err(e.message())

proc getAllAddons*(
    s: ref Database,
    latestToOldest: bool = true,
    sortBy: AddonDateSortBy = UploadDate,
    limit: int = -1,
): Res[seq[Addon]] =
  try:
    let
      sortColumn =
        case sortBy
        of UploadDate: "upload_date"
        of LastUpdated: "last_updated"
      sortDirection =
        case latestToOldest
        of false: "ASC"
        of true: "DESC"
      sqlLimit =
        if limit < 0:
          "ALL"
        else:
          $limit
    var addons: seq[Addon]
    for i in s.conn.fastRows(psql.SqlQuery(makeQuery("get_all_addons.sql"))):
      addons.add(?toAddon(i))
    return ok(addons)
  except CatchableError as e:
    return err(e.message())

proc getAddonById*(s: ref Database, whichId: int): Res[Opt[Addon]] =
  try:
    let getAddonQuery = s.prepareQuery("get one addon", "get_addon_by_id.sql", 1)

    let row = s.conn.getRow(getAddonQuery, whichId)
    if row[0] == "":
      return ok(Opt.none(Addon))

    let existingAddon = s.conn.getRow(getAddonQuery, whichId).toAddon()
    if not existingAddon.isOk():
      return existingAddon.convertErr()

    return ok(Opt[Addon].some(existingAddon.unwrap()))
  except CatchableError as e:
    return err(e.message())

proc getRandomAddon*(s: ref Database): Res[Opt[Addon]] =
  try:
    let getAddonQuery = s.prepareQuery("get random addon", "get_addon_by_random.sql", 0)

    let row = s.conn.getRow(getAddonQuery)
    if row[0] == "":
      return ok(Opt.none(Addon))

    let existingAddon = s.conn.getRow(getAddonQuery).toAddon()
    if not existingAddon.isOk():
      return existingAddon.convertErr()

    return ok(Opt[Addon].some(existingAddon.unwrap()))
  except CatchableError as e:
    return err(e.message())

proc previewFilenameForAddon*(addon: Addon, extension: string = "png"): Opt[string] =
  if not addon.id.isSome():
    return Opt.none(string)

  let id = addon.id.unwrap()
  try:
    let name = $id
    let fsAndWebName = name & "." & extension

    # check if the file really exists in the file system
    let translatedPath = paths.Path(config.PreviewsDir) / paths.Path(fsAndWebName)

    if not (files.fileExists(translatedPath)):
      return Opt.none(string)

    return Opt[string].some(fsAndWebName)
  except CatchableError as e:
    log.error("cannot get preview name", error = e.message())
    return Opt.none(string)

proc addonToAddonBlurb*(s: ref Database, addon: Addon): Res[AddonBlurb] =
  let addonId = %addon.id
  let latest = ?addon_versions.getLatestAddonVersion(s, addon)
  var imageUrl: string
  var imageAlt: string
  if (let i = previewFilenameForAddon(addon); i.isSome()):
    imageUrl = "/previews/" & i.unwrap()
    imageAlt = "Preview for addon " & $addonId
  else:
    imageUrl = "/assets/missing-preview.png"
    imageAlt = "Missing preview"
  return ok(
    AddonBlurb(
      id: addonId,
      name: addon.name,
      version: latest.versionString,
      imageUrl: imageUrl,
      imageAlt: imageAlt,
      authorId: addon.authorId,
      author: addon.author,
      synopsis: addon.synopsis,
      lastUpdateTime: addon.lastUpdated,
      latestFileSize: latest.fileSize,
    )
  )

proc getAddonNameFromId*(s: ref Database, id: int): Res[string] =
  let getNameQuery =
    try:
      s.prepareQuery("get addon name only", "get_addon_name_only_by_id.sql", 1)
    except KeyError as e:
      return err(e.message())
    except DbError as e:
      return err(e.message())
  let name =
    try:
      strhelper.ellipsize(s.conn.getValue(getNameQuery, $id), config.AddonNameLimit)
    except DbError as e:
      return err(e.message())
  return ok(name)

proc updateAddonInfo*(s: ref Database, addon: Addon): Failable =
  let id = %addon.id
  let updateAddonInfoQuery =
    try:
      s.prepareQuery("update addon info", "replace_addon_info.sql", 5)
    except KeyError as e:
      return err(e.message())
    except DbError as e:
      return err(e.message())
  try:
    s.conn.exec(
      updateAddonInfoQuery,
      addon.name,
      addon.synopsis,
      (
        if addon.description.isSome:
          addon.description.unwrap()
        else:
          ""
      ),
      addon.author,
      $id,
    )
  except DbError as e:
    return err(e.message())
  return ok()

proc makeVersionList(): string {.compiletime.} =
  var vl: string
  for i in AddonTarget:
    vl.add '\''
    vl.add $i
    vl.add '\''
    if not (i == high(AddonTarget)):
      vl.add ','
  vl

proc updateAddonCompat*(s: ref Database, addon: Addon, compat: AddonTarget): Failable =
  const versionList = makeVersionList()
  let id = %addon.id
  let updateAddonCompatQuery =
    try:
      s.prepareQuery("update addon complevel", "replace_addon_compat_level.sql", 2)
    except KeyError as e:
      return err(e.message())
    except DbError as e:
      return err(e.message())
  try:
    s.conn.exec(updateAddonCompatQuery, $compat, $id)
  except DbError as e:
    return err(e.message())
  return ok()

proc getLatestHighestRatedAddons*(s: ref Database, limit: int = 10): Res[seq[Addon]] =
  var addons: seq[Addon]
  let getHighestRatedAddonsQuery =
    try:
      s.prepareQuery("highest rated addons", "get_addons_highest_rated.sql", 1)
    except KeyError as e:
      return err(e.message())
    except DbError as e:
      return err(e.message())
  try:
    for i in s.conn.fastRows(getHighestRatedAddonsQuery, $limit):
      addons.add(?toAddon(i))
  except DbError as e:
    return err(e.message())
  return ok(addons)

proc buildSearchQuery(q: SearchQuery): string =
  # we're going in raw, because this is a variable query.
  var finalQuery =
    """
    WITH whole_results AS (
    SELECT
      id,
      name,
      synopsis,
      description,
      author,
      associated_author_id,
      """ &
    config.PostgresFormatTime("upload_date") &
    """ as upload_date,
      """ & config.PostgresFormatTime("last_updated") &
    """ as last_updated,
      COUNT(*) OVER () as total_results
    FROM
    """ &
    config.AddonTableName
  var whereClauses: seq[string] = @[]
  if q.term.isSome():
    whereClauses.add(
      case q.termType
      of AddonTerm:
        """
        (name ILIKE '%""" & dbQuoteDirectly(q.term.unwrap()) &
          """%')
        """
      of LevelTerm:
        """
        (
          id IN
          (
            SELECT DISTINCT
              addon_id
            FROM
              """ &
          config.AddonLevelTableName &
          """
            WHERE
              level_name ILIKE '%""" &
          dbQuoteDirectly(q.term.unwrap()) &
          """%'
          )
        )
        """
      of AddonOrLevelTerm:
        """
        (
          id IN
          (
            SELECT DISTINCT
              addon_id
            FROM
              """ &
          config.AddonLevelTableName &
          """
            WHERE
              level_name ILIKE '%""" &
          dbQuoteDirectly(q.term.unwrap()) &
          """%'
          )
          OR
          name ILIKE '%""" &
          dbQuoteDirectly(q.term.unwrap()) &
          """%'
        )
        """
    )
  if q.author.isSome():
    whereClauses.add(
      """
      (author ILIKE '%""" & dbQuoteDirectly(q.author.unwrap()) &
        """%')
      """
    )
  if q.tags.isSome():
    var tagList = ""
    block:
      let tags = q.tags.unwrap()
      for i in 0 ..< len(tags):
        tagList.add(psql.dbQuote(tags[i]))
        if i != (len(tags) - 1):
          tagList.add(',')

    whereClauses.add(
      """
      (
        id IN
        (
          SELECT
            addon_id
          FROM
            """ &
        config.AddonTagAssocTableName &
        """
          WHERE
            tag_id IN
          (
            SELECT
              id
            FROM
              """ &
        config.TagTableName &
        """
            WHERE
              tag IN
              (
                """ &
        tagList &
        """
              )
          )
        )
      )
    """
    )
  if len(whereClauses) > 0:
    finalQuery.add(" WHERE ")
    finalQuery.add(strutils.join(whereClauses, " AND "))
  finalQuery.add(") SELECT * FROM whole_results ")
  if q.showResultsAfter.isSome():
    finalQuery.add(
      """
      WHERE (
        id > """ & $q.showResultsAfter.unwrap() &
        """
      )
      """
    )
  {.warning: "TODO do I need to add ORDER BY?".}
  if q.numOfResults.isSome():
    finalQuery.add(" LIMIT " & $q.numOfResults.unwrap())
  return finalQuery

proc getAddonsFromSearchQuery*(
    s: ref Database, q: SearchQuery, amountBuffer: var int
): Res[seq[Addon]] =
  var addons: seq[Addon]
  var alreadyWritten = false
  try:
    for i in s.conn.fastRows(psql.SqlQuery(buildSearchQuery(q))):
      addons.add(?toAddon(i))
      if not alreadyWritten:
        amountBuffer = strutils.parseInt(i[8])
        alreadyWritten = true
  except DbError as e:
    return err(e.message())
  except ValueError as e:
    return err(e.message())
  return ok(addons)

proc getAddonsByUser*(s: ref Database, user: User): Res[seq[Addon]] =
  let uid = %user.id
  var addons: seq[Addon]
  let getAddonUserQuery =
    try:
      s.prepareQuery("get addon from user query", "get_addons_by_user.sql", 1)
    except DbError as e:
      return err(e.message())
  try:
    for i in s.conn.fastRows(getAddonUserQuery, $uid):
      addons.add(?toAddon(i))
  except DbError as e:
    return err(e.message())
  return ok(addons)

when isMainModule:
  let sq = buildSearchQuery(
    SearchQuery(
      term: Opt[string].some("test'); DROP TABLE students; --"),
      termType: AddonOrLevelTerm,
      author: Opt[string].some("hello"),
      tags: Opt[seq[string]].some(@["hi", "test", "1", "2", "3"]),
      numOfResults: Opt[int].some(10),
      showResultsAfter: Opt[int].some(11),
    )
  )
  var ii = ""
  var spaceRecently = false
  for i in sq:
    case i
    of ' ', '\n', '\t', '\r':
      if not spaceRecently:
        ii.add(' ')
        spaceRecently = true
    else:
      ii.add(i)
      spaceRecently = false
  debugEcho()
  debugEcho(ii)
  debugEcho()
