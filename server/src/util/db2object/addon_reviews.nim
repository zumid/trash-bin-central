import ../datatypes
import ../dbmanager
import ../resutils
import ../log

from std/strutils import nil
from std/times import nil
from ../../config import nil

{.push gcsafe, raises: [].}

proc insertReviewToDb*(
    s: ref Database, addon: Addon, reviewer: Opt[User], rating: int, content: string
): Failable =
  let id = %addon.id
  try:
    let reviewerIdValue =
      if reviewer.isSome():
        psqlNullableFrom(reviewer.unwrap().id)
      else:
        psqlNullableFrom(Opt.none(int))
    let reviewerName =
      if reviewer.isSome():
        reviewer.unwrap().name
      else:
        "Anonymous"
    let newReviewQuery = s.prepareQuery("new review", "new_addon_review.sql", 4)
    s.conn.exec(newReviewQuery, $id, reviewerName, $rating, content)
    s.unprepareQuery("new review")
    return ok()
  except DbError as e:
    return err(e.message())

proc insertReviewReplyToDb*(
    s: ref Database, addon: Addon, reviewId: int, reviewer: Opt[User], content: string
): Failable =
  let id = %addon.id
  try:
    let reviewerIdValue =
      if reviewer.isSome():
        psqlNullableFrom(reviewer.unwrap().id)
      else:
        psqlNullableFrom(Opt.none(int))
    let reviewerName =
      if reviewer.isSome():
        reviewer.unwrap().name
      else:
        "Anonymous"
    let newReplyQuery =
      s.prepareQuery("new review reply", "new_addon_review_reply.sql", 4)
    s.conn.exec(newReplyQuery, $id, $reviewId, reviewerName, content)
    s.unprepareQuery("new review reply")
    return ok()
  except DbError as e:
    return err(e.message())

proc toReview(row: psql.Row): Res[Review] =
  try:
    return ok(
      Review(
        id: strutils.parseInt(row[0]),
        addonId: strutils.parseInt(row[1]),
        reviewer: row[2],
        associatedReviewerId: intFromNullable[row[3]],
        rating: strutils.parseInt(row[4]),
        reviewDate: times.parse(row[5], config.PostgresIsoFormat),
        content: row[6],
      )
    )
  except CatchableError as e:
    return err(e.message())

proc toReviewReply(row: psql.Row): Res[ReviewReply] =
  try:
    return ok(
      ReviewReply(
        id: strutils.parseInt(row[0]),
        reviewer: row[1],
        associatedReviewerId: intFromNullable[row[2]],
        reviewDate: times.parse(row[3], config.PostgresIsoFormat),
        content: row[4],
      )
    )
  except CatchableError as e:
    return err(e.message())

proc getAddonReviews*(
    s: ref Database, addon: Addon, withReplies: bool = true
): Res[seq[Review]] =
  let id = %addon.id
  try:
    let getReviewsQuery =
      s.prepareQuery("get all reviews for addon", "get_all_reviews_for_addon.sql", 1)
    let getReviewRepliesQuery =
      s.prepareQuery("get all replies for review", "get_all_replies_for_review.sql", 1)
    var reviewList: seq[Review]
    for row in s.conn.fastRows(getReviewsQuery, id):
      reviewList.add(?toReview(row))
    if withReplies:
      for review in mitems(reviewList):
        for reply in s.conn.fastRows(getReviewRepliesQuery, review.id):
          review.replies.add(?toReviewReply(reply))
    return ok(reviewList)
  except DbError as e:
    return err(e.message())

proc getRatingString*(s: ref Database, addon: Addon | AddonBlurb): Res[string] =
  let id =
    when addon is Addon:
      %addon.id
    else:
      addon.id
  try:
    let getRatingStringQuery = s.prepareQuery(
      "get rating string for addon", "get_rating_string_for_addon.sql", 1
    )
    return ok(s.conn.getValue(getRatingStringQuery, $id))
  except DbError as e:
    return err(e.message())

proc getLatestReviews*(
    s: ref Database, limit: int = 5, withReplies: bool = false
): Res[seq[Review]] =
  try:
    let getReviewsQuery =
      s.prepareQuery("get latest reviews", "get_latest_reviews.sql", 0)
    let getReviewRepliesQuery =
      s.prepareQuery("get all replies for review", "get_all_replies_for_review.sql", 1)
    var reviewList: seq[Review]
    for row in s.conn.fastRows(getReviewsQuery):
      reviewList.add(?toReview(row))
    if withReplies:
      for review in mitems(reviewList):
        for reply in s.conn.fastRows(getReviewRepliesQuery, review.id):
          review.replies.add(?toReviewReply(reply))
    return ok(reviewList)
  except DbError as e:
    return err(e.message())

proc getReviewsByUser*(s: ref Database, id: int): Res[seq[Review]] =
  let getReviewsQuery =
    try:
      s.prepareQuery("get reviews by user", "get_reviews_by_user.sql", 1)
    except DbError as e:
      return err(e.message())
  var reviewList: seq[Review]
  try:
    for row in s.conn.fastRows(getReviewsQuery, $id):
      reviewList.add(?toReview(row))
    return ok(reviewList)
  except DbError as e:
    return err(e.message())

proc getReviewsByUser*(s: ref Database, user: User): Res[seq[Review]] =
  let id = %user.id
  return getReviewsByUser(s, id)
