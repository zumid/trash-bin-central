import ../resutils
import ../dbmanager
import ../datatypes

from ../../config import nil # used by dbmanager

{.push gcsafe, raises: [].}

proc trackAddonDownload*(
    s: ref Database, version: AddonVersionInfo, ip: Opt[string] = Opt.none(string)
): Failable =
  let newAddonDownloadQuery =
    try:
      if ip.isSome():
        s.prepareQuery("track new addon download", "new_addon_download_count.sql", 3)
      else:
        s.prepareQuery(
          "track new addon download", "new_addon_download_count_unknown_ip.sql", 2
        )
    except KeyError as e:
      return err(e.message())
    except DbError as e:
      return err(e.message())
  try:
    if ip.isSome():
      s.conn.exec(newAddonDownloadQuery, $version.addonId, $version.hash, ip.unwrap())
    else:
      s.conn.exec(newAddonDownloadQuery, $version.addonId, $version.hash)
  except DbError as e:
    return err(e.message())
  return ok()

proc getDownloadCountForAddon*(s: ref Database, addon: Addon | AddonBlurb): Res[int] =
  let id =
    when addon is Addon:
      %addon.id
    else:
      addon.id
  let downloadCountQuery =
    try:
      s.prepareQuery("get addon download count", "get_download_count_for_addon.sql", 1)
    except KeyError as e:
      return err(e.message())
    except DbError as e:
      return err(e.message())
  let res =
    try:
      s.conn.getValue(downloadCountQuery, $id)
    except DbError as e:
      return err(e.message())
  try:
    return ok(strutils.parseInt(res))
  except ValueError as e:
    return err(e.message())
