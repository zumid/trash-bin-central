import std/strtabs
import std/strutils
import std/strformat
import std/times
import ../log
import ../resutils
import ../dbmanager
import ../datatypes

from std/random import sample

from ../../config import nil
from ../parse/cookies as cookieparser import nil

{.push gcsafe, raises: [].}

var sessDb: sqlite.DbConn
type CheckTokenError* = enum
  NoSession = "No session, please try again" # 400
  DatabaseError = "Database error, please try again" # 500
  InvalidToken = "Invalid token, please try again" # 400

# Session management
proc openSessionDb*(withInit: bool): Failable =
  {.cast(raises: []).}:
    sessDb = connectToSqDbOrErr("about to init sess db")
  try:
    if withInit:
      sqlite.exec(sessDb, prepareQuery("create_session_table.sql"))
      sqlite.exec(sessDb, prepareQuery("create_flash_message_table.sql"))
      sqlite.exec(sessDb, prepareQuery("create_acsrf_token_table.sql"))
      sqlite.exec(sessDb, prepareQuery("create_on_delete_session_trigger.sql"))
  except DbError as e:
    return err(e.message())
  return ok()

proc getSession*(headers: StringTableRef): Res[Opt[SessionRef]] =
  ## Queries the database for an existing session with the key
  ## stated in e.g. tb_session.

  # get the session ID from the cookie
  let sessionIdResult = cookieparser.cookieExists(headers, config.SessionCookieName)
  if not sessionIdResult.isOk():
    return sessionIdResult.convertErr()

  let sessionId = block:
    let r = sessionIdResult.unwrap()
    if not r.isSome():
      return ok(Opt.none(SessionRef))
    r.unwrap()

  var
    sessionObject = SessionRef() ## Prepare session object
    sessionDbRow: seq[string] ## Prepare buffer to store the db row

  block: # database access
    # newer Nim versions will ignore formatting errors,
    # whereas older ones will bail out with a ValueError.
    # the cast pragma is especially useful here
    {.cast(raises: []).}:
      # Because receiving the session ID is letting
      # the engine know that this session is "still alive",
      # we add 5 minutes to the expiry time.
      let updateAndGetSession = prepareQuery("update_and_get_session.sql")
    sessionDbRow =
      try:
        sqlite.getRow(sessDb, updateAndGetSession, sessionId)
      except DbError as e:
        return err(e.message())

  # id has a value for sure because it's a primary key
  # assume this one will always be there
  if len(sessionDbRow[0]) == 0:
    return ok(Opt.none(SessionRef))

  sessionObject.id = sessionDbRow[0]

  # is nillable
  sessionObject.userId =
    if len(sessionDbRow[1]) == 0:
      Opt.none(int)
    else:
      try:
        Opt[int].some(sessionDbRow[1].parseInt())
      except Exception as e:
        return err(e.message())

  # is nillable
  sessionObject.expiry =
    if len(sessionDbRow[2]) == 0:
      Opt.none(times.DateTime)
    else:
      try:
        {.warning: "Assumes default Postgres ISO format".}
        Opt[times.DateTime].some(
          times.parse(sessionDbRow[2], config.PostgresIsoFormat, utc())
        )
      except Exception as e:
        return err(e.message())

  return ok(Opt[SessionRef].some(sessionObject))

proc createAntiCsrfTokenRaw(): string =
  var r = random.initRand()
  # create the token
  var acsrfToken = newString(64)
  for i in 0 ..< len(acsrfToken):
    acsrfToken[i] = r.sample(
      {'A' .. 'Z'} + {'a' .. 'z'} + {'0' .. '9'} +
        {'!', '#', '$', '+', '-', '.', '/', '~'}
    )
  return acsrfToken

proc makeSession*(
    user: UserRef, expireDate: Opt[times.DateTime], responseHeaders: var seq[string]
): Res[SessionRef] =
  # {.warning: "TODO: initRand seed should be IP and current time".}
  var
    r = random.initRand()
    sessionObj = SessionRef()

  # create the session ID
  sessionObj.id = newString(128)
  for i in 0 ..< len(sessionObj.id):
    sessionObj.id[i] = r.sample(
      {'A' .. 'Z'} + {'a' .. 'z'} + {'0' .. '9'} +
        {'!', '#', '$', '+', '-', '.', '/', '~'}
    )

  log.trace("created session", id = sessionObj.id)

  # user ID
  sessionObj.userId =
    if user != nil and user.id.isSome():
      user.id
    else:
      Opt.none(int)

  sessionObj.expiry = expireDate

  sessionObj.isCreated = true

  # Into the database...
  block: # database access
    # Unfortunately there's no easy way to use
    # prepared statements with something that is
    # nullable, because even the word "NULL" will
    # be quoted.
    let userIdStr = sqliteNullableFrom(sessionObj.userId)

    {.cast(raises: []).}:
      let insertSessionQuery = prepareQuery("new_session_id.sql")

    try:
      sessDb.exec(insertSessionQuery, sessionObj.id)
    except DbError as e:
      return err(e.message())

  # And send it to the client
  # Expires parameter might not be necessary...
  responseHeaders.add(
    "Set-Cookie: " & config.SessionCookieName & "=" & sessionObj.id & "; " &
      "Path=/; HttpOnly; SameSite=Lax"
  )

  return ok(sessionObj)

proc getOrMakeSession*(
    requestHeaders: StringTableRef,
    responseHeaders: var seq[string],
    user: UserRef = nil,
    expireDate: Opt[times.DateTime] =
      Opt[times.DateTime].some(times.now() + config.DefaultSessionExpiry),
): Res[SessionRef] =
  let sess = getSession(requestHeaders)
  if not sess.isOk():
    log.trace("get session failed, making new one", reason = sess.unwrapErr())
    return makeSession(user, expireDate, responseHeaders)

  let sessResult = sess.unwrap()
  if not sessResult.isSome():
    log.trace("session doesn't exist, making new one")
    return makeSession(user, expireDate, responseHeaders)

  return ok(sessResult.unwrap())

proc cleanSessions*(which: Opt[string]): Failable =
  ## If `which` is an ok("something") then it will
  ## only delete that session ID. Otherwise, it will
  ## clean all sessions past their expiry date.

  {.cast(raises: []).}:
    let
      cleanSessionQuery = prepareQuery("delete_one_session.sql")
      cleanAllQuery = prepareQuery("delete_all_sessions.sql")
  try:
    if not which.isSome():
      sessDb.exec(cleanAllQuery)
    else:
      let idString = which.unwrap()
      sessDb.exec(cleanSessionQuery, idString)
    return ok()
  except DbError as e:
    return err(e.message())

proc cleanExpiredAcsrfTokens*(): Failable =
  {.cast(raises: []).}:
    let cleanAllQuery = prepareQuery("delete_all_acsrf_tokens.sql")
  try:
    sessDb.exec(cleanAllQuery)
    return ok()
  except DbError as e:
    return err(e.message())

proc sessionIdToInternalId(sessionId: string): Res[int] =
  if len(sessionId) < 1:
    return err("empty session ID")
  let
    sessIdQuery = prepareQuery("get_session_internal_id.sql")
    sessIdRow =
      try:
        sqlite.getRow(sessDb, sessIdQuery, sessionId)
      except DbError as e:
        return err(e.message())
  if len(sessIdRow[0]) < 1:
    return err("session ID not found")
  try:
    return ok(parseInt(sessIdRow[0]))
  except ValueError as e:
    return err(e.message())

proc createAntiCsrfToken*(sessionId: string): Res[string] =
  let intid = ?sessionIdToInternalId(sessionId)
  let acsrfToken = createAntiCsrfTokenRaw()

  log.trace("create token", id = acsrfToken)

  block: # database access
    log.trace("associating token with session", id = acsrfToken)
    {.cast(raises: []).}:
      let insertTokenQuery = prepareQuery("associate_acsrf_token.sql")
    try:
      sessDb.exec(insertTokenQuery, acsrfToken, $intId)
    except DbError as e:
      return err(e.message())
  return ok(acsrfToken)

proc checkAntiCsrfToken*(token, sessionId: string): Result[void, CheckTokenError] =
  if len(token) < 1:
    log.error("No token")
    return err(NoSession)

  let intId = block:
    let id = sessionIdToInternalId(sessionId)
    if id.isOk():
      id.unwrap()
    else:
      log.error("error getting id", e = id.unwrapErr())
      return err(DatabaseError)

  # Get tuple
  var ascrfRow: seq[string]

  # Check token exists
  log.trace("check token exists for session id", id = token, session = sessionId)

  # Prepare used queries
  {.cast(raises: []).}:
    let
      checkTokenQuery = prepareQuery("check_acsrf_token.sql")
      deleteTokenQuery = prepareQuery("delete_acsrf_token.sql")

  ascrfRow =
    try:
      sqlite.getRow(sessDb, checkTokenQuery, token, $intId)
    except DbError as e:
      log.error("db error", reason = e.message())
      return err(DatabaseError)

  # No token
  if len(ascrfRow[0]) < 1:
    log.error("acsrf token does not exist", token = token, sessionId = sessionId)
    return err(InvalidToken)

  # Delete token immediately after checking
  try:
    sessDb.exec(deleteTokenQuery, token, $intId)
    log.trace("validated, deleted from db")
  except DbError as e:
    log.error("db error", reason = e.message())
    return err(DatabaseError)

  return ok()

template addFlashMessageImpl(sessionId: string, whichMessage: untyped): Failable =
  if len(sessionId) < 1:
    return err("Empty session ID")

  log.trace("create flash message", msgId = whichMessage, sesId = sessionId)

  let intId = block:
    let id = sessionIdToInternalId(sessionId)
    if not id.isOk():
      return id.convertErr()
    else:
      id.unwrap()

  block: # database access
    {.cast(raises: []).}:
      let insertFlashMessageQuery = prepareQuery("new_flash_message.sql")
    try:
      when whichMessage is string:
        sessDb.exec(insertFlashMessageQuery, $intId, whichMessage)
      elif whichMessage is FlashMessage:
        sessDb.exec(insertFlashMessageQuery, $intId, ord(whichMessage))
      else:
        {.error: "whichMessage is either a string or a FlashMessage".}
    except DbError as e:
      return err(e.message())
  ok()

proc addFlashMessage*(sessionId: string, whichMessage: FlashMessage): Failable =
  addFlashMessageImpl(sessionId, whichMessage)

# See note in displayFlashMessages why such cursed things are possible.
proc addFlashMessageCustom*(sessionId: string, whichMessage: string): Failable =
  addFlashMessageImpl(sessionId, whichMessage)

proc displayFlashMessages*(sessionId: string): Res[seq[string]] =
  ## Displays all flash messages and clears them immediately
  if len(sessionId) < 1:
    return err("Empty session ID")
  var messages: seq[string] = @[]
  block: # database access
    {.cast(raises: []).}:
      let getMessagesQuery = prepareQuery("get_flash_messages_for_session.sql")
      let clearMessagesQuery = prepareQuery("delete_flash_messages_for_session.sql")
    try:
      for row in sessDb.fastRows(getMessagesQuery, sessionId):
        # It may look like we're abusing a database to hold two different
        # types, but this is SQLite we're talking about, and the developers
        # really intended for this to happen:
        # https://www.sqlite.org/flextypegood.html
        try:
          let idNum = row[0].parseInt()
          let id = cast[FlashMessage](idNum)
          messages.add($FlashMessage(id))
        except ValueError as e:
          log.trace("can't add FlashMessage, trying as string", e = e.message())
          if row[0] != "":
            messages.add(row[0])
      sessDb.exec(clearMessagesQuery, sessionId)
    except CatchableError as e:
      return err(e.message())
  ok(messages)

proc attachUserToSession*(u: User, sessionId: string): Failable =
  let id = %u.id
  let associateUserQuery = prepareQuery("associate_user_id_with_session.sql")
  try:
    sessDb.exec(associateUserQuery, id, sessionId)
  except DbError as e:
    return err(e.message())
  return ok()

proc getUserIdFromSession*(sessionId: string): Res[Opt[int]] =
  let userIdQuery = prepareQuery("get_user_id_from_session.sql")
  try:
    let uid = sessDb.getValue(userIdQuery, sessionId)
    if len(uid) < 1:
      return ok(Opt.none(int))
    return ok(Opt[int].ok(parseInt(uid)))
  except DbError as e:
    return err(e.message())
  except ValueError as e:
    return err(e.message())

proc clearSessionUser*(sessionId: string): Failable =
  let clearUserQuery = prepareQuery("clear_user_id_from_session.sql")
  try:
    sessDb.exec(clearUserQuery, sessionId)
  except DbError as e:
    return err(e.message())
  return ok()
