##[
.. importdoc:: ../config


For types which represent a database result, ``Opt[T]`` fields can exist, in
which case empty values should signify values that do not exist in the database.

Conversely, whenever a value exists they should be whatever was returned from
the database.

]##

import std/tables
import std/sets
import ./log
import ./resutils

from std/times import nil
from std/httpcore import nil
from forkygs/httpserver as http import nil
from db_connector/db_postgres as psql import nil

type PixelData* = object ## Represents a single pixel of a `Picture`_.
  r*: uint8
  g*: uint8
  b*: uint8

type Picture* = object
  w*: int
  h*: int
  data*: seq[PixelData]

type Database* = object
  ## A database connection handle, which also keeps track of prepared statements.
  conn*: psql.DbConn
  prepStmts*: HashSet[string]
    ## Existing prep statements, keeping track of them in the application prevents
    ## "already defined" errors and having to query the DB for defined prep statements.

proc `=copy`(a: var Database, b: Database) {.error.}

proc `=destroy`(a: var Database) =
  ## Ensure the db connection is closed etc. etc.
  try:
    if a.conn != nil:
      log.trace("close connection", `from` = getStackTrace())
    psql.close(a.conn)
  except psql.DbError as e:
    log.trace("error closing connection", error = e.message())
  `=destroy`(a.prepStmts)

# Parsed directory data types
type
  WadType* = enum
    Wad
    Pk3
    Wadzip

  DetectedFeature* = enum
    SinglePlayer = "S"
    Race = "R"
    Match = "M"
    Ctf = "F"
    Battle = "B"
    Tutorial = "T"
    Character = "C"
    Follower = "F"
    Lua = "L"

  DetectedFeatures* = set[DetectedFeature]

  Directory* = object
    flat*: int
    tex*: int
    sprite*: int
    soc*: int
    lua*: int
    map*: int
    gfx*: int
    snd*: int
    msc*: int
    readme*: string

  DirectoryRef* = ref Directory

  LevelHeading* = object
    name*: string
    act*: int

  AddonAnalysisInfo* = object
    directory*: Directory
    features*: DetectedFeatures
    levels*: OrderedTable[int, LevelHeading]
    levelPics*: OrderedTable[int, Picture]
    thumbnail*: Opt[Picture]

# Derived from database tables
type
  User* = object ## Corresponds to `UserTableName`_.
    id*: Opt[int]
    name*: string
    pfpHash*: Opt[int64]
    joinDate*: times.DateTime
    lastSeen*: times.DateTime

  UserStats* = object
    addonCount*: int
    reviewCount*: int

  Addon* = object ## Corresponds to `AddonTableName`_.
    id*: Opt[int]
    name*: string
    synopsis*: string
    description*: Opt[string]
    author*: string
    authorId*: Opt[int]
    uploadDate*: times.DateTime
    lastUpdated*: times.DateTime
    previewHash*: int64

  ReviewReply* = object
    id*: int
    reviewer*: string
    associatedReviewerId*: Opt[int]
    reviewDate*: times.DateTime
    content*: string

  Review* = object
    id*: int
    addonId*: int
    reviewer*: string
    associatedReviewerId*: Opt[int]
    rating*: int
    reviewDate*: times.DateTime
    content*: string
    replies*: seq[ReviewReply]

  AddonGalleryImage* = object
    addonId*: int
    name*: string
    description*: string
    imageHash*: int64

  AddonVersionInfo* = object ## Corresponds to `AddonVersionTableName`_.
    hash*: int
    addonId*: int
    physicalFileName*: string
    virtualFileName*: string
    fileType*: WadType
    versionString*: string
    fileSize*: int
    releaseDate*: times.DateTime
    uploaderId*: Opt[int]
    releaseNotes*: Opt[string]
    flats*: int
    texs*: int
    sprites*: int
    socs*: int
    luas*: int
    maps*: int
    gfxs*: int
    snds*: int
    mscs*: int
    readme*: string

  Session* = object ## Corresponds to `SessionTableName`_.
    isCreated*: bool ## For keeping track in app, no database equivalent exists.
    id*: string
    userId*: Opt[int]
    expiry*: Opt[times.DateTime]

type
  UserRef* = ref User
  AddonRef* = ref Addon
  ReviewRef* = ref Review
  AddonGalleryImageRef* = ref AddonGalleryImage
  AddonVersionInfoRef* = ref AddonVersionInfo
  SessionRef* = ref Session

# display types
type
  NewsKind* = enum
    Announcement
    Release
    Maintenance

  NewsBlurb* = object ## Render-friendly representation of a news bulletin.
    id*: int
    kind*: NewsKind
    headline*: string
    synopsis*: string
    posterId*: int
    posterName*: string
    postTime*: times.DateTime

  AddonBlurb* = object ## Render-friendly representation of an add-on entry.
    id*: int
    name*: string
    version*: string
    imageUrl*: string
    imageAlt*: string
    authorId*: Opt[int]
    author*: string
    synopsis*: string
    lastUpdateTime*: times.DateTime
    latestFileSize*: int

type
  TermType* = enum
    AddonTerm = "addon"
    LevelTerm = "level"
    AddonOrLevelTerm = "addon-or-level"

  SearchQuery* = object
    term*: Opt[string] = Opt.none(string)
    termType*: TermType
    author*: Opt[string] = Opt.none(string)
    tags*: Opt[seq[string]] = Opt.none(seq[string])
    numOfResults*: Opt[int] = Opt.none(int)
    showResultsAfter*: Opt[int] = Opt.none(int)

type RespType* = tuple[code: httpcore.HttpCode, body: string]
  ## Standard response type for easy returning of HTTP codes.

type FlashMessage* = enum
  ## Types of messages that can be displayed to the user.
  UnknownError = "Unknown error"
  UploadSuccess = "Upload successful"
  AddOnNameMissing = "Add-on name missing"
  VersionNumberMissing = "Version number missing"
  SynopsisMissing = "Must provide synopsis"
  AddonTargetMissing = "Missing add-on target"
  FileMissing = "File missing"
  MustAgreeGuideline = "Must agree to submission guidelines"
  NotAMultipartForm = "Not a multipart form"
  BlankFileName = "No file uploaded"
  UnsupportedExtension = "Unsupported extension, must be either .wad or .pk3"
  CannotUploadFile = "Unable to upload file"
  InvalidFile = "Not a valid file"
  FileTooBig = "File too big"
  InvalidSession = "Invalid session, please try again"
  InvalidTarget = "Invalid addon target"
  AuthorMissing = "Author not specified"
  AddOnExists = "An add-on of this name already exists"
  InvalidEditCode = "Edit code invalid"
  CannotEditAddon = "Unable to edit add-on"
  AddOnNameTooLong = "Add-on name is too long!"
  CannotParseDescription = "Can't format description, there might be invalid syntax"
  CannotParseChangeLog = "Can't format change log, there might be invalid syntax"
  NoUsername = "No user name specified"
  NoPassword = "No password specified"
  PasswordsDontMatch = "Password confirmation doesn't match"
  InvalidFormType = "Invalid form type"
  UsernameTooLong = "Username must not be longer than 32 characters"
  InvalidUsernameSpecs = "Username allowed characters: Alphanumeric, space, dot."
  PasswordTooShort = "Password must not be shorter than 8 characters"
  CannotRegister = "Unable to register, please try again later"
  UsernameExists = "This username is already taken"
  InvalidLogin = "Username or password invalid"

type AddonTarget* = enum
  ## Possible SRB2 versions an addon can be made for.
  Srb2020200 = "srb2-2.2"
  Srb2020100 = "srb2-2.1"
  Srb2020000 = "srb2-2.0"
  Srb2010900 = "srb2-1.09"
  Srb2010700 = "srb2-1.07"
  Srb2010100 = "srb2-1.01"
  Srb2Demo4 = "srb2-demo4"
  Srb2Demo3 = "srb2-demo3"
  Srb2Demo2 = "srb2-demo2"
  Srb2Demo1 = "srb2-demo1"
  Srb2k010000 = "srb2k-1.0"
  Drrr020000 = "drrr-2.0"
