import results
import ./log

from std/os import nil
from std/strutils import nil
from ../config import nil

export results except err

type Failable* = Result[void, string]
type Res*[T] = Result[T, string]

proc logStackTrace*() =
  {.cast(noSideEffect).}:
    if (let enabled = os.getEnv(config.BacktracesEnvString); len(enabled) < 1):
      return
    let st = strutils.splitLines(getStackTrace())
    for t in 0 ..< len(st):
      if t == len(st) - 2:
        # remove self call
        break
      debugEcho st[t]

template message*(e: ref Exception): string =
  ## Renders the exception message as ``Exception: Some message here``.
  runnableExamples:
    let e = newException(Exception, "hey!")
    assert(e.message() == "Exception: hey!")
  $e.name & ": " & e.msg

template unwrap*(v: untyped): untyped =
  when defined(release) or defined(danger):
    v.unsafeGet()
  else:
    v.get()

template unwrapErr*(v: untyped): untyped =
  when defined(release) or defined(danger):
    v.unsafeError()
  else:
    v.error()

# If you're wondering why I'm repeating myself, that's because it's
# gonna get fucky if I call templates within templates in an effort
# to get optional backtraces.

template convertErr*(e: untyped): untyped =
  logStackTrace()
  let i = instantiationInfo()
  if (let enabled = os.getEnv(config.BacktracesEnvString); len(enabled) > 0):
    debugEcho i.filename & "(" & $i.line & "," & $i.column & ")"
  results.err(e.unwrapErr())

template `%`*[T](v: Opt[T]): untyped =
  if v.isSome():
    v.unwrap()
  else:
    logStackTrace()
    let i = instantiationInfo()
    if (let enabled = os.getEnv(config.BacktracesEnvString); len(enabled) > 0):
      debugEcho i.filename & "(" & $i.line & "," & $i.column & ")"
    return results.err("no value exists")

template err*(t: typedesc, v: untyped): untyped =
  logStackTrace()
  let i = instantiationInfo()
  if (let enabled = os.getEnv(config.BacktracesEnvString); len(enabled) > 0):
    debugEcho i.filename & "(" & $i.line & "," & $i.column & ")"
  results.err(t, v)

template err*(v: untyped): untyped =
  logStackTrace()
  let i = instantiationInfo()
  if (let enabled = os.getEnv(config.BacktracesEnvString); len(enabled) > 0):
    debugEcho i.filename & "(" & $i.line & "," & $i.column & ")"
  results.err(v)

template initErr*(t: typedesc, v: untyped): untyped =
  results.err(t, v)
