import forkygs/multipartserver
import ./log
import ./datatypes
import ./dbmanager
import ./resutils

from os import sleep

type TbContext = ref object of MultipartContext
  database: ref Database

{.push gcsafe, raises: [].}

template mpcontext*(): untyped =
  TbContext(socketcontext)

template getCurrentDbConnection*(): ref Database =
  mpcontext().database

proc multipartInitProc*(g: GuildenServer) =
  socketcontext = new TbContext
  handleMultipartInitialization(g)
  var database = connectToPgDb()
  var backoff = 2000
  while not database.isOk():
    # keep trying
    log.fatal("cannot connect to db", error = database.unwrapErr())
    log.info("completely suspending thread", backoff = backoff)
    # use `sleep` instead of `suspend` - this will disallow other threads to run
    # and risk a SIGSEGV (since database is still NULL)
    sleep(backoff)
    backoff *= 2
    database = connectToPgDb()
  log.info("successfully connected to database")
  mpcontext().database = database.unwrap()

{.pop.}
