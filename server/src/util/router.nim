import std/macros
import std/tables
import std/genasts

import std/strtabs
export strtabs

import std/strutils
export strutils

import regex
export regex

type RouterHttpMethods = enum
  Get = "GET"
  Head = "HEAD"
  Post = "POST"
  Put = "PUT"
  Delete = "DELETE"
  Options = "OPTIONS"
  Patch = "PATCH"

macro makeRouter*(name: string, body: untyped) =
  ##[
  Creates a router proc of the following form:

  ```nim
  proc routerProcName(reqType: string; path: string): void
  ```

  Where `name` will replace `routerProcName`. The `reqType` that
  this new proc will accept is the following (all **uppercase**):
    1. GET
    2. HEAD
    3. POST
    4. PUT
    5. DELETE
    6. OPTIONS
    7. PATCH

  When defining a router, you will define a path for the reqType
  using **lowercase**, e.g. to define `GET` for `/` and `PUT` for `/obj`, you do:

  ```nim
  makeRouter("test"):
    get "/":
      # your stuff here
      discard
    put "/obj":
      # ditto
      discard
    default:
      # ...
      discard
  ```

  You must define a `default` path, when the route for a URL is not
  found.

  You may optionally define a `methodNotAllowed` path, for when
  the URL is accessed not through one of the defined methods.

  You can also define "dynamic" paths that capture URL parameters
  surrounded in braces. The URL parameters will be made available
  via the `pathParams` variable as a string, like this:

  ```nim
  makeRouter("test2"):
    get "/posts/{postId}":
      let id = pathParams.getOrDefault("postId")
      echo "got post ID: " & id
  ```

  Other variables made available to you include `pathOnly` and `getParams`.

  When the path is `/test/abc?id=10&def=qwerty`:

  * `pathOnly` = `/test/abc`
  * `getParams` = `id=10&def=qwerty`

  `-d:routerMacroDbg` shows the generated proc and some internal info at compile-time

  `-d:routerMacroRuntimeDbg` adds some `debugEcho` stuff showing the name of the route,
  the captured pathParams and getParams.
  ]##
  runnableExamples:
    var output: string = ""
    makeRouter("routeThisPhrhrht"):
      get "/":
        output = "got /"
      get "/upload":
        output = "loaded /upload"
      post "/upload":
        output =
          "uploaded something" & (
            if len(getParams) > 0:
              " with params: " & getParams
            else:
              ""
          )
      default:
        output = "unknown"
      methodNotAllowed:
        output = "whoops!"
    routeThisPhrhrht("GET", "/")
    assert output == "got /"
    routeThisPhrhrht("GET", "/upload")
    assert output == "loaded /upload"
    routeThisPhrhrht("POST", "/upload")
    assert output == "uploaded something"
    routeThisPhrhrht("POST", "/upload?aaaaaa=1231223&&&&11rrrdsaop3r")
    assert output == "uploaded something with params: aaaaaa=1231223&&&&11rrrdsaop3r"
    routeThisPhrhrht("GET", "/eogijoergjioerajgieog")
    assert output == "unknown"
    routeThisPhrhrht("PUT", "/upload")
    assert output == "whoops!"

  when defined(routerMacroRuntimeDbg):
    debugEcho "===== WARNING: RUNTIME ECHO IS ENABLED ======"

  var staticRoutes: Table[string, seq[(RouterHttpMethods, NimNode)]]
  var dynamicRoutes: Table[string, seq[(RouterHttpMethods, NimNode)]]
  var defaultRoutine = newEmptyNode()
  var methodNotAllowedRoutine = newEmptyNode()

  # Perform AST analysis
  for b in body:
    case b.kind
    of nnkCommand: # command(ident<method>, strlit, stmtlist)
      #[
        Statements of the form:
          get "/":
            <statements>
          post "/upload":
            <statements>
      ]#
      let
        methodName = b[0].strVal
        url = block: # clean up url
          let i = b[1].strVal
          if len(i) > 1 and i[^1] == '/':
            let j = i[0 ..^ 2]
            when defined(routerMacroDbg):
              debugEcho "===== router URL cleaned ========"
              debugEcho "was:  " & i
              debugEcho "into: " & j
            j
          else:
            i
        list = b[2]
        newTup = (parseEnum[RouterHttpMethods](methodName.toUpperAscii()), list)
      var isDynamicRoute = false
      for i in url:
        if i == '{':
          isDynamicRoute = true
          break
      if isDynamicRoute:
        if not (url in dynamicRoutes):
          dynamicRoutes[url] = @[newTup]
        else:
          dynamicRoutes[url].add(newTup)
      else: # is static route
        if not (url in staticRoutes):
          staticRoutes[url] = @[newTup]
        else:
          staticRoutes[url].add(newTup)
    of nnkCall: # call(ident"others", stmtlist)
      #[
        Statements of the form:
          default:
            <statements>
      ]#
      case b[0].strVal
      of "default":
        defaultRoutine = b[1]
      of "methodNotAllowed":
        methodNotAllowedRoutine = b[1]
    else:
      discard

  when defined(routerMacroDbg):
    debugEcho "============= STATIC ROUTES ============="
    for k, v in pairs(staticRoutes):
      debugEcho k & ":"
      for m in v:
        let (mtd, node) = m
        debugEcho "  " & $mtd & " => " & node.repr.repr.replace("\n", "")
        debugEcho()
    debugEcho "============= DYNAMIC ROUTES ============="
    for k, v in pairs(dynamicRoutes):
      debugEcho k & ":"
      for m in v:
        let (mtd, node) = m
        debugEcho "  " & $mtd & " => " & node.repr.repr.replace("\n", "")
        debugEcho()

  # Generate a regex pattern from all the dynamic routes
  var genRegex = ""
  if len(dynamicRoutes) > 0:
    var counter = 0
    const paramConverter = re2"""\{(\w+)\}"""
    when defined(routerMacroDbg):
      debugEcho "============= DYNAMICROUTE NAMES ============"
    for url in keys(dynamicRoutes):
      # Add a case for the dynamic route
      when defined(routerMacroDbg):
        debugEcho "Route" & $counter & " => " & url
      genRegex &= "(?P<Route"
      genRegex &= $counter
      genRegex &= ">"
      genRegex &=
        url.replace(
          paramConverter,
          (
            proc(m: RegexMatch2, s: string): string =
              # turn "{id}" into "(?P<RouteXX_id>[^/]+?)"
              "(?P<Route" & $counter & "_" & s[m.group(0)] & ">[^/]+?)"
          ),
        )
      genRegex &= "$)|"
      inc(counter)
    # cut the final OR symbol
    genRegex = genRegex[0 ..< (len(genRegex) - 1)]
  when defined(routerMacroDbg):
    debugEcho "============= GENERATED REGEX PATTERN ============="
    debugEcho genRegex

  # Generate default handler proc
  var defaultProc = genAst(
    r = ident("reqType"),
    po = ident("pathOnly"),
    g = ident("getParams"),
    pp = ident("pathParams"),
    d = defaultRoutine,
  ):
    proc notFound(r, po, g: string, pp: StringTableRef): void =
      d

  let defaultProcCall = genAst(
    r = ident("reqType"),
    po = ident("pathOnly"),
    g = ident("getParams"),
    pp = ident("pathParams"),
  ):
    notFound(r, po, g, pp)

  # Generate invalid method proc, if any
  var methodNotAllowedProc = block:
    if methodNotAllowedRoutine.kind != nnkEmpty:
      genAst(
        r = ident("reqType"),
        po = ident("pathOnly"),
        g = ident("getParams"),
        pp = ident("pathParams"),
        d = methodNotAllowedRoutine,
      ):
        proc methodNotAllowed(r, po, g: string, pp: StringTableRef): void =
          d

    else:
      newEmptyNode()
  let methodNotAllowedCall = genAst(
    r = ident("reqType"),
    po = ident("pathOnly"),
    g = ident("getParams"),
    pp = ident("pathParams"),
  ):
    methodNotAllowed(r, po, g, pp)

  # Build switch statement for static routes
  var routeSwitch = nnkCaseStmt.newTree(ident("pathOnly"))
  for routeName, routeBody in pairs(staticRoutes):
    var rqSwitch = nnkCaseStmt.newTree(ident("reqType"))
    for i in routeBody:
      let (rqKind, rqRoutine) = i
      rqSwitch.add(nnkOfBranch.newTree(newStrLitNode($rqKind), rqRoutine))
    rqSwitch.add(
      nnkElse.newTree(
        if methodNotAllowedRoutine.kind != nnkEmpty:
          methodNotAllowedCall
        else:
          defaultProcCall
      )
    )
    routeSwitch.add(
      nnkOfBranch.newTree(newStrLitNode(routeName), newStmtList(rqSwitch))
    )

  var dynamicRouteRoutine = newStmtList(newCommentStmtNode("dynamic path fallback"))
  if len(genRegex) > 0:
    # Common variables
    let matchedRouteHolderName = genSym(nskVar, "matchedRoute")

    # Case statement
    let matchFoundCaseStmts = nnkCaseStmt.newTree(matchedRouteHolderName)
    var counter = 0
    for k, v in pairs(dynamicRoutes):
      var matchFoundMtdCases = newStmtList()
      matchFoundCaseStmts.add:
        nnkOfBranch.newTree(newLit("Route" & $counter), matchFoundMtdCases)
      when defined(routerMacroRuntimeDbg):
        matchFoundMtdCases.add:
          genAst(
            which = newLit("Route" & $counter),
            getParams = ident("getParams"),
            pathParams = ident("pathParams"),
          ):
            debugEcho("=== CURRENT ROUTE => ", which)
            debugEcho("=== PATH PARAMS => ", pathParams)
            debugEcho("=== GET PARAMS => ", getParams)
      matchFoundMtdCases.add:
        nnkCaseStmt.newTree(ident("reqType"))
      for endpoint in v:
        let (rq, content) = endpoint
        matchFoundMtdCases[^1].add:
          nnkOfBranch.newTree(newLit($rq), content)
      matchFoundMtdCases[^1].add:
        nnkElse.newTree(
          newStmtList(
            newCommentStmtNode("invalid method"),
            (
              if methodNotAllowedRoutine.kind != nnkEmpty:
                methodNotAllowedCall
              else:
                defaultProcCall
            ),
          )
        )
      inc(counter)
    matchFoundCaseStmts.add:
      nnkElse.newTree(
        newStmtList(newCommentStmtNode("something went wrong"), defaultProcCall)
      )
    let dynRouteContents = genAst(
      r = ident("reqType"),
      po = ident("pathOnly"),
      g = ident("getParams"),
      pp = ident("pathParams"),
      mr = matchedRouteHolderName,
      giantRegex = nnkCallStrLit.newTree(ident("re2"), newLit(genRegex)),
      dynRouteLen = len(dynamicRoutes),
      caseStmts = matchFoundCaseStmts,
    ):
      const rexp = giantRegex
      var m: RegexMatch2
      if match(po, rexp, m):
        let groupNames = groupNames(m)
        var mr = ""
        ## Find the first route that matches the URL
        for i in 0 ..< dynRouteLen:
          let routeNameCandidate = "Route" & $i
          try:
            if m.group(routeNameCandidate) != reNonCapture:
              mr = routeNameCandidate
              break
          except KeyError:
            ## Exit here, since there is no route that matches
            notFound(r, po, g, pp)
            return
        if len(mr) > 1:
          ## Insert the captured path parameters
          for i in groupNames:
            if i.startsWith(mr) and len(i) > len(mr):
              try:
                pp[i[(len(mr) + 1) ..^ 1]] = po[m.group(i)]
                discard
              except KeyError:
                ## Should we ignore non-matching?
                discard
          caseStmts
        else:
          ## Nothing REALLY matched
          notFound(r, po, g, pp)
      else:
        ## Nothing matched
        notFound(r, po, g, pp)
    dynamicRouteRoutine.add(dynRouteContents)
  else:
    dynamicRouteRoutine.add(defaultProcCall)

  routeSwitch.add(nnkElse.newTree(dynamicRouteRoutine))

  # build the part where the path gets split
  let splitPath = block:
    genAst(
      path = ident("path"), pathOnly = ident("pathOnly"), getParams = ident("getParams")
    ):
      ## Calculate where the path should split
      var startPos = -1
      for i in 0 ..< len(path):
        case path[i]
        of '?', '&':
          startPos = i
          break
        else:
          discard
      ## Accessible path and get params
      let pathOnly = block:
        var p =
          if startPos > -1:
            path[0 ..< startPos]
          else:
            path
        ## Clean up trailing slash
        if len(p) > 1 and p[^1] == '/':
          p[0 ..^ 2]
        else:
          p
      let getParams = block:
        if startPos > -1:
          path[(startPos + 1) ..^ 1]
        else:
          ""

  let pathParamsDefine = nnkVarSection.newTree(
    nnkIdentDefs.newTree(
      ident("pathParams"), newEmptyNode(), newCall(ident("newStringTable"))
    )
  )

  # step 4. build the final router proc
  let builtProc = genAst(
    routerName = ident(name.strVal),
    reqType = ident("reqType"),
    path = ident("path"),
    routerBody = newStmtList(
      defaultProc, methodNotAllowedProc, pathParamsDefine, splitPath, routeSwitch
    ),
  ):
    proc routerName(reqType, path: string): void =
      routerBody

  when defined(routerMacroDbg):
    debugEcho "======BUILT========="
    debugEcho builtProc.repr

  builtProc

when isMainModule:
  makeRouter("route"):
    get "/":
      discard
    get "/{id}":
      discard
    get "/{id}/edit":
      discard
    post "/{id}/edit":
      discard
    get "/{id}/add":
      discard
    post "/{id}/add":
      discard
    get "/{id}/updates":
      discard
    post "/{id}/review":
      discard
    default:
      discard
    methodNotAllowed:
      discard

  import std/times
  template run(which) =
    var s = cpuTime()
    for i in 0 .. 1_000_000:
      route("GET", which)
    echo "GET " & which, " --> ", (cpuTime() - s)
    s = cpuTime()
    for i in 0 .. 1_000_000:
      route("POST", which)
    echo "POST " & which, " --> ", (cpuTime() - s)
    s = cpuTime()
    for i in 0 .. 1_000_000:
      route("PATCH", which)
    echo "PATCH " & which, " --> ", (cpuTime() - s)
    s = cpuTime()
    for i in 0 .. 1_000_000:
      route("DELETE", which)
    echo "DELETE " & which, " --> ", (cpuTime() - s)

  run("/")
  run("/1235")
  run("/1235/edit")
  run("/1235/edit/")
  run("/1235/edit/?wfwe=25tt")
  run("/1235/add")
  run("/1235/add/")
  run("/1235/updates")
  run("/1235/updates/")
  run("/1235/review")
  run("/1235/review/")
  run("/ewffwe/")
  run("/32032jc3/?wfwe=25tt")
