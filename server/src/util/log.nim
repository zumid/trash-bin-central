import chronicles

type LogRecord[Output] = object
  output*: Output

template initLogRecord*(r: var LogRecord, lvl: LogLevel, topics: string, name: string) =
  r.output.append(
    case lvl
    of TRACE: "(T) "
    of DEBUG: "(D) "
    of NOTICE: "(?) "
    of WARN: "(!) "
    of ERROR: "(E) "
    of FATAL: "(X) "
    of INFO: "(I) "
    of NONE: ""
  )
  if topics != "":
    r.output.append("[" & topics & "] ")
  r.output.append(name & ". ")

template setProperty*(r: var LogRecord, key: string, val: auto) =
  r.output.append(key & ": " & $val & ". ")

template setFirstProperty*(r: var LogRecord, key: string, val: auto) =
  r.setProperty key, val

template flushRecord*(r: var LogRecord) =
  r.output.append("\n")
  r.output.flushOutput

customLogStream logfmt[LogRecord[StdOutOutput]]

publicLogScope:
  stream = logfmt
