# unqualified imports
import std/times
import std/strtabs
import ./util/datatypes
import ./util/resutils
import ./util/log
import ./util/dbmanager # template usage means this has to be imported too

# partially-qualified imports
from std/httpcore as httpcore import `$`
from nimja/parser import compileTemplateFile
from ./util/db2object/session_user import getSessionUser

# import a lot of stuff from here
from ./util/helper/strhelper import escape, anchorize

# qualified imports
from std/os import nil
from std/strutils import nil
from forkygs/httpserver import nil
from ./config import nil
from ./util/db2object/addon_info import nil
from ./util/db2object/addon_downloads import nil
from ./util/db2object/addon_reviews import nil
from ./util/db2object/sessions import nil
from ./util/parse/bbcode import nil
from ./util/tbcontext import nil

proc getBaseUrl(): string =
  if (let url = os.getEnv(config.ServerUrlEnvString); len(url) > 0): url else: ""

proc fuzzyTime(t: DateTime): string =
  let interval = t.between(now())
  if interval.years > 0:
    return $interval.years & "y"
  elif interval.months > 0:
    return $interval.months & "mo"
  elif interval.weeks > 0:
    return $interval.weeks & "w"
  elif interval.days > 0:
    return $interval.days & "d"
  elif interval.hours > 0:
    return $interval.hours & "h"
  elif interval.minutes > 0:
    return $interval.minutes & "m"
  else:
    return "recent"

proc getToasts(session: Res[SessionRef]): seq[string] =
  result = @[]
  if session.isOk():
    let flashResult = sessions.displayFlashMessages(session.unwrap().id)
    if flashResult.isOk():
      return flashResult.unwrap()
    else:
      log.error("can't show flash messages", error = flashResult.unwrapErr())

proc extractAcsrf(session: Res[SessionRef]): string =
  result = ""
  if session.isOk():
    let s = sessions.createAntiCsrfToken(session.unwrap().id)
    if s.isOk():
      return s.unwrap()

proc bbCode2Html*(input: string): string =
  let r = bbcode.bbCode2HtmlResult(input)
  if not r.isOk():
    "<div class=\"bbcode-error\">Cannot render bbcode (" & r.unwrapErr() & ")</div><pre>" &
      escape(input) & "</pre>"
  else:
    r.unwrap()

template initHeaderData() {.dirty.} =
  let toasts = session.getToasts()
  let db = tbcontext.getCurrentDbConnection()
  let activeUser = db.getSessionUser(session)
  let logoutAcsrf =
    if session.isOk() and activeUser.isSome():
      session.extractAcsrf()
    else:
      ""

proc Error*(
    code: httpcore.HttpCode, addinfo: string = "", session: Res[SessionRef]
): string =
  let heading = "HTTP " & $int(code)
  var message = ($code)[4 ..^ 1]
  if len(addinfo) > 0:
    message &= ", " & addinfo
  initHeaderData()
  compileTemplateFile("generic_error.html", baseDir = config.HtmlTemplateDir)

proc Landing*(
    addonsLatest: seq[AddonBlurb] = @[],
    addonsRanked: seq[AddonBlurb] = @[],
    randomAddons: seq[AddonBlurb] = @[],
    session: Res[SessionRef],
): string =
  initHeaderData()
  compileTemplateFile("landing.html", baseDir = config.HtmlTemplateDir)

proc Addons*(addonsLatest: seq[AddonBlurb] = @[], session: Res[SessionRef]): string =
  initHeaderData()
  compileTemplateFile("addons.html", baseDir = config.HtmlTemplateDir)

proc SearchPage*(
    session: Res[SessionRef],
    addons: seq[AddonBlurb],
    totalResults: int,
    nextUrl: Opt[string],
): string =
  initHeaderData()
  compileTemplateFile("search.html", baseDir = config.HtmlTemplateDir)

proc hashToHex(hash: int): Res[string] =
  try:
    ok(strutils.toLowerAscii(strutils.toHex(hash)))
  except CatchableError as e:
    err(e.message())

proc AddonInfo*(
    addon: Addon,
    addonTags: seq[string],
    levelList: seq[string],
    versions: seq[AddonVersionInfo],
    session: Res[SessionRef],
    reviews: seq[Review],
    previewImage: Opt[string] = Opt.none(string),
): string =
  ## .. warning::
  ##    Addon is assumed to have an ID

  # let id = addon.id.unwrap()
  let latest = versions[0]
  let acsrf = session.extractAcsrf()
  initHeaderData()
  compileTemplateFile("addon_info.html", baseDir = config.HtmlTemplateDir)

proc AddonUpdates*(versions: seq[AddonVersionInfo], session: Res[SessionRef]): string =
  initHeaderData()
  compileTemplateFile("addon_updates.html", baseDir = config.HtmlTemplateDir)

proc Register*(session: Res[SessionRef]): string =
  let acsrf = session.extractAcsrf()
  initHeaderData()
  compileTemplateFile("register.html", baseDir = config.HtmlTemplateDir)

proc Login*(session: Res[SessionRef], priorInput: StringTableRef): string =
  let acsrf = session.extractAcsrf()
  initHeaderData()
  compileTemplateFile("login.html", baseDir = config.HtmlTemplateDir)

proc Upload*(session: Res[SessionRef], priorInput: StringTableRef): string =
  let acsrf = session.extractAcsrf()
  initHeaderData()
  compileTemplateFile("upload.html", baseDir = config.HtmlTemplateDir)

proc AddonAddVersion*(
    session: Res[SessionRef], priorInput: StringTableRef, showEditCodeField: bool
): string =
  let acsrf = session.extractAcsrf()
  initHeaderData()
  compileTemplateFile("addon_add_version.html", baseDir = config.HtmlTemplateDir)

proc AddonEdit*(
    session: Res[SessionRef], priorInput: StringTableRef, showEditCodeField: bool
): string =
  let acsrf = session.extractAcsrf()
  initHeaderData()
  compileTemplateFile("addon_edit.html", baseDir = config.HtmlTemplateDir)

proc RedirectPlaceholder*(location: string): string =
  compileTemplateFile("redirect_placeholder.html", baseDir = config.HtmlTemplateDir)

proc LatestReviews*(session: Res[SessionRef], reviews: seq[Review]): string =
  initHeaderData()
  compileTemplateFile("reviews.html", baseDir = config.HtmlTemplateDir)

proc BBCodePreview*(input: string): string =
  compileTemplateFile("bbcode_test.html", baseDir = config.HtmlTemplateDir)

proc UserPage*(
    userInfo: User,
    userStats: UserStats,
    addonList: seq[AddonBlurb],
    reviewList: seq[Review],
    session: Res[SessionRef],
): string =
  initHeaderData()
  compileTemplateFile("user.html", baseDir = config.HtmlTemplateDir)
