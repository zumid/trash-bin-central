##
## Handle raw HTTP requests.
##

# Unqualified imports
import std/strtabs
import ./util/log
import ./util/resutils

# Partially qualified imports
from std/strutils import join
from forkygs/guildenserver import GuildenSternVersion
from ./util/helper/strtab_to_list import asList

# Qualified imports
from std/nativesockets as socks import nil
from forkygs/httpserver as http import nil
from ./config import nil
from ./util/errorpages as ep import nil
from ./util/parse/http_path as path import nil
from ./util/db2object/sessions import nil

# Controllers
from std/os import nil
from ./controller/AddonController import nil
from ./controller/SearchController import nil
from ./controller/IndexController import nil
from ./controller/AssetController import nil
from ./controller/UploadController import nil
from ./controller/ReviewController import nil
from ./controller/BBCodeController import nil
from ./controller/LoginController import nil
from ./controller/LogoutController import nil
from ./controller/UserPageController import nil
from ./controller/APIController import nil

template getRequestContext(): untyped =
  ## Literally just what http() should have been called.
  http.http()

proc handle*() {.gcsafe, raises: [].} =
  ##[
  Request handler. Parses the URI and then hands it off to controllers.
  ]##
  let
    mtd = http.getMethod()
    uri = http.getUri()
    requestHeaders = getRequestContext().headers
    requestIp =
      try:
        {.warning: "Assuming IPv4".}
        when not defined(nimdoc):
          # nimdocs freaks out at this for whatever reason
          let i = socks.getPeerAddr(http.socketcontext.socket, socks.AF_INET)
          Opt[string].some(i[0])
        else:
          Opt.none(string)
        # Client port info isn't necessary...
      except OsError as e:
        log.error("can't get peer address", e = e.msg)
        Opt.none(string)

  let reqHeadList = requestHeaders.asList()
  log.debug(
    "Request",
    `method` = mtd,
    uri = uri,
    headers = reqHeadList,
    ip = (
      if requestIp.isSome():
        requestIp.unwrap()
      else:
        "unknown"
    ),
  )

  var responseHeaders: seq[string] = @[]

  # At the first opportunity, reject connection if it comes from another
  # domain than the ones we intended
  if (
    let
      myHosts = block:
        var x: seq[string]
        for i in strutils.split(os.getEnv(config.DomainEnvString), ','):
          x.add(strutils.strip(i))
        x
      host = requestHeaders.getOrDefault("host")
    var found: bool = false
    for i in myHosts:
      if len(i) < 1: continue
      if host == i:
        found = true
        break
    not found
  ):
    let body =
      """<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>400 Bad Request</title>
    <meta name="viewport" content="initial-width=device-width,initial-scale=1">
    <style>
      body { text-align: center; }
    </style>
  </head>
  <body>
    <h1>400 Bad Request</h1>
  </body>
</html>"""
    http.reply(http.Http400, body, responseHeaders)
    return

  let sess = sessions.getOrMakeSession(requestHeaders, responseHeaders)

  # parse request URI
  let pathParts = block:
    let parts = path.parse(uri)
    if not parts.isOk():
      ep.replyAndTerminateWithError(
        http.Http400, "cannot parse URI", "invalid link", parts.unwrapErr(), sess
      )
    parts.unwrap()

  when false:
    let pathQ = (parts: pathParts.path, query: pathParts.query.asList())
    log.debug("got path", path = pathQ)

  var paths = pathParts.path

  # set server ID, probably not a good idea
  const serverId =
    "Server: Trash-Bin-Central/" & config.TrashBinCentralVersion & " Guildenstern/" &
    GuildenSternVersion & " Nim/" & NimVersion
  responseHeaders.add(serverId)

  let routeName = path.pop(paths)

  # handle requests by method, and then by route.
  # http://localhost:<PORT>/something/1/2
  #                         ^^^^^^^^^
  #                         this part determines which controller is called,
  #                         then the controller is in charge of interpreting
  #                         whatever is after it.

  if routeName.isSome():
    let route = routeName.unwrap()
    case mtd
    of "GET", "HEAD":
      let isHead = mtd == "HEAD"
      case route
      # SPECIAL CASE: upload, download, etc. takes over
      # replying completely, since the files it can transfer are so big
      of "assets":
        AssetController.GET(
          pathParts,
          paths,
          responseHeaders,
          requestHeaders,
          sess,
          kind = AssetController.AssetKind.Asset,
          isHead = isHead,
        )
        return
      of "download":
        AssetController.GET(
          pathParts,
          paths,
          responseHeaders,
          requestHeaders,
          sess,
          kind = AssetController.AssetKind.Download,
          ipAddress = requestIp,
          isHead = isHead,
        )
        return
      of "previews":
        AssetController.GET(
          pathParts,
          paths,
          responseHeaders,
          requestHeaders,
          sess,
          kind = AssetController.AssetKind.Preview,
          isHead = isHead,
        )
        return
      of "favicon.ico":
        AssetController.GET(
          pathParts,
          paths,
          responseHeaders,
          requestHeaders,
          sess,
          kind = AssetController.AssetKind.Favicon,
          isHead = isHead,
        )
        return
      else:
        # REGULAR ROUTES: use the standard mechanism
        let (httpCode, body) =
          case route
          of "addons":
            AddonController.handle(
              mtd, pathParts, paths, responseHeaders, requestHeaders, sess
            )
          of "reviews":
            ReviewController.GET(
              pathParts, paths, responseHeaders, requestHeaders, sess
            )
          of "search":
            SearchController.GET(
              pathParts, paths, responseHeaders, requestHeaders, sess
            )
          of "upload":
            UploadController.GET(
              pathParts, paths, responseHeaders, requestHeaders, sess
            )
          of "bbcode":
            BBCodeController.GET(
              pathParts, paths, responseHeaders, requestHeaders, sess
            )
          of "login":
            LoginController.handle(
              mtd, pathParts, paths, responseHeaders, requestHeaders, sess
            )
          of "user":
            UserPageController.handle(
              mtd, pathParts, paths, responseHeaders, requestHeaders, sess
            )
          of "api":
            APIController.handle(
              mtd, pathParts, paths, responseHeaders, requestHeaders, sess
            )
          else:
            ep.makeErrorTuple(
              http.Http404,
              "invalid GET controller",
              "section not found",
              "no GET controller found for " & route,
              sess,
            )
        if isHead:
          # RFC 2616, 9110 requires a HEAD implementation, which is
          # literally just a GET without the actual contents
          # I don't feel like rearchitecting just for the sake of
          # "not actually handling a GET" for a HEAD request...
          let
            blank = " "
            headerStr = responseHeaders.join("\c\L")
          discard http.reply(httpCode, blank.addr, $len(body), 0, headerStr.addr, false)
        else:
          # GET
          http.reply(httpCode, body, responseHeaders)
    of "POST":
      let (httpCode, body) =
        if routeName.isSome():
          let route = routeName.unwrap()
          case route
          of "upload":
            UploadController.POST(
              pathParts, paths, responseHeaders, requestHeaders, sess
            )
          of "addons":
            AddonController.handle(
              mtd, pathParts, paths, responseHeaders, requestHeaders, sess
            )
          of "login":
            LoginController.handle(
              mtd, pathParts, paths, responseHeaders, requestHeaders, sess
            )
          of "logout":
            LogoutController.POST(
              pathParts, paths, responseHeaders, requestHeaders, sess
            )
          else:
            ep.makeErrorTuple(
              http.Http405,
              "invalid POST controller",
              "section not found",
              "no POST controller found for " & route,
              sess,
            )
        else:
          ep.makeErrorTuple(
            http.Http405, "POST on others", "section not found",
            "POST disallowed for other routes", sess,
          )
      http.reply(httpCode, body, responseHeaders)
    else:
      ep.replyAndTerminateWithError(
        http.Http405,
        "Unimplemented method",
        "unimplemented method",
        "method " & mtd & " not implemented for routes",
        sess,
      )
  else: # root controller
    case mtd
    of "GET", "HEAD":
      let isHead = mtd == "HEAD"
      let (httpCode, body) =
        IndexController.GET(pathParts, paths, responseHeaders, requestHeaders, sess)
      if isHead:
        ## GET is still run only for getting the size of the generated response
        let
          blank = " "
          headerStr = responseHeaders.join("\c\L")
        discard http.reply(httpCode, blank.addr, $len(body), 0, headerStr.addr, false)
      else:
        http.reply(httpCode, body, responseHeaders)
    else:
      ep.replyAndTerminateWithError(
        http.Http405,
        "Unimplemented method",
        "unimplemented method",
        "method " & mtd & " not implemented for root",
        sess,
      )
