import std/sequtils
import ../util/log
import ../util/resutils
import ../util/datatypes

from stew/byteutils import toBytes

from std/streams as stdstrm import nil
from std/random import nil
from std/algorithm import nil
from nimPNG as png import nil
from binstreams as strm import nil

type Offset = object
  x: int
  y: int

type Rectangle = object
  x: int
  y: int
  w: int
  h: int

#### decode patch ####

proc fetchFromPalette(index: int): PixelData =
  const palette = staticRead("srb2.pal").toBytes()
  try:
    return PixelData(
      r: uint8(palette[index * 3]),
      g: uint8(palette[(index * 3) + 1]),
      b: uint8(palette[(index * 3) + 2]),
    )
  except:
    return PixelData(r: 0, g: 0, b: 0)

proc pixelDataToRgb24(p: seq[PixelData]): seq[byte] =
  for pixel in p:
    result.add(pixel.r)
    result.add(pixel.g)
    result.add(pixel.b)

template posToIndex(x, y, width: int): int =
  # x and y are 0-indexed
  (y * width) + x

proc patchToPicture*(patch: seq[byte]): Picture {.raises: [IOError].} =
  # decode pixel data
  let stream = strm.newMemStream(patch, littleEndian)
  defer:
    strm.close(stream)

  result.w = strm.read(stream, uint16).int
  result.h = strm.read(stream, uint16).int
  result.data = newSeq[PixelData](result.w * result.h)

  try:
    # skip left offset
    discard strm.read(stream, uint16)

    # skip top offset
    discard strm.read(stream, uint16)
  except IOError:
    discard

  for postIndex in 0 ..< result.w:
    let postOffset = strm.read(stream, uint32).int

    # wish doPeekMemStream was exposed.
    let prevOffset = strm.getPosition(stream)
    strm.setPosition(stream, postOffset)
    block readPost:
      let topDelta = strm.read(stream, uint8).int
      if topDelta == 0xff:
        break readPost

      let postLength = strm.read(stream, uint8).int

      # padding byte
      discard strm.read(stream, uint8)

      for postRowIndex in 0 ..< postLength:
        let color = strm.read(stream, uint8).int
        let pixPos = posToIndex(postIndex, topDelta + postRowIndex, result.w)

        result.data[pixPos] = fetchFromPalette(color)
    strm.setPosition(stream, prevOffset)
  return result

proc pictureToPng*(picture: Picture): seq[byte] =
  let genPng = png.encodePNG24(pixelDataToRgb24(picture.data), picture.w, picture.h)
  let buffer = stdstrm.newStringStream()
  defer:
    stdstrm.close(buffer)

  png.writeChunks(genPng, buffer)
  stdstrm.setPosition(buffer, 0)

  return stdstrm.readAll(buffer).toBytes()

proc patchToPng*(patch: seq[byte]): seq[byte] =
  return pictureToPng(patchToPicture(patch))

### Generate addon preview ####

{.push raises: [].}

proc pasteImage(base: var Picture, paste: Picture, offset: Offset): Failable =
  ## Pastes image `paste` on top of image `base`.
  if (base.w < 1) or (base.h < 1):
    return err("no base image")

  if (paste.w < 1) or (paste.h < 1):
    return err("no image to paste")

  for rowNum in 0 ..< paste.h:
    for colNum in 0 ..< paste.w:
      let pastePixPos = colNum + (rowNum * paste.w)
      let baseAtPos = ((offset.y + rowNum) * base.w) + (offset.x + colNum)
      # skip out of bounds
      if (baseAtPos < 0) or (baseAtPos >= len(base.data)) or (pastePixPos < 0) or
          (pastePixPos >= len(paste.data)):
        continue
      {.push checks: off.} # checks already done so skip Nim's native ones
      base.data[baseAtPos] = paste.data[pastePixPos]
      {.pop.}

proc crop(input: Picture, cropArea: Rectangle): Picture =
  result = Picture(w: cropArea.w, h: cropArea.h)
  result.data = newSeq[PixelData](result.w * result.h)
  let originPixel = (cropArea.y * input.w) + cropArea.x
  var i = 0
  for rowNum in 0 ..< (cropArea.h):
    for colNum in 0 ..< (cropArea.w):
      result.data[i] = input.data[originPixel + colNum + (rowNum * input.w)]
      inc(i)
  return result

proc doubleUp(input: Picture): Picture =
  ## Resize image to 2x scale
  result = Picture(w: input.w * 2, h: input.h * 2)
  result.data = newSeq[PixelData](result.w * result.h)
  var resPixelIndex = 0
  var imgPixelIndex = 0
  for rowNum in 0 ..< (input.h):
    for count in 0 .. 1: # double up the rows
      for colNum in 0 ..< (input.w): # double up the columns
        result.data[resPixelIndex] = input.data[imgPixelIndex + colNum]
        inc(resPixelIndex)
        result.data[resPixelIndex] = input.data[imgPixelIndex + colNum]
        inc(resPixelIndex)
    inc(imgPixelIndex, input.w)
  return result

proc arrangeImagesCommon(
    pickedIndices: var openArray[int],
    levelPics: OrderedTable[int, Picture],
    finalImage: var Picture,
): void {.raises: [KeyError].} =
  let keyList = algorithm.sorted(levelPics.keys.toSeq())
  case len(keyList)
  of 0: # no images registered
    return
  # if levsel images are 1 - 4 images
  # list them in order
  of 1:
    pickedIndices[0] = keyList[0]
  of 2:
    pickedIndices[0] = keyList[0]
    pickedIndices[1] = keyList[1]
  of 3:
    pickedIndices[0] = keyList[0]
    pickedIndices[1] = keyList[1]
    pickedIndices[2] = keyList[2]
  of 4:
    pickedIndices[0] = keyList[0]
    pickedIndices[1] = keyList[1]
    pickedIndices[2] = keyList[2]
    pickedIndices[3] = keyList[3]
  else:
    # fetch them randomly if more than 4
    var rand = random.initRand()
    let `range` = 0 ..< len(keyList)
    for i in 0 ..< len(pickedIndices):
      var pickedIndex = keyList[random.rand(rand, `range`)]
      while pickedIndex in pickedIndices:
        pickedIndex = keyList[random.rand(rand, `range`)]
      pickedIndices[i] = pickedIndex
  log.trace("picked level indices", indices = pickedIndices)

  # Place images
  var imageNum = 0
  for levelIndex in pickedIndices:
    # assume a "-1" result is unreachable
    assert(levelIndex > 0)
    # do place
    case len(levelPics)
    of 1:
      discard finalImage.pasteImage(
        levelPics[levelIndex].doubleUp(),
        (
          case imageNum
          of 0:
            Offset(x: 0, y: 0)
          else:
            Offset(x: 0, y: 0)
        ),
      )
    of 2:
      const usePortraitCrops = true
      when usePortraitCrops: # vertical arrangement
        case imageNum
        of 0:
          discard finalImage.pasteImage(
            levelPics[levelIndex].doubleUp().crop(
              Rectangle(x: 80, y: 0, w: 160, h: 200)
            ),
            Offset(x: 0, y: 0),
          )
        of 1:
          discard finalImage.pasteImage(
            levelPics[levelIndex].doubleUp().crop(
              Rectangle(x: 80, y: 0, w: 160, h: 200)
            ),
            Offset(x: 160, y: 0),
          )
        else:
          discard
      else: # horizontal arrangement
        case imageNum
        of 0:
          discard finalImage.pasteImage(
            levelPics[levelIndex].doubleUp().crop(
              Rectangle(x: 0, y: 50, w: 320, h: 100)
            ),
            Offset(x: 0, y: 0),
          )
        of 1:
          discard finalImage.pasteImage(
            levelPics[levelIndex].doubleUp().crop(
              Rectangle(x: 0, y: 50, w: 320, h: 100)
            ),
            Offset(x: 0, y: 100),
          )
        else:
          discard
    of 3:
      case imageNum
      of 0:
        discard finalImage.pasteImage(levelPics[levelIndex], Offset(x: 0, y: 0))
      of 1:
        discard finalImage.pasteImage(levelPics[levelIndex], Offset(x: 160, y: 0))
      of 2:
        discard finalImage.pasteImage(
          levelPics[levelIndex].doubleUp().crop(Rectangle(x: 0, y: 50, w: 320, h: 100)),
          Offset(x: 0, y: 100),
        )
      else:
        discard finalImage.pasteImage(levelPics[levelIndex], Offset(x: 0, y: 0))
    else: # 4 and up
      discard finalImage.pasteImage(
        levelPics[levelIndex],
        (
          case imageNum
          of 0:
            Offset(x: 0, y: 0)
          of 1:
            Offset(x: 160, y: 0)
          of 2:
            Offset(x: 0, y: 100)
          of 3:
            Offset(x: 160, y: 100)
          else:
            Offset(x: 0, y: 0)
        ),
      )
    # next iter
    inc(imageNum)

proc previewFromLevelPics(
    levelPics: OrderedTable[int, Picture]
): Res[Picture] {.raises: [].} =
  let numLevelSelectPics = len(levelPics)
  if numLevelSelectPics < 1:
    return err("no level select images")

  const baseWidth = 160 * 2
  const baseHeight = 100 * 2
  var finalImage = Picture(w: baseWidth, h: baseHeight)
  finalImage.data = newSeq[PixelData](baseWidth * baseHeight)

  # `pickedIndices` would be an array[4, Opt[int]] but the compiler says,
  # `parallel 'fields' iterator does not work for 'case' objects`
  # because Opt[int] is a case object and the compiler can't handle
  # disjoint checking on that... I think.

  # Actually perform image placement
  try:
    case numLevelSelectPics
    of 1:
      var pickedIndices: array[1, int] = [-1]
      pickedIndices.arrangeImagesCommon(levelPics, finalImage)
    of 2:
      var pickedIndices: array[2, int] = [-1, -1]
      pickedIndices.arrangeImagesCommon(levelPics, finalImage)
    of 3:
      var pickedIndices: array[3, int] = [-1, -1, -1]
      pickedIndices.arrangeImagesCommon(levelPics, finalImage)
    else:
      # 4 and up
      var pickedIndices: array[4, int] = [-1, -1, -1, -1]
      pickedIndices.arrangeImagesCommon(levelPics, finalImage)
  except Exception as e:
    return err(e.message())

  return ok(finalImage)

proc createAddonPreview*(analysis: AddonAnalysisInfo): Res[Picture] =
  if not analysis.thumbnail.isSome():
    return previewFromLevelPics(analysis.levelPics)
  return ok(analysis.thumbnail.unwrap())
