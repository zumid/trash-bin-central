##[

This default disk implementation simply uploads the files directly to the
server. It's quite simple, but uhh *does it scale?????* 🤓🤓🤓

1. Arm the nugget:

   .. code:: nim

     prepareUpload("something.pk3")

2. (Robo) Blast it:

   .. code:: nim

     bufferUpload("...")
     bufferUpload("...")
     bufferUpload("...")

3. Finalize (or cancel) it:

   .. code:: nim

     when cancel:
       cancelUpload()
     else:
       finishUpload()

]##

# unqualified imports
import ../../util/resutils
import ../../util/log

# partially-qualified imports
from std/paths as ospaths import Path, `/`
from std/files import removeFile
from ../../config import nil

type
  UploadHandle* = object
    handle: File
    path: Path
    finished: bool

  UploadHandleRef* = ref UploadHandle

{.push gcsafe, raises: [].}

proc closeHandleImpl(handle: UploadHandleRef): void =
  handle.handle.close()
  handle.finished = true

proc prepareUpload*(filename: string): Res[UploadHandleRef] =
  ## .. importdoc:: ../../config
  ## Create an upload handle and reserves a file at `UploadBucketDir`_.
  let newFileName = Path(config.UploadBucketDir) / Path(filename)
  try:
    let newFile = open(string(newFileName), fmWrite)
    log.debug("Created upload handle")
    return ok(UploadHandleRef(handle: newFile, path: newFileName, finished: false))
  except IOError as e:
    return err(e.msg)

proc bufferUpload*(handle: UploadHandleRef, buffer: seq[byte]): Failable =
  ## Adds bytes to the upload handle.
  # log.trace("Buffering upload", writesize = len(buffer))
  if handle.handle == nil:
    log.error("nil handle")
    return err("attempting to buffer a nil upload")
  if handle.finished:
    log.error("finished handle")
    return err("attempting to buffer an already-finished upload")
  try:
    discard handle.handle.writeBytes(buffer, 0, len(buffer))
    # log.trace("perform buffer")
  except IOError as e:
    log.error("can't buffer", reason = e.msg)
    handle.closeHandleImpl()
    return err(e.msg)
  return ok()

proc cancelUpload*(handle: UploadHandleRef): void =
  ## For now, the same as `finishUpload`_ except the file
  ## is also removed.
  log.debug("Upload handle cancelled")
  if handle.finished:
    return
  if handle.handle == nil:
    return
  handle.closeHandleImpl()
  try:
    removeFile(handle.path)
  except OSError as e:
    log.error("can't remove file", reason = e.msg)

proc finishUpload*(handle: UploadHandleRef): void =
  ## Cleans up the upload handle and closes the file.
  log.debug("Upload handle finished")
  if handle.finished:
    return
  if handle.handle == nil:
    return
  handle.closeHandleImpl()

proc getPath*(handle: UploadHandleRef): Path =
  handle.path
