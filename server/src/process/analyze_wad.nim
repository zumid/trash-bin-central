import std/paths
import std/strutils
import std/tables
import binstreams
import ../util/resutils
import ../util/datatypes
when defined(traceResults):
  import ../util/log

from nimPNG import `==` # templates ruin this particular thing :(
from stew/byteutils import fromBytes, toBytes
from zippy/ziparchives as zip import walkFiles, extractFile, close
from ../util/parse/levelnum import decodeLevelNumber
from ../util/parse/soc import parseSoc

from lzf import nil
from ./patch_image import nil

{.push gcsafe, raises: [].}

proc verifyMagicNumber*(s: seq[byte]): Res[WadType] =
  ## Verify magic number of the first chunk of the file.
  ## That way, if the file isn't what was expected we can
  ## just reject it immediately.
  if len(s) < 4:
    return err("not enough data!")
  let thisMagicNum = s[0 .. 3]
  case string.fromBytes(thisMagicNum)
  of "IWAD", "PWAD", "SDLL": ## Doom WAD
    return ok(Wad)
  of "PK\x03\x04": ## PK3
    return ok(Pk3)
  of "ZWAD": ## Wadzip
    return ok(Wadzip)
  else:
    return err("unknown format")

proc unZwad(
    wf: FileStream, fileSize: int64
): seq[byte] {.raises: [IOError, lzf.IncompleteDataError, lzf.BackreferenceError].} =
  if (let uncompressedSize = wf.read(uint32); uncompressedSize > 0):
    # file is compressed, copy the file first into it
    var rawCompressedData =
      when (NimMajor, NimMinor) < (2, 2):
        newSeqUninitialized[byte](fileSize - 4)
      else:
        newSeqUninit[byte](fileSize - 4)
    wf.read(rawCompressedData, 0, len(rawCompressedData))
    return lzf.decompress(rawCompressedData)
  else:
    # file is not compressed inside the zwad
    var rawData =
      when (NimMajor, NimMinor) < (2, 2):
        newSeqUninitialized[byte](fileSize - 4)
      else:
        newSeqUninit[byte](fileSize - 4)
    wf.read(rawData, 0, fileSize - 4)
    return rawData

proc toPicture*(s: seq[byte]): Picture =
  # check bytes against magic number
  try:
    if len(s) >= 4 and
        (s[0] == 0x89'u8 and s[1] == 0x50'u8 and s[2] == 0x4e'u8 and s[3] == 0x47'u8):
      # This is already a png
      {.warning: "Assume the PNG does not contain an alpha channel".}
      # this uses the *other* Results so…
      let p = nimPNG.decodePNG24(s)
      if nimPNG.isErr(p):
        return Picture()
      let pp = nimPNG.get(p)
      var k = Picture(w: pp.width, h: pp.height)
      k.data = @[]
      var i = 0
      while i < len(pp.data):
        var newPixel = PixelData()
        newPixel.r = pp.data[i]
        inc i
        newPixel.g = pp.data[i]
        inc i
        newPixel.b = pp.data[i]
        inc i
        k.data.add(newPixel)
      return k
    # not a PNG
    return patch_image.patchToPicture(s)
  except CatchableError:
    return Picture()

proc toPng*(s: seq[byte]): seq[byte] =
  # check bytes against magic number
  if len(s) >= 4 and
      (s[0] == 0x89'u8 and s[1] == 0x50'u8 and s[2] == 0x4e'u8 and s[3] == 0x47'u8):
    # This is already a png
    return s
  try:
    return patch_image.patchToPng(s)
  except CatchableError:
    return @[]

proc analyzeWadImpl(s: Path): Res[AddonAnalysisInfo] =
  var wf = newFileStream(s.string, littleEndian, fmRead)
  if wf == nil:
    return err("file not found")
  try:
    # verify magic number again
    var isZwad =
      case wf.readStr(4)
      of "IWAD", "PWAD", "SDLL":
        false
      of "ZWAD":
        true
      else:
        return err("Invalid format")
    # get header stuff
    let
      nLumps = wf.read(uint32)
      dirLoc = wf.read(uint32).int64
    wf.setPosition(dirLoc)
    # scan directory
    var
      info = AddonAnalysisInfo(thumbnail: Opt.none(Picture))
      # keep track of marker locations
      inFlat = false
      inTexture = false
      inSprite = false
      thumbnailIsSet = false
    # start scanning
    for i in 1 .. nLumps:
      let
        filePos = wf.read(uint32).int64
        fileSize = wf.read(uint32).int64
        name = wf.readStr(8).strip(leading = false, chars = {'\x00'}).toUpperAscii()
      case name
      of "THINGS", "LINEDEFS", "SIDEDEFS", "VERTEXES", "SEGS", "SSECTORS", "NODES",
          "SECTORS", "REJECT", "BLOCKMAP":
        # ignore special map lump names
        continue
      # markers
      of "F_START", "FF_START":
        inFlat = true
        continue
      of "F_END", "FF_END":
        inFlat = false
        continue
      of "S_START", "SS_START":
        inSprite = true
        continue
      of "S_END", "SS_END":
        inSprite = false
        continue
      of "TX_START":
        inTexture = true
        continue
      of "TX_END":
        inTexture = false
        continue
      of "CREDITS", "README":
        let prevPos = wf.getPosition()
        wf.setPosition(filePos)
        block:
          if isZwad:
            info.directory.readme = string.fromBytes(wf.unZwad(fileSize))
          else:
            # all files are not compressed
            info.directory.readme = newString(fileSize)
            wf.read(info.directory.readme, 0, fileSize)
        wf.setPosition(prevPos)
        continue
      else:
        discard
      # categorize lumps
      # specific names take priority...
      if name in ["MAINCFG", "OBJCTCFG"] or name.startsWith("SOC_"):
        inc info.directory.soc
        # parse SOC
        let prevPos = wf.getPosition()
        wf.setPosition(filePos)
        block:
          var socBuffer: string
          if isZwad:
            socBuffer = string.fromBytes(wf.unZwad(fileSize))
          else:
            # all files are not compressed
            socBuffer = newString(fileSize)
            wf.read(socBuffer, 0, fileSize)
          socBuffer.parseSoc(info)
        wf.setPosition(prevPos)
        continue
      if name.startsWith("LUA_"):
        inc info.directory.lua
        info.features = info.features + {DetectedFeature.Lua}
        continue
      if name == "TBCTHUMB":
        let prevPos = wf.getPosition()
        wf.setPosition(filePos)
        block:
          if isZwad:
            info.thumbnail = Opt[Picture].some(wf.unZwad(fileSize).toPicture())
          else:
            # all files are not compressed
            var data: seq[byte] =
              when (NimMajor, NimMinor) < (2, 2):
                newSeqUninitialized[byte](fileSize)
              else:
                newSeqUninit[byte](fileSize)
            wf.read(data, 0, fileSize)
            info.thumbnail = Opt[Picture].some(data.toPicture())
          thumbnailIsSet = true
        wf.setPosition(prevPos)
        continue
      if name.startsWith("MAP"):
        if name[^1] == 'P':
          # level select preview image
          if thumbnailIsSet:
            continue
          # extract map number
          let mapNumber = name[3 .. 4].decodeLevelNumber()
          let prevPos = wf.getPosition()
          wf.setPosition(filePos)
          block:
            if isZwad:
              info.levelPics[mapNumber] = wf.unZwad(fileSize).toPicture()
            else:
              # all files are not compressed
              var data: seq[byte] =
                when (NimMajor, NimMinor) < (2, 2):
                  newSeqUninitialized[byte](fileSize)
                else:
                  newSeqUninit[byte](fileSize)
              wf.read(data, 0, fileSize)
              info.levelPics[mapNumber] = data.toPicture()
          wf.setPosition(prevPos)
        else:
          inc info.directory.map
        continue
      if name.startsWith("DS"): # digital sound
        inc info.directory.snd
        continue
      if name.startsWith("D_") or name.startsWith("O_"): # music
        inc info.directory.msc
        continue
      # then the marker stuff...
      if inFlat:
        inc info.directory.flat
        continue
      if inTexture:
        inc info.directory.tex
        continue
      if inSprite:
        inc info.directory.sprite
        continue
    # finished
    {.warning: "TODO verify WAD analysis results".}
    return ok(info)
  except CatchableError as e:
    return err(e.message())

proc analyzePk3Impl(s: Path): Res[AddonAnalysisInfo] =
  var
    info = AddonAnalysisInfo(thumbnail: Opt.none(Picture))
    gotReadme = false
    thumbnailIsSet = false
  try:
    let zipReader = zip.openZipArchive(string(s))
    defer:
      zipReader.close()
    for name in zipReader.walkFiles():
      let (dir, nnormName, ext) = Path(name.toLowerAscii()).splitFile()
      let normName = string(nnormName)
      if normName == "init" and ext == "lua":
        info.directory.lua.inc()
        continue
      if not gotReadme and normName.startsWith("readme"):
        info.directory.readme = zipReader.extractFile(name)
        gotReadme = true
        continue
      if normName.startsWith("tbcthumb"):
        info.thumbnail =
          Opt[Picture].some(toBytes(zipReader.extractFile(name)).toPicture())
        thumbnailIsSet = true
        continue
      # map textures can be placed anywhere
      if normName.startsWith("map") and len(normName) == len("mapXXp") and
          normName[^1] == 'p':
        if thumbnailIsSet:
          continue
        # found map texture
        # extract map number
        let mapNumber = normName[3 .. 4].decodeLevelNumber()
        # place to map
        info.levelPics[mapNumber] = toBytes(zipReader.extractFile(name)).toPicture()
        continue
      # make EXTRA sure that we have the root dir
      let
        (dir1, dir2) = dir.splitPath()
        rootDir =
          if string(dir1) == "":
            string(dir2)
          else:
            string(dir1)
      case rootDir
      of "flats":
        info.directory.flat.inc()
      of "textures":
        info.directory.tex.inc()
      of "sprites":
        info.directory.sprite.inc()
      of "soc":
        info.directory.soc.inc()
        zipReader.extractFile(name).parseSoc(info)
      of "lua":
        info.directory.lua.inc()
        info.features = info.features + {DetectedFeature.Lua}
      of "maps":
        info.directory.map.inc()
      of "graphics":
        info.directory.gfx.inc()
      of "sounds":
        info.directory.snd.inc()
      of "music":
        info.directory.msc.inc()
      else:
        discard
    result = ok(info)
  except CatchableError as e:
    result = err(e.message())

proc analyzeFile*(s: Path, t: WadType): Res[AddonAnalysisInfo] =
  case t
  of Wad, Wadzip:
    return analyzeWadImpl(s)
  of Pk3:
    return analyzePk3Impl(s)
