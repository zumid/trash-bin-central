import std/strtabs
import ../util/log
import ../util/resutils
import ../util/datatypes
import ../util/dbmanager
import ./upload_impl/disk

from std/paths as ospaths import `/`
from std/strutils import endsWith
from stew/byteutils import toBytes
from forkygs/multipartserver import
  isMultipartForm, receiveParts, parseContentDisposition, PartState
from ../util/db2object/session_user import getSessionUser

from std/random import nil
from checksums/sha2 import nil
from ../config as config import nil
from ../util/db2object/sessions import nil
from ../util/db2object/addon_info import nil
from ../util/db2object/addon_levels import nil
from ../util/db2object/addon_tags import nil
from ../util/db2object/addon_versions import nil
from ../util/helper/strhelper import nil
from ../util/parse/bbcode import nil
from ./patch_image import nil
from ./analyze_wad import nil

{.push gcsafe, raises: [].}

type UploadedAddon* = object
  details*: Addon
  fileName*: string

proc guessTypeFromExtension(filename: string): Opt[WadType] =
  let filenameNoCase = strutils.toLowerAscii(filename)
  if filenameNoCase.endsWith(".wad") or filenameNoCase.endsWith(".dll"):
    return Opt[WadType].some(Wad)
  if filenameNoCase.endsWith(".pk3"):
    return Opt[WadType].some(Pk3)
  return Opt.none(WadType)

# Form input names
const
  fiTitle = "title"
  fiVersion = "vers"
  fiAuthor = "author"
  fiSynopsis = "synopsis"
  fiDescription = "desc"
  fiWhichCompat = "compat"
  fiACSRFToken = "csrf"
  fiAgree = "agree"
  fiMainFile = "main-file"
  fiReleaseNotes = "changelog"
  fiEditCode = "edit-code"

proc validateAddonVersionInput*(f: StringTableRef): Result[void, seq[FlashMessage]] =
  ## Shared between addon upload page and edit version page.
  let vers = strhelper.getFormInput(f, fiVersion)
  let chlog = strhelper.getFormInput(f, fiReleaseNotes)
  var errorList: seq[FlashMessage]
  # version
  if len(vers) < 1:
    errorList.add(VersionNumberMissing)
  # description
  if len(chlog) > 0:
    if (let x = bbcode.bbcode2HtmlResult(chlog); not x.isOk()):
      log.error("error parsing change log", error = x.unwrapErr())
      errorList.add(CannotParseChangeLog)
  #
  if len(errorList) > 0:
    return err(errorList)
  return ok()

proc validateAddonInfoInput*(
    f: StringTableRef, isLoggedIn: bool
): Result[void, seq[FlashMessage]] =
  ## Shared between addon upload page and edit info page.
  let
    title = strhelper.getFormInput(f, fiTitle)
    author = strhelper.getFormInput(f, fiAuthor)
    synopsis = strhelper.getFormInput(f, fiSynopsis)
    desc = strhelper.getFormInput(f, fiDescription)
    compat = strhelper.getFormInput(f, fiWhichCompat)
  var errorList: seq[FlashMessage]
  # title
  if len(title) < 1:
    errorList.add(AddOnNameMissing)
  elif len(title) > config.AddonNameLimit:
    errorList.add(AddOnNameTooLong)
  # author
  if not isLoggedIn and len(author) < 1:
    errorList.add(AuthorMissing)
  # synopsis
  if len(synopsis) < 1:
    errorList.add(SynopsisMissing)
  # compat
  if len(compat) < 1:
    errorList.add(AddonTargetMissing)
  try:
    discard strutils.parseEnum[AddonTarget](compat)
  except ValueError as e:
    log.error("Parse compat error", error = e.msg)
    errorList.add(InvalidTarget)
  # description
  if (let x = bbcode.bbcode2HtmlResult(desc); not x.isOk()):
    log.error("error parsing description", error = x.unwrapErr())
    errorList.add(CannotParseDescription)
  #
  if len(errorList) > 0:
    return err(errorList)
  return ok()

proc validateAddonMiscInput*(
    f: StringTableRef, sessionId: string
): Result[void, seq[FlashMessage]] =
  ## Shared between addon upload page, edit info page and edit version page.
  let
    csrf = strhelper.getFormInput(f, fiACSRFToken)
    agree = strhelper.getFormInput(f, fiAgree)
  var errorList: seq[FlashMessage]
  if len(csrf) < 1:
    errorList.add(InvalidSession)
  if len(agree) < 1:
    errorList.add(MustAgreeGuideline)
  if (let i = sessions.checkAntiCsrfToken(csrf, sessionId); not i.isOk()):
    log.error("csrf token check failed", error = i.unwrapErr())
    errorList.add(InvalidSession)
  #
  if len(errorList) > 0:
    return err(errorList)
  return ok()

proc processUploadedFile(
    s: ref Database,
    handle: UploadHandleRef,
    formParams: StringTableRef,
    sessionId: string,
    fileType: WadType,
    runningHash: var sha2.ShaStateStatic[sha2.Sha_224],
    chunk: string,
    editCodeOutput: var Opt[string],
    forAddon: Opt[Addon],
    errorList: var seq[FlashMessage],
    isOwner: Opt[bool],
): Opt[UploadedAddon] =
  let hasAddon = forAddon.isSome()
  if hasAddon and not forAddon.unwrap().id.isSome():
    log.error("addon does not have an ID!")
    errorList.add(CannotUploadFile)
    return Opt.none(UploadedAddon)

  let currentUser = getSessionUser(s, sessionId)

  if not hasAddon:
    # new add-on needs info input
    if (let i = validateAddonInfoInput(formParams, currentUser.isOk()); not i.isOk()):
      errorList.add(i.unwrapErr())

  if (let i = validateAddonVersionInput(formParams); not i.isOk()):
    errorList.add(i.unwrapErr())

  if (let i = validateAddonMiscInput(formParams, sessionId); not i.isOk()):
    errorList.add(i.unwrapErr())

  if len(errorList) > 0:
    return Opt.none(UploadedAddon)

  log.trace("file path", path = handle.getPath().string())

  let analysisResults = analyze_wad.analyzeFile(handle.getPath(), fileType)
  if not analysisResults.isOk():
    log.error("analysis fail", e = analysisResults.unwrapErr())
    errorList.add(CannotUploadFile)
    return Opt.none(UploadedAddon)

  if (let i = s.begin(); not i.isOk()):
    log.error("can't begin transaction", e = i.unwrapErr())
    errorList.add(CannotUploadFile)
    return Opt.none(UploadedAddon)

  let newAddon =
    if hasAddon: # we have an existing addon
      forAddon.unwrap()
    else: # this is a new addon
      let name = strhelper.getFormInput(formParams, fiTitle)
      let exists = addon_info.addonExistsByName(s, name)
      if exists.isOk():
        if exists.unwrap():
          errorList.add(AddonExists)
          return Opt.none(UploadedAddon)
      else:
        log.error("cannot determine if addon already exists", e = exists.unwrapErr())
        errorList.add(CannotUploadFile)
        return Opt.none(UploadedAddon)
      let desc = strhelper.getFormInput(formParams, fiDescription)

      # Add the addon info
      let editCode =
        if currentUser.isOk():
          ""
        else:
          let editCodeResult = addon_info.generateEditCode()
          if not editCodeResult.isOk():
            log.error("cannot generate edit code", e = editCodeResult.unwrapErr())
            errorList.add(CannotUploadFile)
            return Opt.none(UploadedAddon)
          editCodeResult.unwrap()

      editCodeOutput =
        if currentUser.isOk():
          Opt.none(string)
        else:
          Opt[string].some(editCode)

      let newAddonResult = addon_info.insertAddonToDb(
        s,
        Addon(
          name: name,
          synopsis: strhelper.getFormInput(formParams, fiSynopsis),
          description:
            if desc == "":
              Opt.none(string)
            else:
              Opt[string].some(desc),
          author:
            if currentUser.isOk():
              currentUser.unwrap().name
            else:
              strhelper.getFormInput(formParams, fiAuthor),
          authorId:
            if currentUser.isOk() and currentUser.unwrap().id.isSome():
              currentUser.unwrap().id
            else:
              Opt.none(int),
        ),
        editCode,
      )
      if not newAddonResult.isOk():
        log.error("add new addon fail", e = newAddonResult.unwrapErr())
        errorList.add(CannotUploadFile)
        return Opt.none(UploadedAddon)
      newAddonResult.unwrap()

  if hasAddon:
    # but if there's an existing add-on, we must also
    # have an edit code to go with it
    if not isOwner.isSome() and (
      let i = addon_info.checkEditCode(s, newAddon, formParams.getOrDefault(fiEditCode))
      not i.isOk()
    ):
      errorList.add(InvalidEditCode)
      return Opt.none(UploadedAddon)

  let versionString = strhelper.getFormInput(formParams, fiVersion)
  let relNotes = strhelper.getFormInput(formParams, fiReleaseNotes)

  # Get the hash of the file which we calculated earlier
  sha2.update(runningHash, cast[seq[char]](chunk.toBytes()))

  # Try to convert it into an int
  let finalHash: int = 0
  block:
    let h = sha2.digest(runningHash)
    copyMem(finalHash.addr, h[0].addr, sizeof(finalHash))

  # Then, add the addon version
  let analysis = analysisResults.unwrap()

  var generatedFileName = ""

  # Add tags from features
  for feature in analysis.features:
    case feature
    of SinglePlayer:
      if hasAddon or addon_tags.assignTag(s, newAddon, "single").isOk():
        generatedFileName.add('S')
    of Race:
      if hasAddon or addon_tags.assignTag(s, newAddon, "race").isOk():
        generatedFileName.add('R')
    of Match:
      if hasAddon or addon_tags.assignTag(s, newAddon, "match").isOk():
        generatedFileName.add('M')
    of Ctf:
      if hasAddon or addon_tags.assignTag(s, newAddon, "ctf").isOk():
        generatedFileName.add('F')
    of Battle:
      if hasAddon or addon_tags.assignTag(s, newAddon, "battle").isOk():
        generatedFileName.add('B')
    of Tutorial:
      if hasAddon or addon_tags.assignTag(s, newAddon, "tutorial").isOk():
        generatedFileName.add('T')
    of Character:
      if hasAddon or addon_tags.assignTag(s, newAddon, "characters").isOk():
        generatedFileName.add('C')
    of Follower:
      if hasAddon or addon_tags.assignTag(s, newAddon, "follower").isOk():
        generatedFileName.add('f')
    of Lua:
      if hasAddon or addon_tags.assignTag(s, newAddon, "lua").isOk():
        generatedFileName.add('L')
  if len(generatedFileName) > 0:
    generatedFileName.add('_')
  generatedFileName.add(strhelper.filenameize(newAddon.name))
  generatedFileName.add('_')
  generatedFileName.add(strhelper.filenameize(versionString))
  generatedFileName.add(
    case fileType
    of Wad, Wadzip: ".wad"
    of Pk3: ".pk3"
  )

  let newAddonVersionResult = addon_versions.insertAddonVersionToDb(
    s,
    AddonVersionInfo(
      hash: finalHash,
      addonId: newAddon.id.unwrap(),
      physicalFileName: handle.getPath().string,
      virtualFileName: generatedFileName,
      fileType: fileType,
      versionString: versionString,
      fileSize: 0, # filled in by insertAddonVersionToDb
      flats: analysis.directory.flat,
      texs: analysis.directory.tex,
      sprites: analysis.directory.sprite,
      socs: analysis.directory.soc,
      luas: analysis.directory.lua,
      maps: analysis.directory.map,
      gfxs: analysis.directory.gfx,
      snds: analysis.directory.snd,
      mscs: analysis.directory.msc,
      readme: analysis.directory.readme,
      releaseNotes: stringFromNullable(relNotes),
    ),
  )
  if not newAddonVersionResult.isOk():
    log.error("add new addon version fail", e = newAddonVersionResult.unwrapErr())
    errorList.add(CannotUploadFile)
    return Opt.none(UploadedAddon)

  # Add tags from compat settings for new add-ons
  if not hasAddon:
    try:
      let target = strutils.parseEnum[AddonTarget](formParams[fiWhichCompat])
      if not addon_tags.assignTag(s, newAddon, $target).isOk():
        log.error("could not assign tag")
        discard s.rollback()
        errorList.add(CannotUploadFile)
        return Opt.none(UploadedAddon)
    except ValueError as e:
      log.error("invalid addon target", e = e.msg)
      errorList.add(CannotUploadFile)
      return Opt.none(UploadedAddon)

  if hasAddon: # remove all levels if it's an existing addon
    if not addon_levels.deleteAllLevels(s, newAddon).isOk():
      log.error("couldn't clear addon levels")
      discard s.rollback()
      errorList.add(CannotUploadFile)
      return Opt.none(UploadedAddon)

  # try to commit levels
  for levelNum, levelData in pairs(analysis.levels):
    if not addon_levels.insertAddonLevel(s, newAddon, levelNum, levelData).isOk():
      log.error("could not insert addon level")
      discard s.rollback()
      errorList.add(CannotUploadFile)
      return Opt.none(UploadedAddon)

  let finalImage = patch_image.createAddonPreview(analysis)
  if finalImage.isOk():
    # Save the image
    let baseName = $newAddon.id.unwrap()
    let exportName = ospaths.Path(config.PreviewsDir) / ospaths.Path(baseName & ".png")

    log.trace("saving image", name = string(exportName))
    try:
      var f = open(string(exportName), fmWrite)
      let c = patch_image.pictureToPng(finalImage.unwrap())
      discard f.writeBytes(c, 0, len(c))
      f.close()
    except CatchableError as e:
      {.warning: "TODO: should we ignore this?".}
      log.error("can't save image", e = e.message())
      discard
  else:
    log.error("cannot compose image", e = finalImage.unwrapErr())

  if (let i = s.commit(); not i.isOk()):
    log.error("can't commit transaction", e = i.unwrapErr())
    errorList.add(CannotUploadFile)
    return Opt.none(UploadedAddon)

  return Opt[UploadedAddon].some(
    UploadedAddon(details: newAddon, fileName: generatedFileName)
  )

proc createUniqueFileName(w: WadType): string =
  var r = random.initRand()
  var fileName = newString(32)
  for i in 0 ..< len(fileName):
    fileName[i] = random.sample(r, {'A' .. 'Z'} + {'a' .. 'z'} + {'0' .. '9'})
  return
    fileName & (
      case w
      of Wad, Wadzip: ".wad"
      of Pk3: ".pk3"
    )

proc receiveAddonUpload*(
    s: ref Database,
    sessionId: string,
    formParams: StringTableRef,
    editCodeOutput: var Opt[string],
    forAddon: Opt[Addon],
    isOwner: Opt[bool],
): Result[UploadedAddon, seq[FlashMessage]] =
  ##[
  Accepts a streamed POST multipart request containing an add-on file.
  Writes parameters into ``formParams`` as the request is streamed in,
  while simultaneously writing the main file into _uploads.
  
  After the main file is uploaded, the form parameters are processed
  inside of `processUploadedFile`_.
  ]##
  var
    errors: seq[FlashMessage]
    uploadResult = Opt.none(UploadedAddon)
    gotFirstChunk = false
    name: string
    filename: string
    handle: Res[UploadHandleRef] = Res[UploadHandleRef].initErr("Blank handle")
    fileType: WadType
    ignoreFurtherPackets = false
    sizeSoFar = 0
    runningHash = sha2.initSha_224()

  if not isMultipartForm():
    errors.add(NotAMultipartForm)
    ignoreFurtherPackets = true

  for (state, chunk) in receiveParts():
    # If the server replies early, it won't send a FIN to go along with it.
    # It will just close the entire socket afterwards. For small files, this is
    # okay, as no more packets will be sent afterward.
    #
    # Otherwise, for considerably large files, the browser will send
    # more data. But the socket has already been closed, rendering further sends
    # to be met with a RST. This is what causes the browser to error out with a
    # "Connection reset" message, even though I've designed a nice error page that
    # now won't be shown thanks to the browser deciding to intervene.
    #
    # Furthermore: https://www.excentis.com/blog/tcp-half-close-a-cool-feature-that-is-now-broken/
    #
    # As a result, we have to "emulate" this half-close feature by ignoring
    # further data until the browser is finished sending all data.
    if ignoreFurtherPackets:
      continue

    case state
    of HeaderReady:
      (name, filename) = parseContentDisposition()
      log.trace("got header", chunk = chunk, name = name, filename = filename)

      # skip if filename is empty
      if len(filename) < 1:
        if name == fiMainFile:
          errors.add(BlankFileName)
          ignoreFurtherPackets = true
          continue
        else:
          continue

      # check for supported extensions
      let extensionResult = filename.guessTypeFromExtension()
      if not extensionResult.isSome():
        log.error("File name has wrong extension", filename = filename)
        errors.add(UnsupportedExtension)
        ignoreFurtherPackets = true
        continue

      # prepare upload
      handle = prepareUpload(createUniqueFileName(extensionResult.unwrap()))
      if not handle.isOk():
        log.error("upload prep failed", reason = handle.unwrapErr())
        errors.add(CannotUploadFile)
        ignoreFurtherPackets = true
        continue
      else:
        log.info("file upload started")
    of BodyChunk:
      # log.trace("stream body in progress")
      if len(name) > 0 and name != fiMainFile:
        formParams[name] = chunk
        continue

      if not handle.isOk():
        continue

      let handleResult = handle.unwrap()
      # verify magic number on the first chunk received
      #
      # this allows the file to be rejected from the outset
      # without waiting for the entire thing to be processed,
      # trudged to disk, or whatever.
      if not gotFirstChunk:
        if (let kind = analyze_wad.verifyMagicNumber(chunk.toBytes()); kind.isOk()):
          fileType = kind.unwrap()
          log.debug("is valid file", kind = kind.unwrap())
        else:
          log.error("invalid file!")
          handleResult.cancelUpload()
          errors.add(InvalidFile)
          ignoreFurtherPackets = true
          continue
        gotFirstChunk = true

      # once the file passed the vibe check we can now proceed
      # with processing the upload
      if (let i = handleResult.bufferUpload(chunk.toBytes()); not i.isOk()):
        log.error("Failed to upload file", reason = i.unwrapErr())
        handleResult.cancelUpload()
        errors.add(CannotUploadFile)
        ignoreFurtherPackets = true
        continue
      else:
        # update running hash
        sha2.update(runningHash, cast[seq[char]](chunk))
        # track the size of the uploaded file directly
        # is content-length reliable?
        sizeSoFar += len(chunk)
        if sizeSoFar > config.MaxUploadFileSize:
          log.error("File too big!", size = sizeSoFar, max = config.MaxUploadFileSize)
          handleResult.cancelUpload()
          errors.add(FileTooBig)
          ignoreFurtherPackets = true
          continue
    of BodyReady:
      log.trace("got body")
    of Failed:
      log.error("Failed to upload file")
      if handle.isOk():
        handle.unwrap().cancelUpload()
      errors.add(CannotUploadFile)
      ignoreFurtherPackets = true
      continue
    of Completed:
      log.info("file upload completed", name = name, filename = filename)
      if handle.isOk():
        let handleValue = handle.unwrap()
        # the file needs to be saved before
        # processing
        handleValue.finishUpload()
        uploadResult = processUploadedFile(
          s, handleValue, formParams, sessionId, fileType, runningHash, chunk,
          editCodeOutput, forAddon, errors, isOwner,
        )
      else:
        log.error("handle error", e = handle.unwrapErr())

  if uploadResult.isSome():
    return ok(uploadResult.unwrap())

  return err(errors)
