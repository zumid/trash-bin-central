import std/strtabs
import ../util/resutils

from ../util/parse/http_path import parseParamsFrom
from forkygs/httpserver as http import SocketState

proc receivePostForm*(): Res[StringTableRef] =
  var requestBuf = newStringOfCap(128)
  for (state, chunkStr) in http.receiveStream():
    case state
    of Progress:
      {.
        warning:
          "TODO add safeguard to ignore request if length over some reasonable limit"
      .}
      requestBuf.add(chunkStr)
    of Complete:
      return parseParamsFrom(requestBuf)
    of Fail:
      return err("request failed")
    of TryAgain:
      discard
