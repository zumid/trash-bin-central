import std/strtabs
import ../util/log
import ../util/resutils
import ../util/datatypes

from ../util/parse/http_path import PathParts, Path

from std/httpcore as httpcore import nil
from ../render import nil
from ../util/db2object/addon_info import nil
from ../util/tbcontext import getCurrentDbConnection

{.push gcsafe, raises: [].}

proc GET*(
    fullPath: Path,
    parts: var PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    sess: Res[SessionRef],
): RespType =
  ##[
  ==================== =================================== =
  Endpoint             Description                         Args
  ==================== =================================== =
  /                    Home page
  ==================== =================================== =
  ]##
  var
    addonsLatest: seq[AddonBlurb]
    addonsRanked: seq[AddonBlurb]
    randomAddons: seq[AddonBlurb]

  var s = getCurrentDbConnection()

  let latestAddons = addon_info.getAllAddons(s, true, addon_info.UploadDate, 5)
  let latestRankedAddons = addon_info.getLatestHighestRatedAddons(s, 5)
  let randomAddon = addon_info.getRandomAddon(s)

  if latestAddons.isOk():
    for addon in latestAddons.unwrap():
      let blurb = addon_info.addonToAddonBlurb(s, addon)
      if not blurb.isOk():
        log.error(
          "can't get blurb for this addon", e = blurb.unwrapErr(), id = addon.id
        )
        continue
      addonsLatest.add(blurb.unwrap())
  else:
    log.error("can't get latest addons", e = latestAddons.unwrapErr())

  if latestRankedAddons.isOk():
    for addon in latestRankedAddons.unwrap():
      let blurb = addon_info.addonToAddonBlurb(s, addon)
      if not blurb.isOk():
        log.error(
          "can't get blurb for this addon", e = blurb.unwrapErr(), id = addon.id
        )
        continue
      addonsRanked.add(blurb.unwrap())
  else:
    log.error("can't get latest addons", e = latestRankedAddons.unwrapErr())

  if randomAddon.isOk():
    let addonPresent = randomAddon.unwrap()
    if addonPresent.isSome():
      let blurb = addon_info.addonToAddonBlurb(s, addonPresent.unwrap())
      if blurb.isOk():
        randomAddons.add(blurb.unwrap())
      else:
        log.error("can't turn addon to blurb", e = blurb.unwrapErr())
  else:
    log.error("can't get random addon", e = randomAddon.unwrapErr())

  let body = render.Landing(
    addonsLatest = addonsLatest,
    addonsRanked = addonsRanked,
    randomAddons = randomAddons,
    sess,
  )
  return (code: httpcore.Http200, body: body)
