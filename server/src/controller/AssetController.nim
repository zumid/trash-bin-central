##
## This one will read directly from the file system, potential
## funnies are to be had here.
##
## Used for /assets and /download.
##

import std/strtabs
import ../util/log
import ../util/resutils
import ../util/datatypes

from std/strutils import join, startsWith
from std/files as osfiles import fileExists
from std/paths as ospaths import `/`
from ../util/parse/http_path import PathParts, Path

from std/httpcore as httpcore import nil
from std/mimetypes as mimetypes import nil
from std/times as times import nil
from std/os as os import nil
from forkygs/httpserver as http import nil
from forkygs/guildenserver as gs import nil
from ../config as config import nil
from ../util/tbcontext import nil
from ../util/errorpages as ep import nil
from ../util/db2object/addon_versions import nil
from ../util/db2object/addon_downloads import nil
from ../util/parse/http_ranges import nil

{.push gcsafe, raises: [].}

type AssetKind* = enum
  Asset
  Download
  Preview
  Favicon

proc getMimeOf(ext: string): string =
  var mimes = mimetypes.newMimetypes()
  mimetypes.register(mimes, "less", "plain/text")
  mimetypes.getMimetype(mimes, ext, default = "application/octet-stream")

template setupCache(
    ext: string,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    translatedPath: string,
): untyped =
  let maxAge =
    if getMimeOf(ext).startsWith("image/"):
      times.inSeconds(times.initDuration(weeks = 1))
    else:
      times.inSeconds(times.initDuration(days = 1))
  # cache control
  responseHeaders.add(
    if maxAge > 0:
      "Cache-Control: max-age=" & $maxAge & ", public"
    else:
      "Cache-Control: no-cache"
  )
  # Since this is just for cache control, if an exception happens here
  # we just ignore these directives
  try:
    let modifDate = times.utc(os.getLastModificationTime(translatedPath))
    responseHeaders.add(
      "Last-Modified: " & times.format(modifDate, config.StdHttpFormat)
    )
    if (
      let ifModSince = requestHeaders.getOrDefault("if-modified-since")
      ifModSince != ""
    ):
      let
        imsDate =
          times.toTime(times.parse(ifModSince, config.StdHttpFormat, times.utc()))
        modUnix = times.toUnix(times.toTime(modifDate))
        imsUnix = times.toUnix(imsDate)
      # log.debug("last-mod", date = modUnix)
      # log.debug("if-mod-since", date = imsUnix)
      let isCacheHit = imsUnix == modUnix
      # log.debug("cache hit", hit = isCacheHit)
      if isCacheHit:
        let m = ""
        return (code: httpCore.Http304, body: m)
    # ETag is an option here, but that would require reading the file
    # from the file system, anyway.
  except OSError as e:
    log.warn("can't get modified date from OS", msg = e.msg)
    discard
  except times.TimeParseError as e:
    log.warn("can't parse if-mod-since", msg = e.msg)
    discard

proc serveAssetGeneric(
    parts: var PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    basePath: string,
    sess: Res[SessionRef],
): RespType =
  let translatedPath = ospaths.Path(basePath) / ospaths.Path(parts.join("/"))

  # error if file doesn't exist
  if not (fileExists(translatedPath)):
    return ep.makeErrorTuple(
      httpcore.Http404, "File not found", "file not found", string(translatedPath), sess
    )

  # Read from file system
  let (_, _, ext) = ospaths.splitFile(translatedPath)
  try:
    # force the browser not to download the file
    responseHeaders.add("Content-Disposition: inline")

    # setup cachestuff
    setupCache(ext, responseHeaders, requestHeaders, string(translatedPath))
    responseHeaders.add("Content-Type: " & getMimeOf(ext))

    # actually serve the file
    let m = open(string(translatedPath)).readAll()
    return (code: httpcore.Http200, body: m)
  except IOError as e:
    return ep.makeErrorTuple(
      httpcore.Http500, "IOError", "file can't be served", e.msg, sess
    )

proc serveFavicon(
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    sess: Res[SessionRef],
): RespType =
  # Straight-up just a copy of serveAssetGeneric but for one file.
  # Maybe I'll refactor this later.
  let path = block:
    let p = ospaths.Path(config.AssetsDir) / ospaths.Path("favicon.ico")
    if not (fileExists(p)):
      return ep.makeErrorTuple(
        httpcore.Http404, "File not found", "file not found", string(p), sess
      )
    p
  let (_, _, ext) = ospaths.splitFile(path)
  try:
    responseHeaders.add("Content-Disposition: inline")
    setupCache(ext, responseHeaders, requestHeaders, string(path))
    responseHeaders.add("Content-Type: " & getMimeOf(ext))
    let m = open(string(path)).readAll()
    return (code: httpcore.Http200, body: m)
  except IOError as e:
    return ep.makeErrorTuple(
      httpcore.Http500, "IOError", "file can't be served", e.msg, sess
    )

proc serveAsset(
    parts: var PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    sess: Res[SessionRef],
): RespType =
  serveAssetGeneric(parts, responseHeaders, requestHeaders, config.AssetsDir, sess)

proc serveDownload(
    hash: string,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    ipAddress: Opt[string],
    isHead: bool,
    sess: Res[SessionRef],
): RespType =
  let s = tbcontext.getCurrentDbConnection()
  let versionResult = addon_versions.getAddonVersionByHash(s, hash)
  if not versionResult.isOk():
    return ep.makeErrorTuple(
      httpcore.Http404,
      "Addon version not found",
      "Addon not found",
      versionResult.unwrapErr(),
      sess,
    )

  let versionOpt = versionResult.unwrap()
  if not versionOpt.isSome():
    return
      ep.makeErrorTuple(httpcore.Http404, "File not found", "file not found", "", sess)

  let version = versionOpt.unwrap()

  let translatedPath =
    ospaths.Path(config.UploadBucketDir) / ospaths.Path(version.physicalFileName)

  if not (fileExists(translatedPath)):
    return ep.makeErrorTuple(
      httpcore.Http404, "File not found", "file not found", string(translatedPath), sess
    )

  # Read from file system
  # force the browser to download the file
  {.warning: "Assume virtualFileName is sanitized from the start".}
  responseHeaders.add(
    "Content-Disposition: attachment; filename=\"" & version.virtualFileName & "\""
  )

  # setup cachestuff
  responseHeaders.add(
    "Content-Type: " & (
      case version.fileType
      of WadType.Wad, WadType.Wadzip: "application/x-doom-wad"
      of WadType.Pk3: "application/zip-compressed"
    )
  )

  # actually serve the file
  let rangeHeader = requestHeaders.getOrDefault("range")
  let codeOnSuccess =
    if not isHead and rangeHeader != "": httpcore.Http206 else: httpcore.Http200

  let m =
    try:
      let theFile = open(string(translatedPath))
      let fs = theFile.getFileSize()
      if isHead:
        {.warning: "Sussy".}
        newString(version.fileSize)
      else:
        if rangeHeader != "":
          let rr = http_ranges.rangeFromRequest(rangeHeader)
          if not rr.isOk():
            return ep.makeErrorTuple(
              httpcore.Http416,
              "Invalid range request",
              "Invalid range request",
              rr.unwrapErr(),
              sess,
            )
          let r = rr.unwrap()
          if r.partStart.isSome() and r.partEnd.isSome():
            let
              start = r.partStart.unwrap()
              endn = r.partEnd.unwrap()
              length = endn - start
            theFile.setFilePos(start)
            var s = newString(length)
            discard theFile.readBuffer(s[0].addr, length)
            responseHeaders.add(
              "Content-Range: bytes " & $start & "-" & $endn & "/" & $fs
            )
            s
          elif r.partStart.isSome():
            let start = r.partStart.unwrap()
            theFile.setFilePos(start)
            let length = fs - start
            var s = newString(length)
            discard theFile.readBuffer(s[0].addr, length)
            responseHeaders.add(
              "Content-Range: bytes " & $start & "-" & $(fs - 1) & "/" & $fs
            )
            s
          elif r.partEnd.isSome():
            let start = r.partEnd.unwrap()
            theFile.setFilePos(start, fspEnd)
            let length = fs - theFile.getFilePos()
            var s = newString(length)
            discard theFile.readBuffer(s[0].addr, length)
            responseHeaders.add(
              "Content-Range: bytes " & $start & "-" & $(fs - 1) & "/" & $fs
            )
            s
          else:
            # rangeFromRequest guarantees both ends won't be empty
            # this should be unreachable, but Nim still wants me
            # to do this :(
            return ep.makeErrorTuple(
              httpcore.Http416, "Invalid range request", "Invalid range request", "",
              sess,
            )
        else:
          theFile.readAll()
    except IOError as e:
      return ep.makeErrorTuple(
        httpcore.Http500, "IOError", "file can't be served", e.msg, sess
      )

  if (not isHead) and rangeHeader == "":
    if ipAddress.isSome():
      log.info("downloaded", ip = ipAddress.unwrap())
    else:
      log.info("downloaded (ip unknown)")
    if (let i = addon_downloads.trackAddonDownload(s, version, ipAddress); not i.isOk()):
      log.error("error tracking download", e = i.unwrapErr())

  return (code: codeOnSuccess, body: m)

proc servePreview(
    parts: var PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    sess: Res[SessionRef],
): RespType =
  serveAssetGeneric(parts, responseHeaders, requestHeaders, config.PreviewsDir, sess)

proc GET*(
    fullPath: Path,
    parts: var PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    sess: Res[SessionRef],
    kind: AssetKind,
    ipAddress: Opt[string] = Opt.none(string),
    isHead: bool = false,
): void =
  ##[
  ==================== =================================== =
  Endpoint             Description                         Args
  ==================== =================================== =
  /assets/*            Serve a file from the assets
                       directory
  
  /download/<id>       Download an add-on version
  
  /previews/*          Serve a file from the previews
                       directory
  
  /favicon.ico         Website favicon
  ==================== =================================== =
  ]##

  var failedVibeCheck = false
  var resp =
    if kind != Favicon and len(parts) < 1:
      ep.makeErrorTuple(
        httpcore.Http404, "No file specified", "file not found", "sorry nothing", sess
      )
    else:
      var savedPart: string
      if kind != Favicon:
        # check for the Funnies
        # Sanitize the parsed path parts
        for i in parts:
          # This should cover hidden files and the funny
          # "./" "../" ".../" stuff
          if len(i) > 0 and i[0] == '.':
            savedPart = i
            failedVibeCheck = true
            break
      if failedVibeCheck:
        ep.makeErrorTuple(
          httpcore.Http404, "Funny path traversal attempt detected", "file not found",
          savedPart, sess,
        )
      else:
        let res =
          case kind
          of Download:
            serveDownload(
              parts[0], responseHeaders, requestHeaders, ipAddress, isHead, sess
            )
          of Asset:
            serveAsset(parts, responseHeaders, requestHeaders, sess)
          of Preview:
            servePreview(parts, responseHeaders, requestHeaders, sess)
          of Favicon:
            serveFavicon(responseHeaders, requestHeaders, sess)
        # if res.code.ord notIn {httpcore.Http200.ord, httpcore.Http206.ord}:
        #   failedVibeCheck = true
        res

  #[ # part reply is very tricky (well, no shit)
    debugEcho http.replyStart(resp.code, len(resp.body), responseHeaders)
    # trickle in
    const rate = 3_000_000
    var i = 0
    while true:
      #gs.suspend(1000)
      let adjustedRate =
        if (i + rate > len(resp.body)):
          len(resp.body) - i
        else:
          rate
      debugEcho "i: ", i, " rate: ", adjustedRate
      let (status, bytes) = http.replyMore(resp.body.addr, i, adjustedRate)
      debugEcho "stat: ", status, " #: ", bytes
      if i >= len(resp.body):
        break
      case status
      of Complete:
        i += rate
        continue
      of Fail:
        break
      of TryAgain:
        gs.suspend(100)
        continue
      of Progress:
        continue
    debugEcho http.replyFinish()
  ]#

  http.reply(resp.code, resp.body, responseHeaders)
