import ../util/resutils
import ../util/log

from ../util/router import
  makeRouter, StringTableRef, newStringTable, re2, match, RegexMatch2, groupNames,
  group, reNonCapture, startsWith

from std/strutils import nil
from std/httpcore as hc import nil
from ../util/datatypes as dt import nil
from ../util/parse/http_path as pp import nil

proc handle*(
    mtd: string,
    fullPath: pp.Path,
    parts: var pp.PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    sess: Res[dt.SessionRef],
): dt.RespType =
  var r: dt.RespType
  let origPath = "/" & strutils.join(parts, "/")
  let ptrToResponseHeader = responseHeaders.addr
  makeRouter("route"):
    get "/html/latestReview":
      let body =
        """
<div class="billboard">
  <p style="font-size:3em">Huh???</p>
</div>
"""
      ptrToResponseHeader[].add("Content-Type: text/html")
      r = (hc.Http200, body)
    default:
      let body = "Not found"
      r = (hc.Http404, body)
  route((if mtd == "HEAD": "GET" else: mtd), origPath)
  return r
