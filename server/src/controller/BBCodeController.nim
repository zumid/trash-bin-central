import std/strtabs
import ../util/resutils
import ../util/datatypes

from std/httpcore import nil
from ../util/parse/http_path import Path, PathParts
from ../render import nil

{.push raises: [].}

proc GET*(
    fullPath: Path,
    parts: var PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    sess: Res[SessionRef],
): RespType =
  ##[
  ==================== =================================== =
  Endpoint             Description                         Args
  ==================== =================================== =
  /bbcode/             BBCode preview page                 - **input**: string (BBCode text)
  ==================== =================================== =
  ]##
  let body = render.BBCodePreview(strtabs.getOrDefault(fullPath.query, "input"))
  return (code: httpcore.Http200, body: body)
