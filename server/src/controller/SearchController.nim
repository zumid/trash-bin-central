import std/strtabs
import ../util/log
import ../util/resutils

from ../util/parse/http_path import PathParts, Path, urlFromPath
from ../util/datatypes import RespType, SessionRef, AddonBlurb, SearchQuery, TermType

from std/httpcore as hc import nil
from std/strutils import nil
from ../render import nil
from ../util/errorpages as ep import nil
from ../util/db2object/addon_info import nil
from ../util/tbcontext import nil

const
  fiTerm = "term"
  fiTermType = "term-type"
  fiAuthor = "author"
  fiTags = "tags"
  fiResultsPerPage = "n"
  fiResultsAfter = "after"
  fiSimpleQuery = "q"

proc queryObjectFromRequest(searchQuery: StringTableRef): Res[SearchQuery] =
  var res = SearchQuery()

  # Either way, showResultsAfter and numOfResults can be configured
  res.showResultsAfter =
    if searchQuery.hasKey(fiResultsAfter):
      try:
        Opt[int].some(strutils.parseInt(searchQuery.getOrDefault(fiResultsAfter)))
      except ValueError as e:
        log.error("stopped at showResultsAfter", e = e.message())
        return err("invalid value passed to after, must be a number")
    else:
      Opt.none(int)

  res.numOfResults =
    if searchQuery.hasKey(fiResultsPerPage):
      try:
        Opt[int].some(strutils.parseInt(searchQuery.getOrDefault(fiResultsPerPage)))
      except ValueError as e:
        log.error("stopped at numOfResults", e = e.message())
        return err("invalid value passed to n, must be a number")
    else:
      Opt.none(int)

  if searchQuery.hasKey(fiSimpleQuery):
    # this is a "simple" search query, override everything
    # assume that if empty, q does not exist
    res.term = Opt[string].some(searchQuery.getOrDefault(fiSimpleQuery))
    res.termType = TermType.AddonOrLevelTerm
    if not res.numOfResults.isSome():
      # default value
      res.numOfResults = Opt[int].some(10)
  else:
    # Every parameter is configurable
    res.term =
      if searchQuery.hasKey(fiTerm):
        Opt[string].some(searchQuery.getOrDefault(fiTerm))
      else:
        Opt.none(string)

    res.termType =
      if res.term.isSome():
        try:
          strutils.parseEnum[TermType](searchQuery.getOrDefault(fiTermType))
        except ValueError as e:
          log.error("stopped at TermType", e = e.message())
          if not searchQuery.hasKey(fiTermType):
            return err("term-type is empty")
          return err("invalid value for term-type")
      else:
        AddonTerm

    res.author =
      if searchQuery.hasKey(fiAuthor):
        Opt[string].some(searchQuery.getOrDefault(fiAuthor))
      else:
        Opt.none(string)

    # Split tags here
    res.tags =
      if searchQuery.hasKey(fiTags):
        var tags: seq[string]
        for tag in strutils.split(searchQuery.getOrDefault(fiTags), ','):
          tags.add(strutils.strip(tag))
        Opt[seq[string]].some(tags)
      else:
        Opt.none(seq[string])

  return ok(res)

proc GET*(
    fullPath: Path,
    parts: var PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    sess: Res[SessionRef],
): RespType =
  ##[
  ==================== =================================== =
  Endpoint             Description                         Args
  ==================== =================================== =
  /search              Search add-ons                      - **term**: string | ""
                                                           - **term-type**: "addon" | "level" | "addon-or-level" | ""
                                                           - **author**: string | ""
                                                           - **tags**: (tag ",")+ | ""
                                                           - **n**: int (results per page) | ""
                                                           - **after**: int | ""
                                                           - **q**: string (simple query) | ""
  ==================== =================================== =
  ]##
  {.warning: "TODO work on the pagination side of things".}

  let searchQuery = fullPath.query
  log.info("queries", q = $searchQuery)

  let sqResult = queryObjectFromRequest(searchQuery)
  if not sqResult.isOk():
    let e = sqResult.unwrapErr()
    return ep.makeErrorTuple(hc.Http400, "cannot generate query object", e, e, sess)

  let sq = sqResult.unwrap()

  log.info("Gen QO", sq = sq)
  let s = tbcontext.getCurrentDbConnection()
  var numAddonsFound = -1

  let adnsResult = addon_info.getAddonsFromSearchQuery(s, sq, numAddonsFound)
  if not adnsResult.isOk():
    return ep.makeErrorTuple(
      hc.Http500,
      "cannot get addon results",
      "cannot get addon results",
      adnsResult.unwrapErr(),
      sess,
    )

  let adns = adnsResult.unwrap()
  log.info("number of addons in search", n = numAddonsFound)
  var blurbs: seq[AddonBlurb]
  for a in adns:
    let blurb = addon_info.addonToAddonBlurb(s, a)
    if blurb.isOk():
      blurbs.add(blurb.unwrap())
    else:
      log.error("can't turn addon into blurb", addon = a.id, e = blurb.unwrapErr())

  let nextUrl = block:
    if len(adns) > 0:
      var n = fullPath
      n.query["after"] = $adns[^1].id.unwrap()
      Opt[string].some(urlFromPath(n))
    else:
      Opt.none(string)

  let body = render.SearchPage(sess, blurbs, numAddonsFound, nextUrl)
  return (code: hc.Http200, body: body)
