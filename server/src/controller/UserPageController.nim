import std/strtabs
import ../util/resutils
import ../util/log

from ../util/router import
  makeRouter, StringTableRef, newStringTable, re2, match, RegexMatch2, groupNames,
  group, reNonCapture, startsWith

from std/httpcore as ht import nil
from std/strutils as su import nil
from ../util/datatypes as dt import nil
from ../util/parse/http_path as pp import nil
from ../util/errorpages as ep import nil
from ../util/db2object/users import nil
from ../util/db2object/addon_info import nil
from ../util/db2object/addon_reviews import nil
from ../util/tbcontext import nil
from ../render import nil

proc handle*(
    mtd: string,
    fullPath: pp.Path,
    parts: var pp.PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    sess: Res[dt.SessionRef],
): dt.RespType =
  let s = tbcontext.getCurrentDbConnection()
  var r: dt.RespType
  let origPath = "/" & su.join(parts, "/")
  router.makeRouter("route"):
    get "/{id}":
      let uid = block:
        let i = pathParams.getOrDefault("id")
        try:
          if len(i) < 1:
            r = ep.makeErrorTuple(
              ht.Http404, "not found", "not found", "invalid user id", sess
            )
            return
          su.parseInt(i)
        except ValueError as e:
          log.error("invalid uid", e = e.message())
          r = ep.makeErrorTuple(
            ht.Http404, "not found", "not found", "invalid user id", sess
          )
          return
      let uinfo = block:
        let u = users.getUserById(s, uid)
        if not u.isOk():
          r =
            ep.makeErrorTuple(ht.Http404, "not found", "not found", u.unwrapErr(), sess)
          return
        u.unwrap()
      let addonList = block:
        var a: seq[dt.AddonBlurb]
        let i = addon_info.getAddonsByUser(s, uinfo)
        if not i.isOk():
          r = ep.makeErrorTuple(
            ht.Http500,
            "can't load user page",
            "can't load user page",
            i.unwrapErr(),
            sess,
          )
          return
        for addon in i.unwrap():
          let blurb = addon_info.addonToAddonBlurb(s, addon)
          if blurb.isOk():
            a.add(blurb.unwrap())
        a
      let reviewList = block:
        let i = addon_reviews.getReviewsByUser(s, uinfo)
        if not i.isOk():
          r = ep.makeErrorTuple(
            ht.Http500,
            "can't load user page",
            "can't load user page",
            i.unwrapErr(),
            sess,
          )
          return
        i.unwrap()
      let uStats = block:
        let i = users.getUserStats(s, uinfo)
        if not i.isOk():
          r = ep.makeErrorTuple(
            ht.Http500,
            "can't load user page",
            "can't load user page",
            i.unwrapErr(),
            sess,
          )
          return
        i.unwrap()
      let body = render.UserPage(uinfo, uStats, addonList, reviewList, sess)
      r = (ht.Http200, body)
    default:
      r = ep.makeErrorTuple(ht.Http404, "not found", "not found", "invalid path", sess)
  route((if mtd == "HEAD": "GET" else: mtd), origPath)
  return r
