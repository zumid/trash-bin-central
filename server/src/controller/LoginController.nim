import std/strtabs
import ../util/resutils
import ../util/log

from std/strutils as su import nil
from std/httpcore as ht import nil
from ../util/datatypes as dt import nil
from ../util/parse/http_path as pp import nil
from ../util/errorpages as ep import nil
from ../util/db2object/sessions import nil
from ../util/db2object/users import nil
from ../render import nil

from ../process/receive_post_form import receivePostForm
from ../util/helper/strtab_to_list import asList
from ../util/helper/strhelper import getFormInput
from ../util/tbcontext import getCurrentDbConnection
from ../util/helper/redirect import redirect
from ../util/router import
  makeRouter, StringTableRef, newStringTable, re2, match, RegexMatch2, groupNames,
  group, reNonCapture, startsWith

# common field names
const
  fiAcsrfToken = "csrf"
  fiFormType = "form-type"

# login-specific field names
const
  fiLoginUsername = "mxnvun"
  fiLoginPassword = "xmnxpw"
  fiLoginRemember = "remem"

# signup-specific field names
const
  fiSignupUsername = "mxnvun"
  fiSignupPassword = "xmnxpw"
  fiSignupConfirmPassword = "xmnxcpw"

{.push gcsafe, raises: [].}

proc validateLoginForm(
    s: StringTableRef, sess: dt.SessionRef, formInput: StringTableRef
): Result[void, seq[dt.FlashMessage]] =
  let
    username = s.getFormInput(fiLoginUsername)
    password = s.getFormInput(fiLoginPassword)
    remember = s.getFormInput(fiLoginRemember)
    token = s.getFormInput(fiAcsrfToken)

  formInput["login-username"] = username

  var errors = newSeq[dt.FlashMessage](0)

  if len(token) < 1:
    errors.add(dt.InvalidSession)
  else:
    if not sessions.checkAntiCsrfToken(token, sess.id).isOk():
      errors.add(dt.InvalidSession)

  if len(username) < 1:
    errors.add(dt.NoUsername)

  if len(password) < 1:
    errors.add(dt.NoPassword)

  if len(errors) > 0:
    return err(errors)

  errors.setLen(0)

  let db = getCurrentDbConnection()

  let authedUserResult = users.getUserByAuth(db, username, password)
  if not authedUserResult.isOk():
    errors.add(dt.InvalidLogin)
    return err(errors)

  if not sessions.attachUserToSession(authedUserResult.unwrap(), sess.id).isOk():
    errors.add(dt.UnknownError)
    return err(errors)

  return ok()

proc validateUsername(s: string, errors: var seq[dt.FlashMessage]) =
  if len(s) > 32:
    errors.add(dt.UsernameTooLong)
    return
  when false:
    for i in s:
      case i
      of 'A' .. 'Z':
        discard
      of 'a' .. 'z':
        discard
      of '0' .. '9':
        discard
      of ' ':
        discard
      of '.':
        discard
      of '#':
        discard
      else:
        errors.add(dt.InvalidUsernameSpecs)
        return

proc validatePassword(s: string, errors: var seq[dt.FlashMessage]) =
  if len(s) < 8:
    errors.add(dt.PasswordTooShort)

proc validateSignupForm(
    s: StringTableRef, sess: dt.SessionRef, formInput: StringTableRef
): Result[void, seq[dt.FlashMessage]] =
  let
    username = s.getFormInput(fiSignupUsername)
    password = s.getFormInput(fiSignupPassword)
    confirm = s.getFormInput(fiSignupConfirmPassword)
    token = s.getFormInput(fiAcsrfToken)

  formInput["signup-username"] = username

  var errors = newSeq[dt.FlashMessage](0)

  if len(token) < 1:
    errors.add(dt.InvalidSession)
  else:
    if not sessions.checkAntiCsrfToken(token, sess.id).isOk():
      errors.add(dt.InvalidSession)

  if len(username) < 1:
    errors.add(dt.NoUsername)
  else:
    username.validateUsername(errors)

  if len(password) < 1:
    errors.add(dt.NoPassword)
  else:
    password.validatePassword(errors)

  if len(confirm) < 1:
    errors.add(dt.PasswordsDontMatch)
  else:
    if confirm != password:
      errors.add(dt.PasswordsDontMatch)

  if len(errors) > 0:
    return err(errors)

  # actually register the user
  let db = getCurrentDbConnection()

  # Yeah, it's a pretty cursed if statement
  if (
    let existResult = users.userExists(db, username)
    if not existResult.isOk():
      errors.add(dt.UnknownError)
      return err(errors)
    existResult.unwrap()
  ):
    errors.add(dt.UsernameExists)
    return err(errors)

  if not users.insertUser(db, username, password).isOk():
    errors.add(dt.CannotRegister)
    return err(errors)
  else:
    log.info("user registered", username)

  return ok()

proc handle*(
    mtd: string,
    fullPath: pp.Path,
    parts: var pp.PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    sess: Res[dt.SessionRef],
): dt.RespType =
  var r: dt.RespType
  let origPath = "/" & su.join(parts, "/")
  let ptrToResponseHeader = responseHeaders.addr
  router.makeRouter("route"):
    get "/":
      let body = render.Login(sess, newStringTable())
      r = (ht.Http200, body)
    post "/":
      ##[
        form:
          form-type: "login" | "signup"
          case form-type
          of "login":
            mxnvun: string(user name)
            xmnxpw: string(password)
            remem: bool("remember" flag)
          of "signup":
            mxnvun: string(user name)
            xmnxpw: string(password)
            xmnxcpw: string(confirm password)
      ]##
      let params = block:
        let p = receivePostForm()
        if not p.isOk():
          r = ep.makeErrorTuple(
            ht.Http400, "get POST", "unable to get form params", p.unwrapErr(), sess
          )
          return
        p.unwrap()
      let ss = block:
        if sess.isOk():
          sess.unwrap()
        else:
          r = ep.makeErrorTuple(
            ht.Http500, "get session", "unable to get session", sess.unwrapErr(), sess
          )
          return
      var formInput = newStringTable()
      case params.getFormInput(fiFormType)
      of "login":
        let validated = params.validateLoginForm(ss, formInput)
        if not validated.isOk():
          for i in validated.unwrapErr():
            discard sessions.addFlashMessage(ss.id, i)
          let body = render.Login(sess, formInput)
          r = (ht.Http400, body)
          return
        let redirectUrl = "/"
        r = redirect(redirectUrl, ptrToResponseHeader)
      of "signup":
        let validated = params.validateSignupForm(ss, formInput)

        if not validated.isOk():
          for i in validated.unwrapErr():
            discard sessions.addFlashMessage(ss.id, i)
          let body = render.Login(sess, formInput)
          r = (ht.Http400, body)
          return
        let redirectUrl = "/login"
        r = redirect(redirectUrl, ptrToResponseHeader)
      else:
        discard sessions.addFlashMessage(ss.id, dt.InvalidFormType)
        let body = render.Login(sess, formInput)
        r = (ht.Http200, body)
    default:
      r = ep.makeErrorTuple(ht.Http404, "not found", "not found", "invalid path", sess)
  route((if mtd == "HEAD": "GET" else: mtd), origPath)
  return r
