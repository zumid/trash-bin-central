import std/strtabs
import ../util/resutils
import ../util/log

from ../util/helper/redirect import redirect
from ../process/receive_post_form import receivePostForm
from ../util/helper/strhelper import getFormInput

# from std/httpcore as ht import nil
from ../util/datatypes as dt import nil
from ../util/parse/http_path as pp import nil
# from ../util/errorpages as ep import nil
from ../util/db2object/sessions import nil

const fiAcsrfToken = "lgacsrf"

proc POST*(
    fullPath: pp.Path,
    parts: var pp.PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    sess: Res[dt.SessionRef],
): dt.RespType =
  ##[
  ==================== =================================== =
  Endpoint             Description                         Args
  ==================== =================================== =
  /logout              Log out of the current session      - **lgacsrf**: string (CSRF token)
  ==================== =================================== =
  ]##
  let s = block:
    if sess.isOk():
      sess.unwrap()
    else:
      log.debug("no session, redirecting to homepage")
      return redirect("/", responseHeaders)

  let params = block:
    let p = receivePostForm()
    if not p.isOk():
      log.debug("no form input, redirecting to homepage")
      return redirect("/", responseHeaders)
    p.unwrap()

  let token = params.getFormInput(fiAcsrfToken)
  if not sessions.checkAntiCsrfToken(token, s.id).isOk():
    log.debug("token invalid, redirecting to homepage")
    return redirect("/", responseHeaders)

  log.info("user logout", session = s.id, user = s.userId)

  let cleanSessResult = sessions.cleanSessions(Opt[string].ok(s.id))
  if not cleanSessResult.isOk():
    log.error("unable to logout, redirecting to homepage", e = cleanSessResult.error())
    return redirect("/", responseHeaders)

  log.info("logged out successfully")
  return redirect("/", responseHeaders)
