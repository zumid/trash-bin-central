import std/strtabs
import ../util/log
import ../util/datatypes
import ../util/resutils

from ../util/parse/http_path import PathParts, Path, pop
from ../util/tbcontext import getCurrentDbConnection

from std/httpcore as hc import nil
from ../render import nil
from ../util/db2object/addon_reviews import nil
from ../util/errorpages as ep import nil

proc latestReviewsPage(sess: Res[SessionRef], s: ref Database): RespType =
  let reviewsResult = addon_reviews.getLatestReviews(s, 10)
  let reviews =
    if reviewsResult.isOk():
      reviewsResult.unwrap()
    else:
      return ep.makeErrorTuple(
        hc.Http500,
        "fetch error",
        "error getting latest reviews",
        reviewsResult.unwrapErr(),
        sess,
      )
  let body = render.LatestReviews(sess, reviews)
  return (code: hc.Http200, body: body)

proc GET*(
    fullPath: Path,
    parts: var PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    sess: Res[SessionRef],
): RespType =
  ##[
  ==================== =================================== =
  Endpoint             Description                         Args
  ==================== =================================== =
  /reviews             List of latest reviews
  ==================== =================================== =
  ]##

  var s = getCurrentDbConnection()

  if (len(parts) > 1):
    return ep.makeErrorTuple(
      hc.Http404, "page not found", "page not found", "page not found", sess
    )

  return latestReviewsPage(sess, s)
