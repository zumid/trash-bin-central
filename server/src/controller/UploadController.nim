import std/strtabs
import ../util/log
import ../util/dbmanager
import ../util/helper/redirect
import ../util/resutils
import ../util/datatypes

from ../util/parse/http_path import PathParts, Path
from ../process/receive_addon_upload import receiveAddonUpload, UploadedAddon
from ../util/tbcontext import getCurrentDbConnection

from std/httpcore as hc import nil
from ../render import nil
from ../util/db2object/sessions import nil

{.push gcsafe, raises: [].}

proc GET*(
    fullPath: Path,
    parts: var PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    session: Res[SessionRef],
): RespType =
  ##[
  ==================== =================================== =
  Endpoint             Description                         Args
  ==================== =================================== =
  /upload              Add-on upload page
  ==================== =================================== =
  ]##
  let body = render.Upload(session = session, priorInput = newStringTable())
  return (code: hc.Http200, body: body)

proc POST*(
    fullPath: Path,
    parts: var PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    s: Res[SessionRef],
): RespType =
  ##[
  ==================== =================================== =
  Endpoint             Description                         Args
  ==================== =================================== =
  /upload              Upload an add-on                    - **title**: string
                                                           - **author**: string
                                                           - **compat**: "srb2-2.2" | "srb2-2.1" |
                                                                         "srb2-2.0" | ...
                                                           - **synopsis**: string
                                                           - **desc**: string (Addon description)
                                                           - **main-file**: file
                                                           - **vers**: string (Version ID)
                                                           - **changelog**: string
                                                           - **agree**: bool (Checkbox)
                                                           - **csrf**: string (CSRF token)
  ==================== =================================== =
  ]##

  # Must have session
  let formInput = newStringTable()
  let ss =
    if s.isOk():
      s.unwrap()
    else:
      let body = render.Upload(session = s, priorInput = formInput)
      return (code: hc.Http400, body: body)
  var db = getCurrentDbConnection()
  var editCode = Opt.none(string)

  # Input validating is handled in receiveAddonUpload as well
  # isOwner was passed in for the whole "logged in user owns this addon, so they CAN edit it"
  # thing, so pass some dummy value in there
  let receiveResult = receiveAddonUpload(
    db, ss.id, formInput, editCode, Opt.none(Addon), Opt[bool].some(true)
  )

  if not receiveResult.isOk():
    let v = receiveResult.unwrapErr()
    if len(v) < 1:
      discard sessions.addFlashMessage(ss.id, UnknownError)
    else:
      for msg in v:
        discard sessions.addFlashMessage(ss.id, msg)
    var code = if CannotUploadFile in v: hc.Http500 else: hc.Http400
    let body = render.Upload(session = s, priorInput = formInput)
    return (code: code, body: body)

  let v = receiveResult.unwrap()
  log.trace("got result", detail = v)

  if editCode.isSome():
    let ec = editCode.unwrap()
    discard sessions.addFlashMessageCustom(
      ss.id,
      "Addon uploaded! You are currently not logged in, so save this edit code or you won't be able to edit: " &
        ec,
    )
  else:
    # Does this case need to be handled? Because no edit code SHOULD
    # mean the upload proc should error out anyway.
    discard

  # Assuming that id will *always* be filled in.
  let redirectUrl = "/addons/" & $(v.details.id.unwrap())
  return redirect(redirectUrl, responseHeaders)
