# Unqualified imports
import std/strtabs
import ../util/log
import ../util/resutils
import ../util/helper/redirect
import ../util/parse/http_path
import ../util/datatypes

# Partially-qualified imports
from ../util/helper/strhelper import getFormInput
from ../util/tbcontext import getCurrentDbConnection
from ../process/receive_post_form import receivePostForm
from ../util/db2object/session_user import getSessionUser
from ../util/router import
  makeRouter, StringTableRef, newStringTable, re2, match, RegexMatch2, groupNames,
  group, reNonCapture, startsWith

# Qualified imports
from std/strutils import nil
from std/httpcore as hc import nil
from ../render import nil
from ../process/receive_addon_upload import nil
from ../util/db2object/addon_info import nil
from ../util/db2object/addon_reviews import nil
from ../util/db2object/addon_versions import nil
from ../util/db2object/addon_tags import nil
from ../util/db2object/addon_levels import nil
from ../util/db2object/sessions import nil
from ../util/errorpages as ep import nil
from ../util/parse/bbcode import nil

{.push gcsafe, raises: [].}

proc addonListPage(s: ref Database, fullPath: Path, sess: Res[SessionRef]): RespType =
  var addonsLatest: seq[AddonBlurb]

  let res = addon_info.getAllAddons(s, true, addon_info.UploadDate, -1)
  if not res.isOk():
    log.error("can't get latest addons", e = res.unwrapErr())
  else:
    for addon in res.unwrap():
      let blurb = addon_info.addonToAddonBlurb(s, addon)
      if not blurb.isOk():
        log.error(
          "can't get blurb for this addon", e = blurb.unwrapErr(), id = addon.id
        )
        continue
      addonsLatest.add(blurb.unwrap())

  let body = render.Addons(addonsLatest = addonsLatest, sess)
  return (code: hc.Http200, body: body)

proc addonInfoPage(s: ref Database, addon: Addon, sess: Res[SessionRef]): RespType =
  let versionInfos = block:
    let v = addon_versions.getAddonVersions(s, addon)
    if not v.isOk():
      return ep.makeErrorTuple(
        hc.Http500,
        "can't get addon versions!",
        "Cannot get addon versions",
        v.unwrapErr(),
        sess,
      )
    v.unwrap()

  if len(versionInfos) < 1:
    return ep.makeErrorTuple(
      hc.Http404, "no addon versions!", "No versions exist for the specified addon",
      "no addon versions", sess,
    )

  let addonReviews = block:
    let reviews = addon_reviews.getAddonReviews(s, addon)
    if not reviews.isOk():
      log.error("error getting reviews", e = reviews.unwrapErr())
      when false:
        # Tripped an internal compiler error!!
        # cannot map the empty seq type to a C type
        @[]
      else:
        newSeq[Review](0)
    else:
      reviews.unwrap()

  let addonTags = block:
    let res = addon_tags.getTagsForAddon(s, addon)
    if not res.isOk():
      @[]
    else:
      res.unwrap()

  let addonLevels = block:
    let res = addon_levels.getLevelsFromAddonLatest(s, addon)
    if not res.isOk():
      @[]
    else:
      res.unwrap()

  let body = render.AddonInfo(
    addon,
    addonTags,
    addonLevels,
    versionInfos,
    sess,
    addonReviews,
    addon_info.previewFilenameForAddon(addon),
  )
  return (code: hc.Http200, body: body)

const
  fiEditTitle = "title"
  fiEditAuthor = "author"
  fiEditCompat = "compat"
  fiEditSynopsis = "synopsis"
  fiEditDescription = "desc"
  fiEditCode = "edit-code"

proc addonEditPage(
    session: Res[SessionRef], addon: Addon, isOwner: Opt[bool]
): RespType =
  var priorInput = newStringTable(
    {
      fiEditTitle: addon.name,
      fiEditAuthor: addon.author,
      fiEditSynopsis: addon.synopsis,
      fiEditDescription:
        if addon.description.isOk():
          addon.description.unwrap()
        else:
          "",
    }
  )
  {.warning: "TODO map compat table".}
  let body = render.AddonEdit(
    session,
    priorInput,
    if isOwner.isSome():
      not isOwner.unwrap()
    else:
      true,
  )
  return (code: hc.Http200, body: body)

proc addonAddVersionPage(session: Res[SessionRef], isOwner: Opt[bool]): RespType =
  let body = render.AddonAddVersion(
    session,
    newStringTable(),
    if isOwner.isSome():
      not isOwner.unwrap()
    else:
      true,
  )
  return (code: hc.Http200, body: body)

proc addonViewUpdatesPage(
    s: ref Database, addon: Addon, sess: Res[SessionRef]
): RespType =
  let versionInfos = block:
    let res = addon_versions.getAddonVersions(s, addon)
    if not res.isOk():
      return ep.makeErrorTuple(
        hc.Http500,
        "can't get addon versions!",
        "Cannot get addon versions",
        res.unwrapErr(),
        sess,
      )
    res.unwrap()
  let body = render.AddonUpdates(versionInfos, sess)
  return (code: hc.Http200, body: body)

proc isOwnerOf(currentUser: Opt[User], addon: Addon): Opt[bool] =
  if addon.authorId.isSome():
    if currentUser.isSome():
      Opt[bool].some(
        currentUser.unwrap().id.isSome() and
          (currentUser.unwrap().id.unwrap() == addon.authorId.unwrap())
      )
    else:
      # There is no user currently logged in, but the add-on has a clear
      # author. So they don't own this add-on.
      Opt[bool].some(false)
  else:
    # The add-on has no registered owner
    # The owner is "anonymous"
    if currentUser.isSome():
      # So people who aren't "anonymous" shouldn't be able
      # to edit this
      Opt[bool].some(false)
    else:
      # Only "anonymous" people can edit
      Opt.none(bool)

type ReviewKind = enum
  Review = "review"
  Reply = "reply"

const
  fiReviewType = "type"
  fiACSRFToken = "token"
  fiReviewRating = "review-rating"
  fiReviewContent = "review-content"
  fiReplyContent = "reply-content"
  fiReviewReplyTo = "reply-to"

proc addonValidateReview(t: StringTableRef, session: SessionRef): Failable =
  let ratingStr = t.getFormInput(fiReviewRating)
  if len(ratingStr) < 1:
    return err("must fill out rating")
  try:
    discard strutils.parseInt(ratingStr)
  except ValueError:
    return err("invalid rating")
  let reviewContent = t.getFormInput(fiReviewContent)
  if len(reviewContent) < 1:
    return err("an empty review? really?")
  if (let x = bbcode.bbcode2HtmlResult(reviewContent); not x.isOk()):
    return err("bbcode parsing error: " & x.unwrapErr())
  {.warning: "TODO: do we need a word count?".}
  if not sessions.checkAntiCsrfToken(t.getOrDefault(fiACSRFToken), session.id).isOk():
    return err("refresh the page and try again")
  return ok()

proc addonProcessReview(
    t: StringTableRef,
    s: ref Database,
    addon: Addon,
    session: SessionRef,
    resp: var seq[string],
): RespType =
  # Assume form has already been validated beforehand
  {.cast(raises: []).}:
    let rating = strutils.parseInt(t.getFormInput(fiReviewRating))
  let review = t.getFormInput(fiReviewContent)
  let user = s.getSessionUser(session)
  if (
    let i = addon_reviews.insertReviewToDb(s, addon, user, rating, review)
    not i.isOk()
  ):
    return ep.makeErrorTuple(
      hc.Http400,
      "cannot upload review",
      "problem uploading review",
      i.unwrapErr(),
      Res[SessionRef].ok(session),
    )
  discard sessions.addFlashMessageCustom(session.id, "Review posted successfully")
  return redirect("/addons/" & $(addon.id.unwrap()), resp)

proc addonValidateReply(t: StringTableRef, session: SessionRef): Failable =
  let reviewIdStr = t.getFormInput(fiReviewReplyTo)
  if len(reviewIdStr) < 1:
    return err("which review to reply to?")
  try:
    discard strutils.parseInt(reviewIdStr)
  except ValueError:
    return err("invalid id")
  let replyContent = t.getFormInput(fiReplyContent)
  if len(replyContent) < 1:
    return err("empty reply")
  if (let x = bbcode.bbcode2HtmlResult(replyContent); not x.isOk()):
    return err("bbcode parsing error: " & x.unwrapErr())
  if not sessions.checkAntiCsrfToken(t.getOrDefault(fiACSRFToken), session.id).isOk():
    return err("refresh the page and try again")
  return ok()

proc addonProcessReply(
    t: StringTableRef,
    s: ref Database,
    addon: Addon,
    session: SessionRef,
    resp: var seq[string],
): RespType =
  # Assume form has been validated beforehand
  {.cast(raises: []).}:
    let reviewId = strutils.parseInt(t.getFormInput(fiReviewReplyTo))
  let reply = t.getFormInput(fiReplyContent)
  let user = s.getSessionUser(session)
  if (
    let i = addon_reviews.insertReviewReplyToDb(s, addon, reviewId, user, reply)
    not i.isOk()
  ):
    return ep.makeErrorTuple(
      hc.Http400,
      "cannot upload reply",
      "problem uploading reply",
      i.unwrapErr(),
      Res[SessionRef].ok(session),
    )

  discard sessions.addFlashMessageCustom(session.id, "Reply posted successfully")
  return redirect("/addons/" & $(addon.id.unwrap()), resp)

proc addonUploadReview(
    sess: Res[SessionRef], s: ref Database, addon: Addon, resp: ptr seq[string]
): RespType {.raises: [].} =
  let postContentResult = receivePostForm()
  if not postContentResult.isOk():
    log.error("can't get review contents", e = postContentResult.unwrapErr())
    return ep.makeErrorTuple(
      hc.Http500, "can't get review contents", "couldn't upload review", "", sess
    )
  let postContent = postContentResult.unwrap()

  # Check ACSRF token
  if not sess.isOk():
    return ep.makeErrorTuple(hc.Http400, "no session", "no session", "", sess)

  let session = sess.unwrap()
  # validate form type
  let reviewKind =
    try:
      strutils.parseEnum[ReviewKind](postContent.getOrDefault(fiReviewType))
    except ValueError as e:
      return ep.makeErrorTuple(
        hc.Http400,
        "can't parse review type enum",
        "invalid review type",
        e.message(),
        sess,
      )

  case reviewKind
  of Review:
    if (let i = addonValidateReview(postContent, session); not i.isOk()):
      let e = i.unwrapErr()
      return ep.makeErrorTuple(hc.Http400, "error validating review params", e, e, sess)
    return addonProcessReview(postContent, s, addon, session, resp[])
  of Reply:
    if (let i = addonValidateReply(postContent, session); not i.isOk()):
      let e = i.unwrapErr()
      return ep.makeErrorTuple(hc.Http400, "error validating review params", e, e, sess)
    return addonProcessReply(postContent, s, addon, session, resp[])

proc addonProcessAddVersionGetResult(
    s: ref Database,
    sessId: string,
    addon: Addon,
    formInput: StringTableRef,
    isOwner: Opt[bool],
): Result[void, seq[FlashMessage]] =
  var editCodeOutput = Opt.none(string)
  if (
    let i = receive_addon_upload.receiveAddonUpload(
      s, sessId, formInput, editCodeOutput, Opt[Addon].some(addon), isOwner
    )
    not i.isOk()
  ):
    return i.convertErr()
  return ok()

proc addonProcessAddVersion(
    sess: Res[SessionRef],
    s: ref Database,
    addon: Addon,
    resp: ptr seq[string],
    isOwner: Opt[bool],
): RespType =
  let session =
    if sess.isOk():
      sess.unwrap()
    else:
      return ep.makeErrorTuple(
        hc.Http400, "no session", "invalid session!", sess.unwrapErr(), sess
      )
  let formInput = newStringTable()

  if (
    let i = addonProcessAddVersionGetResult(s, session.id, addon, formInput, isOwner)
    not i.isOk()
  ):
    for msg in i.unwrapErr():
      discard sessions.addFlashMessage(session.id, msg)
    let body = render.AddonAddVersion(
      sess,
      formInput,
      if isOwner.isSome():
        not isOwner.unwrap()
      else:
        true,
    )
    return (code: hc.Http400, body: body)

  return redirect("/addons/" & $(addon.id.unwrap()), resp[])

proc addonProcessEditGetResult(
    s: ref Database,
    postContent: StringTableRef,
    sessId: string,
    addon: Addon,
    isOwner: Opt[bool],
): Result[void, seq[FlashMessage]] =
  # edit page form names MUST BE THE SAME as upload page form names!
  var errors: seq[FlashMessage]

  let currentUser = s.getSessionUser(sessId)

  # validate addon info
  if (
    let i = receive_addon_upload.validateAddonInfoInput(postContent, currentUser.isOk())
    not i.isOk()
  ):
    errors.add(i.unwrapErr())

  # validate edit code
  if not isOwner.isSome() and (
    let i = addon_info.checkEditCode(s, addon, postContent.getOrDefault(fiEditCode))
    not i.isOk()
  ):
    errors.add(InvalidEditCode)

  # validate CSRF and agree
  if (
    let i = receive_addon_upload.validateAddonMiscInput(postContent, sessId)
    not i.isOk()
  ):
    # have to use this weird struct for some reason. If I just use `?` it checks
    # the CSRF token twice. naturally, it fails, because the first time it checks
    # it also deletes the token.
    errors.add(i.unwrapErr())

  if len(errors) > 0:
    return err(errors)

  var a2 = addon
  a2.name = postContent.getFormInput(fiEditTitle)
  a2.synopsis = postContent.getFormInput(fiEditSynopsis)
  a2.author = postContent.getFormInput(fiEditAuthor)
  a2.description = block:
    let d = postContent.getFormInput(fiEditDescription)
    if len(d) < 1:
      Opt.none(string)
    else:
      Opt[string].some(d)

  let cannotEditAddonError = Result[void, seq[FlashMessage]].err(@[CannotEditAddon])

  if (let i = addon_info.updateAddonInfo(s, a2); not i.isOk()):
    log.error("update addon info", e = i.unwrapErr())
    return cannotEditAddonError

  let targetCompat =
    try:
      strutils.parseEnum[AddonTarget](postContent.getOrDefault(fiEditCompat))
    except ValueError as e:
      log.error("determine compat level", e = e.message())
      return cannotEditAddonError

  if (let i = addon_info.updateAddonCompat(s, a2, targetCompat); not i.isOk()):
    log.error("update addon compat", e = i.unwrapErr())
    return cannotEditAddonError

  return ok()

proc addonProcessEdit(
    sess: Res[SessionRef],
    s: ref Database,
    addon: Addon,
    resp: ptr seq[string],
    isOwner: Opt[bool],
): RespType =
  let session =
    if sess.isOk():
      sess.unwrap()
    else:
      return ep.makeErrorTuple(
        hc.Http400, "no session", "invalid session!", sess.unwrapErr(), sess
      )
  let postContent = block:
    let postResult = receivePostForm()
    if postResult.isOk():
      postResult.unwrap()
    else:
      return ep.makeErrorTuple(
        hc.Http400,
        "cannot POST addon process edit",
        "cannot edit addon",
        postResult.unwrapErr(),
        sess,
      )
  if (
    let i = addonProcessEditGetResult(s, postContent, session.id, addon, isOwner)
    not i.isOk()
  ):
    for msg in i.unwrapErr():
      discard sessions.addFlashMessage(session.id, msg)
    let body = render.AddonEdit(
      sess,
      postContent,
      if isOwner.isSome():
        not isOwner.unwrap()
      else:
        true,
    )
    return (code: hc.Http400, body: body)
  return redirect("/addons/" & $(addon.id.unwrap()), resp[])

template addonFromRouteId(s: ref Database) {.dirty.} =
  let addon = block:
    let addonId =
      try:
        let id = pathParams.getOrDefault("id")
        if len(id) < 1:
          r = ep.makeErrorTuple(
            hc.Http404, "page not found", "page not found", "page not found", sess
          )
          return
        strutils.parseInt(id)
      except CatchableError as e:
        r = ep.makeErrorTuple(
          hc.Http400,
          "invalid addon ID!",
          "Invalid addon ID passed in",
          "cannot parse addon ID: " & e.msg,
          sess,
        )
        return
    let addonResult = addon_info.getAddonById(s, addonId)
    if not addonResult.isOk():
      r = ep.makeErrorTuple(
        hc.Http400,
        "invalid addon!",
        "Invalid addon passed in",
        "cannot show addon: " & addonResult.unwrapErr(),
        sess,
      )
      return
    let addonExist = addonResult.unwrap()
    if not addonExist.isSome():
      r = ep.makeErrorTuple(
        hc.Http404, "addon not found!", "Addon not found", "Addon not found", sess
      )
      return
    addonExist.unwrap()

proc handle*(
    mtd: string,
    fullPath: Path,
    parts: var PathParts,
    responseHeaders: var seq[string],
    requestHeaders: StringTableRef,
    sess: Res[SessionRef],
): RespType =
  var s = getCurrentDbConnection()
  var r: RespType
  let origPath = "/" & strutils.join(parts, "/")
  let ptrToResponseHeader = responseHeaders.addr
  makeRouter("route"):
    get "/":
      log.info("firing list page")
      r = addonListPage(s, fullPath, sess)
      #
    get "/{id}":
      log.info("firing id page")
      addonFromRouteId(s)
      r = addonInfoPage(s, addon, sess)
      #
    get "/{id}/edit":
      log.info("firing edit page")
      addonFromRouteId(s)
      let ownerLoggedIn = getSessionUser(s, sess).isOwnerOf(addon)
      r =
        if ownerLoggedIn.isSome() and not ownerLoggedIn.unwrap():
          ep.makeErrorTuple(hc.Http400, "forbidden", "forbidden", "forbidden", sess)
        else:
          addonEditPage(sess, addon, ownerLoggedIn)
      #
    post "/{id}/edit":
      ##[
        form:
          title: string
          author: string
          compat: "srb2-2.2" | "srb2-2.1" | "srb2-2.0" | etc.
          synopsis: string
          desc: string
          agree: bool
          edit-code: string
          csrf: string(CSRF token)
      ]##
      # Handled in this file
      addonFromRouteId(s)
      let ownerLoggedIn = getSessionUser(s, sess).isOwnerOf(addon)
      r =
        if ownerLoggedIn.isSome() and not ownerLoggedIn.unwrap():
          ep.makeErrorTuple(hc.Http400, "forbidden", "forbidden", "forbidden", sess)
        else:
          addonProcessEdit(sess, s, addon, ptrToResponseHeader, ownerLoggedIn)
      #
    get "/{id}/add":
      log.info("firing add page")
      addonFromRouteId(s)
      let ownerLoggedIn = getSessionUser(s, sess).isOwnerOf(addon)
      r =
        if ownerLoggedIn.isSome() and not ownerLoggedIn.unwrap():
          ep.makeErrorTuple(hc.Http400, "forbidden", "forbidden", "forbidden", sess)
        else:
          addonAddVersionPage(sess, ownerLoggedIn)
      #
    post "/{id}/add":
      ##[
        form:
          main-file: File
          vers: string(version ID)
          changelog: string
          agree: bool
          edit-code: string
          csrf: string(CSRF token)
      ]##
      # Handled primarily in receive_addon_upload because it uses the same
      # logic as uploading an add-on.
      addonFromRouteId(s)
      let ownerLoggedIn = getSessionUser(s, sess).isOwnerOf(addon)
      r =
        if ownerLoggedIn.isSome() and not ownerLoggedIn.unwrap():
          ep.makeErrorTuple(hc.Http400, "forbidden", "forbidden", "forbidden", sess)
        else:
          addonProcessAddVersion(sess, s, addon, ptrToResponseHeader, ownerLoggedIn)
      #
    get "/{id}/updates":
      log.info("firing updates page")
      addonFromRouteId(s)
      r = addonViewUpdatesPage(s, addon, sess)
      #
    post "/{id}/review":
      ##[
        form:
          type: "review" | "reply"
          token: string(CSRF token)
          case type
          of "review":
            review-rating: 1..5
            review-content: string
          of "reply":
            reply-to: int
            reply-content: string
      ]##
      addonFromRouteId(s)
      r = addonUploadReview(sess, s, addon, ptrToResponseHeader)
      #
    default:
      log.info("unknown path")
      r = ep.makeErrorTuple(
        hc.Http404, "page not found", "page not found", "page not found", sess
      )
      #
    methodNotAllowed:
      log.info("method not allowed")
      r = ep.makeErrorTuple(
        hc.Http405, "not implemented", "not implemented", "not implemented", sess
      )
  route(
    (
      if mtd == "HEAD":
        "GET" # fake a GET request, because a HEAD is just a GET without the body
      else:
        mtd
    ),
    origPath,
  )
  return r
