## Main entry point.

# Unqualified imports
import ./util/log
import ./util/resutils
import ./util/dbmanager

from std/exitprocs import setProgramResult
from std/typedthreads import createThread
from ./handle_req import handle

from std/os import nil
from std/posix import nil
from std/osproc import nil
from std/strutils import nil
from std/times import nil
from dotenv as env import nil
from forkygs/httpserver as http import nil
from forkygs/guildenserver as gs import nil
from forkygs/epolldispatcher as dispatch import nil
from ./config import nil
from ./util/tbcontext import nil
from ./util/db2object/sessions import nil

type AdditionalServiceTypes = enum
  ## Threads that I'm managing, we need to keep track of them
  SessionCleaner

var
  mySignalHandled = false ## Prevents recursive signal handling
  additionalServices: array[AdditionalServiceTypes, Opt[posix.Pthread]]
    ## pthread IDs of my managed threads

{.push raises: [].}

proc gsLog(level: gs.LogLevel, source: string, message: string) {.gcsafe.}
proc runServer(portNum: int, ipAddr: string, numThreads: uint): Failable
proc tbSessionCleaner(g: gs.GuildenServer): void {.gcsafe.}

proc main(): int =
  ## Imports the .env file, if any, sets up variables and whatnot.
  ## Handles the server's return code.

  # load .env file
  try:
    env.load()
  except Exception as e:
    log.warn("cannot use .env file", message = e.msg)

  # set port number
  let portNum =
    if (let envPortNum = os.getEnv(config.PortNumberEnvString); len(envPortNum) > 0):
      try:
        strutils.parseInt(envPortNum)
      except ValueError as e:
        log.warn("cannot set port number, using defaults", message = e.message())
        config.DefaultPortNumber
    else:
      log.warn("port number unset, using defaults")
      config.DefaultPortNumber

  if (let domain = strutils.strip(os.getEnv(config.DomainEnvString)); len(domain) < 1):
    log.error("DOMAIN environment variable not set!")
    return 1

  let numThreads =
    if (
      let envNumThreads = os.getEnv(config.NumThreadsEnvString)
      len(envNumThreads) > 0
    ):
      try:
        strutils.parseInt(envNumThreads).uint
      except ValueError as e:
        log.warn(
          "cannot set number of threads, using machine's CPU count",
          message = e.message(),
        )
        osproc.countProcessors().uint
    else:
      log.warn("number of threads unset, using machine's CPU count")
      osproc.countProcessors().uint

  let bindAddr =
    if (let envAddr = os.getEnv(config.ListenAddressEnvString); len(envAddr) > 0):
      envAddr
    else:
      log.warn("bind address unset, using localhost")
      "127.0.0.1"

  # inform main db conn string
  log.debug("main DB connection string", connstring = getPgConnString())

  # inform session db conn string
  let sqConnString = getSqConnString()
  log.debug("session DB connection string", connstring = sqConnString)

  # open session DB
  if sqConnString == ":memory:" or (not os.fileExists(sqConnString)):
    log.trace("reinit session db")
    if (let i = sessions.openSessionDb(true); not i.isOk()):
      log.error("error recreating session table", message = i.unwrapErr())
      return 1
    else:
      log.info("recreated session table")
  elif os.fileExists(sqConnString):
    log.trace("open session db")
    if (let i = sessions.openSessionDb(false); not i.isOk()):
      log.error("error opening session table", message = i.unwrapErr())
      return 1
    else:
      log.info("opened session table")

  # run the server
  if (let i = runServer(portNum, bindAddr, numThreads); not i.isOk()):
    log.error("abnormal exit", message = i.unwrapErr())
    return 1
  else:
    log.info("server stopped")
    return 0

proc runServer(portNum: int, ipAddr: string, numThreads: uint): Failable =
  ## The actual server setup. Runs the main web server powered by GuildenStern
  ## as well as additional services.

  # setup the server
  let server = http.newHttpServer(
    handle, # The core of the server--the request handler
    loglevel = gs.TRACE,
    headerfields = [
      "cookie", # session storage and such
      "content-type", # content negotiation
      "if-modified-since", # cache control
      "range", # download pausing and all that
      "host", # target domain
    ],
    contenttype = http.Streaming,
  )
  server.logcallback = gsLog
  server.internalThreadInitializationCallback = tbcontext.multipartInitProc
  server.sockettimeoutms = 1000

  let threadPoolSize = numThreads * 2

  log.info(
    "starting server with this amount of threads",
    threads = numThreads,
    poolsize = threadPoolSize,
  )

  # attempt to start the server
  try:
    # main web server
    if not dispatch.start(
      server = server, port = portNum, threadPoolSize = threadPoolSize, address = ipAddr
    ):
      return err("failed to start server")

    # additional services
    createThread(server.thread, tbSessionCleaner, server)
  except ResourceExhaustedError as e:
    return err("resource exhausted: " & e.msg)

  log.info(
    "server started",
    port = portNum,
    address = (if ipAddr == "": "0.0.0.0" else: ipAddr),
  )

  # wait until the server finishes
  server.thread.joinThreads()
  return ok()

proc setThreadName(
  t: posix.Pthread, name: cstring
): cint {.importc: "pthread_setname_np", header: "<pthread.h>".}

proc tbSessionCleaner(g: gs.GuildenServer): void =
  # The default suspend handler isn't used here since we don't even have
  # a pointer to a socket useable from here.
  log.info("automatic session cleaner started")

  var
    requiredSleep = posix.Timespec(
      tvSec: posix.Time(times.inSeconds(config.SessionCleanupInterval)), tvNsec: 0
    )
    remainderSleep: posix.Timespec

  # let globals know about this thread
  additionalServices[SessionCleaner] = Opt[posix.Pthread].some(posix.pthreadSelf())
  discard setThreadName(posix.pthreadSelf(), cstring("sess cleaner"))

  while true:
    # This will run a final time when the server is shut down
    #
    # I could implement a check for "interrupted" for
    # the nanoSleep block if I don't want this to happen
    if (let i = sessions.cleanSessions(Opt.none(string)); not i.isOk()):
      log.error("error cleaning up sessions", reason = i.unwrapErr())

    if (let i = sessions.cleanExpiredAcsrfTokens(); not i.isOk()):
      log.error("error cleaning up expired tokens", reason = i.unwrapErr())

    if gs.shuttingdown:
      break
    else:
      # This sleep is interruptible by signals, which allows this thread
      # to exit immediately in a clean way.
      discard posix.nanoSleep(requiredSleep, remainderSleep)

proc gsLog(level: gs.LogLevel, source: string, message: string) =
  ## Since GuildenStern supports a custom log callback, this callback
  ## basically redirects its messages to Chronicles.
  log.logScope:
    topics = "guildenstern"
    chroniclesLineNumbers = false
  case level
  of gs.LogLevel.TRACE:
    log.trace("", source, message)
  of gs.LogLevel.DEBUG:
    log.debug("", source, message)
  of gs.LogLevel.INFO:
    log.info("", source, message)
  of gs.LogLevel.NOTICE:
    log.notice("", source, message)
  of gs.LogLevel.WARN:
    log.warn("", source, message)
  of gs.LogLevel.ERROR:
    log.error("", source, message)
  of gs.LogLevel.FATAL:
    log.fatal("", source, message)
  of gs.LogLevel.NONE:
    discard

when isMainModule:
  # Custom signal handler. As there is also custom threads that cleans up stuff
  # like session IDs automatically, I need this to be able to instantly
  # terminate those threads without holding up the entire server.
  posix.onSignal(posix.SigTerm, posix.SigInt):
    # If this thing isn't here, the signal handler will be repeated
    # on the thread, causing an infinite loop. So I have to check if
    # we've already handled this signal.
    if not mySignalHandled:
      # The overridden signal handler consists of this function.
      # To terminate GuildenStern correctly, I have to call this
      # myself.
      gs.shutdown()
      log.trace("received signal to end server", signal = sig)

      # Prevent recursive signal handling calls
      mySignalHandled = true

      # Propagate the signal to every one of my threads
      for thread in additionalServices:
        if thread.isSome():
          discard posix.pthreadKill(thread.unwrap(), sig)

  # Actually run the server.
  main().setProgramResult()
