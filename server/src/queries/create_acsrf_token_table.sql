CREATE TABLE {config.AcsrfTableName}
(
    session_internal_id INTEGER NOT NULL,
    acsrf_token TEXT NOT NULL,
    expiry TEXT NOT NULL
)
