UPDATE
    {config.UserTableName}
SET
    password = $1
WHERE
    name = $2
