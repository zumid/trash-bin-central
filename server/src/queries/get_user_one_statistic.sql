WITH
a AS
(
    SELECT
        count(*) AS addon_count
    FROM
        {config.AddonTableName}
    WHERE
        associated_author_id = $1
),
b AS
(
    SELECT
        count(*) AS review_count
    FROM
        {config.ReviewTableName}
    WHERE
        associated_reviewer_id = $1
        AND
        parent_id IS NULL
)
SELECT
    a.addon_count,
    b.review_count
FROM
    a
JOIN
    b
ON
    TRUE
