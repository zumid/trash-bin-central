SELECT
	id,
	addon_id,
	reviewer,
	associated_reviewer_id,
	rating,
	{config.PostgresFormatTime("review_date")},
	content
FROM
	{config.ReviewTableName}
WHERE
	parent_id IS NULL
	AND
	addon_id = $1
ORDER BY
	id DESC
