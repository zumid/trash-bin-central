UPDATE
	{config.AddonTagAssocTableName}
SET
	tag_id = (
        SELECT
            id
        FROM
            {config.TagTableName}
        WHERE
            tag = $1
    )
WHERE
	addon_id = $2
	AND
	tag_id IN (
		SELECT
			id
		FROM
			{config.TagTableName}
		WHERE
			tag IN (
				{versionList}
			)
	)
