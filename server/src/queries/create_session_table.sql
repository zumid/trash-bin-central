CREATE TABLE {config.SessionTableName}
(
    internal_id INTEGER PRIMARY KEY,

    -- session ID
    id TEXT NOT NULL,

    -- null = not logged in
    user_id INTEGER NULL, -- tb_user.id

    -- null = never expire
    expiry TEXT NULL
)
