SELECT
  flash_message_id
FROM
  {config.FlashMessageTableName}
WHERE
  session_internal_id
IN
(
  SELECT
    internal_id
  FROM
    {config.SessionTableName}
  WHERE
    id = ?
)
