SELECT
  hash,           -- 0
  physical_file_name,      -- 1
  version_string, -- 2
  file_size,      -- 3
  {config.PostgresFormatTime("release_date")},   -- 4
  uploader_id,    -- 5
  flats,          -- 6
  texs,           -- 7
  sprites,        -- 8
  socs,           -- 9
  luas,           -- 10
  maps,           -- 11
  gfxs,           -- 12
  snds,           -- 13
  mscs,           -- 14
  readme,         -- 15
  virtual_file_name, --16
  file_type, -- 17
  release_notes, -- 18
  addon_id -- 19
FROM
  {config.AddonVersionTableName}
WHERE
  addon_id = $1
ORDER BY
  release_date DESC
LIMIT
  1
