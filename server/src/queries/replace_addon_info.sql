UPDATE
	{config.AddonTableName} 
SET
	name = $1,
	synopsis = $2,
	description = $3,
	author = $4,
	last_updated = now()
WHERE
	id = $5
