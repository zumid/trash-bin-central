UPDATE
    {config.SessionTableName}
SET
    user_id = NULL
WHERE
    id = ?
