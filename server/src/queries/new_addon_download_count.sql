INSERT INTO
{config.DownloadCounterTableName}
(
    addon_id,
    version_hash,
    ip_address
)
VALUES
(
    $1, -- addon_id
    $2, -- version_hash
    $3 -- ip_address
)
