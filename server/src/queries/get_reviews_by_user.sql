SELECT
	id,
	addon_id,
	reviewer,
	associated_reviewer_id,
	rating,
	{config.PostgresFormatTime("review_date")} as review_date,
	content
FROM
	{config.ReviewTableName}
WHERE
	associated_reviewer_id = $1
    AND
    parent_id IS NULL
