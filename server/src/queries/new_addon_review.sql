INSERT INTO
{config.ReviewTableName}
(
    addon_id,
    reviewer,
    rating,
    content,
    associated_reviewer_id
)
VALUES
(
    $1, -- addon_id
    $2, -- reviewer
    $3, -- rating
    $4, -- content
    {reviewerIdValue}
)
