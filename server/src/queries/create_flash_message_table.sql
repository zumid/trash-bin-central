CREATE TABLE {config.FlashMessageTableName}
(
    session_internal_id INTEGER NOT NULL,
    flash_message_id INTEGER NOT NULL
)
