INSERT INTO
{config.ReviewTableName}
(
    addon_id,
    parent_id,
    reviewer,
    content,
    associated_reviewer_id
)
VALUES
(
    $1, -- addon_id
    $2, -- parent_id
    $3, -- reviewer
    $4, -- content
    {reviewerIdValue}
)
