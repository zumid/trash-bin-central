INSERT INTO
{config.SessionTableName}
(
    id,
    user_id,
    expiry
)
VALUES
(
    ?,
    {userIdStr},
    DATETIME('now', '+5 minutes')
)
