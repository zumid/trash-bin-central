SELECT
	id,
	addon_id,
	reviewer,
	associated_reviewer_id,
	rating,
	{config.PostgresFormatTime("review_date")} as review_date,
	content
FROM
	{config.ReviewTableName}
WHERE
	parent_id IS NULL
ORDER BY
	review_date DESC
LIMIT
    {limit}
