-- memory concerns over TCP roundtrip concerns...
INSERT INTO {config.AddonLevelTableName}
(
    addon_id,
    level_number,
    level_name,
    act
)
VALUES
(
    $1, -- addon_id
    $2, -- level_number
    $3, -- level_name
    $4 -- act
)
