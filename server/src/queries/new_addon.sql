INSERT INTO
{config.AddonTableName}
(
    name,
    description,
    author,
    associated_author_id,   
    synopsis,
    edit_code
)
VALUES
(
    $1,
    {descriptionSanitized},
    $2,
    {assocAuthorIdSanitized},
    $3,
    {md5CommandSanitized}
)
RETURNING
    id,
    {config.PostgresFormatTime("upload_date")} AS upload_date,
    {config.PostgresFormatTime("last_updated")} AS last_updated
