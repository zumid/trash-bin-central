SELECT
    id AS tag_id
FROM {config.TagTableName} tt
WHERE
    tt.tag = $1
LIMIT 1
