SELECT
  tl.level_name
    || ' ('
    || count(act)
    || ' act'
    || case
      when count(act) > 1
      then 's'
      else ''
      end
    || ')'
    as level_name
FROM
  {config.AddonLevelTableName} tl
WHERE
  tl.addon_id = $1
GROUP BY
  tl.level_name
