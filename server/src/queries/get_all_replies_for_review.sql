SELECT
	id,
	reviewer,
	associated_reviewer_id,
	{config.PostgresFormatTime("review_date")},
	content
FROM
	{config.ReviewTableName}
WHERE
	parent_id = $1
ORDER BY
	id ASC
