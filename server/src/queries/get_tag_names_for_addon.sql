WITH found_tags AS
(
    SELECT DISTINCT
        *
    FROM
        {config.AddonTagAssocTableName} tat
    WHERE
        tat.addon_id = $1
    ORDER BY
        tat.addon_id ASC
)
SELECT
    tt.tag
FROM
    {config.TagTableName} tt
WHERE
    tt.id IN
    (
        SELECT
            tag_id
        FROM
            found_tags
    )
