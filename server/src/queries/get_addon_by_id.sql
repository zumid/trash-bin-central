SELECT
    id,
    name,
    synopsis,
    description,
    author,
    associated_author_id,
    {config.PostgresFormatTime("upload_date")} as upload_date,
    {config.PostgresFormatTime("last_updated")} as last_updated
FROM
    {config.AddonTableName}
WHERE
    id = $1
