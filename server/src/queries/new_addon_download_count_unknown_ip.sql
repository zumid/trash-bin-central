INSERT INTO
{config.DownloadCounterTableName}
(
    addon_id,
    version_hash
)
VALUES
(
    $1, -- addon_id
    $2, -- version_hash
)
