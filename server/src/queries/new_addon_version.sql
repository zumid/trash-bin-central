INSERT INTO
{config.AddonVersionTableName}
(
  hash,
  addon_id,
  physical_file_name,
  version_string,
  file_size,
--
  flats,
  texs,
  sprites,
  socs,
  luas,
  maps,
  gfxs,
  snds,
  mscs,
  readme,
  virtual_file_name,
  file_type,
  release_notes
)
VALUES
(
  $1, -- hash,
  $2, -- addon_id,
  $3, -- physical_file_name,
  $4, -- version_string,
  $5, -- file_size,
--
  $6, -- flats,
  $7, -- texs,
  $8, -- sprites,
  $9, -- socs,
  $10, -- luas,
  $11, -- maps,
  $12, -- gfxs,
  $13, -- snds,
  $14, -- mscs,
  $15, -- readme
  $16, -- virtual_file_name
  $17,  -- file_type
  $18 -- release_notes
)
