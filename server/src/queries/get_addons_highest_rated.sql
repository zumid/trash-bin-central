SELECT
    id,
    name,
    synopsis,
    description,
    author,
    associated_author_id,
    {config.PostgresFormatTime("upload_date")} as upload_date,
    {config.PostgresFormatTime("last_updated")} as last_updated
FROM
	{config.AddonTableName} ta
WHERE
	ta.id IN
    (
        SELECT
            addon_id
        FROM
            (
                SELECT
                    tr.addon_id,
                    avg(tr.rating) AS rating_avg
                FROM
                    {config.ReviewTableName} tr
                WHERE
                    tr.rating IS NOT NULL
                GROUP BY
                    tr.addon_id
                ORDER BY
                    rating_avg DESC
                LIMIT
                    $1
            )
    )
-- ORDER BY
-- 	ta.last_updated DESC
