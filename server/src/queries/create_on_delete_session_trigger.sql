CREATE TRIGGER
    tb_delete_session_trigger
BEFORE DELETE ON
    {config.SessionTableName}
FOR EACH ROW
BEGIN
    DELETE FROM
        {config.AcsrfTableName}
    WHERE
        session_internal_id = OLD.internal_id;
    DELETE FROM
        {config.FlashMessageTableName}
    WHERE
        session_internal_id = OLD.internal_id;
END
