SELECT
    1
FROM
    {config.AcsrfTableName}
WHERE
    acsrf_token = ?
    AND
    CURRENT_TIMESTAMP <= expiry
AND
    session_internal_id = ?
