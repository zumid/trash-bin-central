INSERT INTO
{config.AddonTagAssocTableName}
(
    addon_id,
    tag_id
)
VALUES
(
    $1,
    $2
)
RETURNING
    addon_id
