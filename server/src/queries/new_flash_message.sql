INSERT INTO {config.FlashMessageTableName}
(
  session_internal_id,
  flash_message_id
)
VALUES
(
  ?,
  ?
)
