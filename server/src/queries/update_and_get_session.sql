UPDATE
    {config.SessionTableName}
SET
    expiry = DATETIME('now', '+5 minutes')
WHERE
    id = ?
    AND
    CURRENT_TIMESTAMP <= expiry
RETURNING
    id,
    user_id,
    {config.SqliteFormatTime("expiry")} AS expiry
