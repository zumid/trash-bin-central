INSERT INTO {config.AcsrfTableName}
(
    acsrf_token,
    session_internal_id,
    expiry
)
VALUES
(
    ?,
    ?, 
    DATETIME('now', '+5 minutes')
)
