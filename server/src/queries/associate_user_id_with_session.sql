UPDATE
    {config.SessionTableName}
SET
    user_id = ?
WHERE
    id = ?
