SELECT
    CASE WHEN count(rating) < 1 
    THEN
        '-'
    ELSE
        to_char(avg(rating), '9.9') ||
        ' (' ||
        count(rating)::text ||
        ')'
    END
FROM
    {config.ReviewTableName}
WHERE
    rating IS NOT NULL
    AND
    addon_id = $1