INSERT INTO
{config.UserTableName}
(
    name,
    password,
    pfp_hash
)
VALUES
(
    $1,
    $2,
    0
)
RETURNING
    id
