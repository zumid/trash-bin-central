SELECT
    id,
    name,
    pfp_hash,
    {config.PostgresFormatTime("join_date")} AS join_date,
    {config.PostgresFormatTime("last_seen")} AS last_seen
FROM
    {config.UserTableName}
WHERE
    name = $1
