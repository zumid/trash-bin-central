UPDATE
    {config.UserTableName}
SET
    last_seen = NOW()
WHERE
    name = $1
