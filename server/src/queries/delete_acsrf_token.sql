DELETE
FROM
    {config.AcsrfTableName}
WHERE
    acsrf_token = ?
AND
    session_internal_id = ?
