SELECT
    lv.addon_id ||
    '-' ||
    lv.level_number
FROM
    {config.AddonLevelTableName} lv
WHERE
    lv.addon_id = $1
ORDER BY
    random()
LIMIT
    1
