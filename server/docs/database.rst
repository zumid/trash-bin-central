===============
Database design
===============

.. role:: sql(code)
  :language: sql

``generated as identity`` is Postgres, ANSI and Oracle syntax, it is practically equivalent to Postgres's ``serial`` and MySQL's ``auto_increment``.

1. Yes ``enum`` exists, but do I want to change enums in two places (the db and the app) instead of one (the app)?
2. Denormalized is better for perf, and yet increases the risk of incorrect state. Decisions, decisions.
3. "Everything is an integer", a.k.a the C way.

User
----

.. code:: sql

  create table tb_user (
    id integer generated always as identity,
    name varchar(24) unique not null
  );

Image
-----

Can be used for user avatars and mod images.

.. code:: sql

  create table image (
    hash bigint primary key,
    -- 0: png
    -- 1: jpeg
    -- 2: bmp
    type integer
  );

Addon
-----

.. code:: sql

  create table addon (
    id integer generated always as identity,
    name varchar(128) unique not null,
    description text,
    --
    upload_date timestamp default now(),
    last_updated timestamp default now(),
    --
    author_id integer,
    --
    image_hash bigint
  );

  alter table addon
  add constraint fk_addon_author_id
  foreign key (author_id) references tb_user(id);

  alter table addon
  add constraint fk_addon_image_hash
  foreign key (image_hash) references image(hash);

Addon version
-------------

.. code:: sql

  create table addon_version (
    hash integer primary key,
    addon_id integer,
    --
    file_name varchar(128) unique not null,
    version_string varchar(128) not null,
    --
    file_size integer,
    --
    release_date timestamp default now(),
    --
    uploader_id integer
  );

  alter table addon_version
  add constraint fk_addon_version_addon_id
  foreign key (addon_id) references addon(id);

  alter table addon_version
  add constraint fk_addon_version_uploader_id
  foreign key (uploader_id) references tb_user(id);

Addon version stats
-------------------

.. code:: sql
  
  create table addon_version_statistics (
    hash integer,
    lua_file_count integer,
    character_count integer,
    map_count integer,
    song_count integer
  );

  alter table addon_version_statistics
  add constraint fk_addon_version_statistics_hash
  foreign key (hash) references addon_version(hash);
