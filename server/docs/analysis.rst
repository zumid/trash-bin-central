================
WAD/PK3 Analysis
================

.. contents::

The common extracted structure for an add-on is:

.. code:: nim

  type Srb2Directory* = object
    flatFiles*: seq[string]
    texFiles*: seq[string]
    spriteFiles*: seq[string]
    socFiles*: seq[string]
    luaFiles*: seq[string]
    mapFiles*: seq[string]
    gfxFiles*: seq[string]
    sndFiles*: seq[string]
    mscFiles*: seq[string]
    readme*: string

When uploading a mod to the MB, for sake of organization you are required to name your mod name in a specific way.

Prefix, underscore, name, underscore, version, dot pk3 or wad.

Prefixes are one or more letters informing what the mod consists of:

====== ===========
Prefix Explanation
====== ===========
**S**  Single player
**R**  Racing
**M**  Match
**F**  Capture the flag
**C**  Character
**D**  Dr. Robotnik's Ring Racers
**L**  Has custom Lua
**P**  Persona
**B**  Battle
====== ===========

You had to specify this manually, and the judges will "verify" this.

Purpose-built software allows us to do better:

- At least the L prefix can be definitively determined
  through analysis.
- Further analysis, e.g., of the SOC scripts, may automatically
  determine S, M, F, C, P, B prefixes.
- Mods usually can't be used across games, so a game selection
  should imply either R or D.

In fact, we should be free from the prefix system entirely,
and let software do all the work.

WAD structure
-------------

It's essentially a flat list of files.

Possible magic numbers are:

- ``IWAD``
- ``PWAD``
- ``SDLL``

`Wadzip <http://alam.srb2.org/wadzip/src-v2.0.6>`_ has the ``ZWAD`` magic number.

.. code:: nim

  ## converts a WAD to a WAD directory
  func toWadDir*(w: Wad): Srb2Directory =
    var
      in_flat = false
      in_tx = false
      in_sprite = false

    for key in w.file_names:
      let k = key.toLowerAscii()

      if k in ["maincfg", "objctcfg"] or k.startsWith("soc_"):
        result.socFiles.add(key)
        continue

      if k.startsWith("lua_"):
        result.luaFiles.add(key)
        continue

      if k.startsWith("map"):
        result.mapFiles.add(key)
        continue

      if k.startsWith("ds"): # digital sound
        result.sndFiles.add(key)
        continue

      if k.startsWith("d_") or k.startsWith("o_"): # music
        result.mscFiles.add(key)
        continue

      if in_flat:
        result.flatFiles.add(key)
        continue

      if in_tx:
        result.texFiles.add(key)
        continue

      if in_sprite:
        result.spriteFiles.add(key)
        continue

      case k
      of "f_start", "ff_start":
        in_flat = true
      of "f_end", "ff_end":
        in_flat = false
      of "s_start", "ss_start":
        in_sprite = true
      of "s_end", "ss_end":
        in_sprite = false
      of "tx_start":
        in_tx = true
      of "tx_end":
        in_tx = false
      else:
        continue

    result.readme = (
      if w.lumps.hasKey("CREDITS"):
        cast[string](w.getLump("CREDITS").get())
      elif w.lumps.hasKey("README"):
        cast[string](w.getLump("README").get())
      else:
        ""
    )

PK3 structure
-------------

.. code:: nim

  proc pk3FromFile*(s: string): Srb2Directory =
    let dir2fields = {
      # required
      "flats/": result.flatFiles.addr,
      "textures/": result.texFiles.addr,
      "sprites/": result.spriteFiles.addr,
      "soc/": result.socFiles.addr,
      "lua/": result.luaFiles.addr,
      "maps/": result.mapFiles.addr,

      # optional
      "graphics/": result.gfxFiles.addr,
      "sounds/": result.sndFiles.addr,
      "music/": result.mscFiles.addr,
    }.toTable

    let zipReader = s.openZipArchive()

    for fileName in zipReader.walkFiles:
      case fileName.toLower()
      of "init.lua":
        result.luaFiles.add(fileName)
        continue
      of "readme", "readme.txt", "readme.md":
        result.readme = zipReader.extractFile(fileName)
        continue
      else:
        discard

      for key in dir2fields.keys:
        if fileName.toLowerAscii().startsWith(key):
          dir2fields[key][].add(fileName[key.len ..^ 1])
          break

Things to auto-detect
---------------------

Detection results will be reflected in the file name whenever
possible, but will mainly be added to tags.

One implementation of this auto-detection method is in
`the SRB2 auto-renamer tool <https://tails-team.github.io/srb2namer>`_.

SOC files are parsed in a completely case-insensitive (by normalizing to
entirely uppercase) and ad-hoc way by SRB2. The auto-detection rules here
describe **keyword-based** detection for TypeOfLevel etc. for 2.1 and later.
2.0 and earlier use a **flag-based** scheme, with numbers determining what
types of level it is. This may be supported in the future.

    since the code for soc is just a bunch of fastcompares in a long ass if else. the games code makes it upper case automatically for the internal code so theres no issues with having mixed case in the soc files itself

    — NepDisk

SRB2
~~~~

====== ===========
Prefix Explanation
====== ===========
**S**  Single player, Co-op, competition
**R**  Race-only, particularly circuit
**M**  Match, Tag, Hide and Seek
**F**  Capture the flag
**C**  Added characters
**L**  Has custom Lua
**P**  Persona
====== ===========

How to detect:

* **S** - In MAINCFG: In *any* Level: TypeOfLevel contains "Singleplayer", "Co-op", "Coop", "Solo", "SP", "Single" or "Competition"
* **R** - In MAINCFG: In *any* Level: TypeOfLevel contains "Race"
* **M** - In MAINCFG: In *any* Level: TypeOfLevel contains "Match"
* **F** - In MAINCFG: In *any* Level: TypeOfLevel contains "CTF"
* **C** - [In MAINCFG: At least one Character exists] or [Either a "P_SKIN" or "S_SKIN" file exists]
* **L** - At least one Lua file exists
* **P** - ?

SRB2K
~~~~~

====== ===========
Prefix Explanation
====== ===========
**K**  SRB2 Kart
**R**  Race-only, particularly circuit
**B**  Battle
**S**  Single player, Co-op, competition
**C**  Added characters
**L**  Has custom Lua
====== ===========

* **K** - Inferred from game selection
* **R** - In MAINCFG: In *any* Level: TypeOfLevel contains "Race"
* **B** - In MAINCFG: In *any* Level: TypeOfLevel contains "Battle"
* **S** - In MAINCFG: In *any* Level: TypeOfLevel contains "Singleplayer", "Co-op", "Coop", "Solo", "SP", "Single" or "Competition"
* **C** - [In MAINCFG: At least one Character exists] or [Either a "P_SKIN" or "S_SKIN" file exists]
* **L** - At least one Lua file exists

DRRR
~~~~

====== ===========
Prefix Explanation
====== ===========
**D**  Dr. Robotnik's Ring Racers
**R**  Race
**B**  Battle
**S**  Single player, Special Stage, Boss battles
**T**  Tutorial level
**C**  Added characters
**F**  Follower
**L**  Has custom Lua
====== ===========

* **D** - Inferred from game selection
* **R** - In MAINCFG: In *any* Level: TypeOfLevel contains "Race"
* **B** - In MAINCFG: In *any* Level: TypeOfLevel contains "Battle"
* **S** - In MAINCFG: In *any* Level: TypeOfLevel contains "Singleplayer", "Co-op", "Coop", "Solo", "SP", "Single" or "Competition"
* **T** - In MAINCFG: In *any* Level: TypeOfLevel contains "Tutorial"
* **C** - [In MAINCFG: At least one Character exists] or [Either a "P_SKIN" or "S_SKIN" file exists]
* **F** - In MAINCFG: At least one Follower exists
* **L** - At least one Lua file exists
