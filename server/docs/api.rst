========
HTML API
========

.. contents::

Addons
======

Search
------

.. code::
  /addons

- Left side: Search parameters
  - Form
    - The options should be prefilled with the equivalent state
      reflected in the URL.
  - Page list
    - Should be a list of links. The links will automatically
      append the currently saved options.
- Right side: The actual mods list.
    - The limit might be user configurable, but it should be
      I think 30.

The search form is a GET parameter.

Here is the list of GET parameters, \* marks the default value.

===== ================== =
Field Type               Content
===== ================== =
q     string             Text query

for   string enum:       Which aspect of the addon ``q`` refers to
        - ``title``\*
        - ``author``
        - ``tags``

s     string enum:       How the search should be sorted
        - ``upload``
        - ``release``\* 
        - ``name``
        - ``stars``
        - ``downloads``
      or empty

a     bool               Whether or not the search is sorted by
                         ascending-order.

                         **Example**: for dates, if true(ish), they
                         will be sorted by oldest-first, not newest-first.

game  string enum:       Filter by game.
        - ``srb2-2.2``\*
        - ``srb2-2.1``
        - ``srb2k-1``
        - ``drrr-2``
      or empty

hl    bool               If true, filter by addons with at least 1 Lua script.

hc    bool               If true, filter by addons with at least 1 custom character.

hm    bool               If true, filter by addons with at least 1 custom map.

hs    bool               If true, filter by addons with at least 1 new song.

after integer            Used for paging. Show addons after a specific ID.
===== ================== =


Info page
---------

.. code::
  /addons/{id}

Could be styled after the stuff in Romhacking.com.

- Left side
  - Description: self explanatory
  - Gallery: if applicable
  - Downloads list:
    - Version
    - Release date
    - File size
    - Download
  - Statistics of latest version:
    - Lua
    - Maps
    - Characters
    - Songs
    - Other stuff
- Right side: A simple info box
    - Original release date
    - Last updated
    - Stars
    - Downloads
    - File format


User
====

Info page
---------

.. code::
  /user/{id}

- Left side: user infobox
  - Avatar and name
  - Register date
- Right side: user statistics
  - Number and list of authored mods

Login
-----

.. code::
  /login

Logout
------

.. code::
  /logout

Sign up
-------

.. code::
  /signup

Front page
==========

.. code::
  /

Should contain:

- Latest news, 4 latest suffices
- Latest addons: Last 4 addons by newest release date
- Highest rated addons: Last 4 addons by stars for the past 7 days
