=============
Feature ideas
=============

.. contents::

Anon interaction
================

The ability to upload and comment anonymously.

Details
-------

1. **Complement, not replace**

   Anon interaction might not *replace* user-based interaction, but rather co-exist
   with it.

   As in `HCS64 <https://vgm.hcs64.com/>`_, the one benefit an account might give
   you is that you don't have to manually enter an author name whenever uploading
   an addon.

2. **Upload edit codes**
   
   Upon uploading an addon, an **edit code** is generated for this addon. This edit
   code will be displayed as a flash message for the current session, and then
   never seen again. In the database, this edit code will be stored **hashed**, so
   not even the admin could know what it even was.

   .. note::

      At the moment, the session tables are implemented in SQLite. The flash
      messages are part of this table. Thanks to `flexible typing <https://www.sqlite.org/flextypegood.html>`_
      it is possible to store custom flash messages (as a string) alongside the basic
      integers that map to an enum constant.
   
   When someone wants to edit addon metadata or to upload a new version of said addon,
   this edit code has to be provided, or else nothing would be done.

   This is similar to what is done in sites like `Rentry <https://rentry.co>`_.

   .. note::

      The edit codes thing don't apply when the addon is inserted by a registered
      user, in which case only the user which has uploaded it may edit the addon.

Considerations
--------------

1. **Verification**

   How exactly do we expect to verify users so that it isn't too easy for them to
   do funny ops? The traditional answer to this has been a **captcha**.
   reCaptcha, hCaptcha sounds tempting, but uh hey isn't this training data or
   something? There's also simpler kinds of captcha, and of course the image captcha.

   What to choose, what to choose.

   Also, DDoS isn't being considered here, as it's a problem regardless.

Queue
=====

After an addon upload, there is a queue to process it.

Details
-------

1. **Separate upload and processing**
   
   At the moment, processing is done on the uploaded file as soon as the file finishes
   uploading, using the exact same file handle. This
   is fine with the current implementation of file upload, of which only the ``file``
   one exists—that is, save directly to a file within the server. That's fine and
   all but what if I need another way, like sending it to some S3 or Minio server
   for hosting?

.. note::
   continue this

Considerations
--------------

.. note::
   continue this
