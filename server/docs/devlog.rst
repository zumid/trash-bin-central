======
Devlog
======

.. contents::

.. importdoc:: util/dbmanager, util/datatypes, util/resutils

2024-12-30
----------

.. note::
   * Implement HTTP partial downloads

In Firefox, at least: when unpausing a download, if it receives as the response
code anything other than an HTTP 206, it will call it quits. However for
correctness I should also implement range parsing.


2024-12-24
----------

It's kinda important that I can pause a download, but I don't think this has the
capability right now. The way to do it is to implement
`range requests <https://developer.mozilla.org/en-US/docs/Web/HTTP/Range_requests>`_

Since normal assets are small, I don't think there's any need for it. Downloads
on the other hand can be pretty big. With a crappy connection, one would be
incentivized to use download managers to speed it up by hammering the website
8 connections at a time. (just ask me…)

2024-12-20
----------

.. note::
   * Add release notes into the add-on versions type
   * Fuzzy time in add-on blurbs
   * Add ``TBCTHUMB`` add-on preview override
   * Remove the "Add-on of" placeholder in the add-on infobox, as that is implied
     by the tags attached to the add-on.
   * Add random add-on blurb featured on the home page.

Before I can do all the other stuff related to add-ons, I must fix the add-on
upload form. Specifically, I added an input to add a changelog, because there
will be a new page that just... lists the (hopefully) human-readable change log.
Subsequently, this will need a new column in the ``tb_addon_version`` table,
``release_notes``.

So I wanted ``release_notes`` to be a string NULLable, but a quirk of using
type-safe wrappers over something that may actually allow NULL is that I 
can't even use NULL even if I wanted to. And if I tried inserting ``NULL``,
what I'd get is a literal string "NULL". What if I tried doing it the other
way around? Well, I would need to replace the prepared statement thingamajig
with a spoooooooky string format. And we wouldn't want that do we?

Fortunately, the fetch output of a null value is always an empty string, so
even if ``''`` and ``NULL`` have completely different representations, I can
check them the same way. There already exists a `stringFromNullable`_ function
that does just that. It resolves an empty string into a ``none()``.

So I wanted a ``randomAddon``to be displayed in the index page, but the way
it does that is by importing ``addon_blurb``, which assumes a variable named
``addon`` would be present. I don't really want to confuse myself with
multiple ``addon`` variables, rather would keep them in their own scopes.
Is there a way to start and end Nim blocks in Nimja?

2024-12-17
----------

.. note::
   * Complete mockup
   * Begin integrating new mockup with backend

I think I might know why people separate front-end and back-end: to allow the
two to work in parallel. But the truth is still that the two need each other,
and require coordination. The thing is, also, that this separation would allow
both ends to be ridiculously over-engineered. One side's drowning in ``node_modules``
and the other is drowning in endless ``FactoryFactoryFactory``.

There's this sinking feeling that "mockup"/"front-end" work isn't "real work"
as far as this project is concerned, but we all have learned that as long as
it looks good and *artistic* people will forgive you for terrible design
decisions \*COUGH\*nu-STJR\*COUGH\*

Tested the whole thing with JMeter, too. I seem to be running into a theme where
when there's too many requests GuildenSterm has to process, one of them is eventually
gonna trip and then stall the entire thing for a minute or two, and then *actually*
SIGSEGV out. Stack trace says it's freaking out while trying to read from some
socket as to what to respond. Butttt I dun wannaaaaaa (make my own server)!!!

2024-12-02
----------

.. note::
   * Thread-based connection pooling (I think)

A lot of stuff happened in the meantime that I forgot to write here, so I'll just
write up about today.

Attempting to make a database connection pool took three tries:

1. Having this be a `Res[Database]` (`Res`_), since `connectToPgDb()`_ returned a `Res[Database]`.
   The Database object has a `=destroy` hook that disconnects the database
   session. Using patty for pattern matching didn't work. The unwrapped object
   was a move, and the move resulted in the hook being called, disconnecting the
   session before it can be used.

2. Using `Database`_, and unwrapping `connectToPgDb()`_ works... until requests are
   actually made. Then for whatever reason the `=destroy hooks` are called for
   threads that made the request, and so connections drop one by one.

3. Disabling `=copy` on `Database`_. Ensures that the move operation isn't done,
   thus not calling `=destroy` on an old location whenever a move is done.
   I just wanted a way to let Nim know that one of the fields are still used...
   `=sink` didn't do what I wanted :(
   To make everything work, I took the address of whatever `getCurrentDbConnection()`_
   returned and dereferenced it for every database function call. It's so hacky...

...so I just turned it into refs instead :/ No need to dereference anything
manually, then.

2024-10-12
----------

.. note::
   * Helper thread for cleaning up sessions in a timely manner
   * A custom cookie parser
   * Basic HTTP sessions support via database
   * Constraints on the DB layout

When implementing a session system, I should have a thread running which
does automatic cleanup of invalid sessions every so often. Invalid threads
here means "past its due-by time".

I'm the one instantiating this thread, so it is not managed by GuildenStern.
But I seem to be running into a problem.

The thread should wait a bit until the next query, but should exit along
with the other threads when the user requests an exit. The options I've tried:

1. Use ``os.sleep``. I soon figured out that the thread will just
   ignore SIGTERM from the main process and keep going, until it has
   to be ``kill'd`` manually. Not until I was aware of
   ``guildenstern.shuttingdown``, which I must check on every iteration,
   and then exit if set. This is because the SIGTERM and SIGINT handlers have
   been overridden to set this variable instead to allow threads to
   exit cleanly.
2. Use ``guildenstern.suspend``. This is what's recommended to me.
   That proc tries to access this threads' ``socketcontext``, which doesn't
   exist because GuildenStern isn't the one managing this thread.
   So it crashes with a SIGSEGV. I tried creating a new ``socketcontext``,
   but what would be the point, as the proper suspend handler is set upon starting
   the main HTTP server?
3. Use ``posix.nanosleep``. This is apparently an interruptible sleep.
   But it has no effect here, and sleeps complete anyway. Signals from
   the main thread seems to not get passed here, so as a result will always
   wait until it's done sleeping, even if the thread is to exit on
   ``guildenstern.shuttingdown``.

Given the third option is an *interruptible sleep*, and signals must be
propagated manually to my threads, I set up a custom signal handler.

The signal handler installed by GuildenStern calls ``guildenstern.shutdown()``, which,
aside from setting ``guildenstern.shuttingdown`` also tries to trigger the
dispatcher to exit cleanly. I do likewise. The only thing added is signal propagation
to my extra threads which allows ``posix.nanosleep`` to interrupt its sleep as
it should.

2024-10-11
----------

.. note::
   * Switch database engines at compile-time (``-d:useDbEngine=Postgres|Sqlite``)
   * Temporarily turn off compilation of the mod upload code, because it was taking
     up wayyyyyy too much time and I want to work on something else at the moment.
   * Continue with setting up DB stuff
   * A registration page

So now I want to implement a user registration system. That mod uploading thing
was inexplicably making the build times take forever, because it's a nested
iterator and it produces 20 MB of C code containing god knows what. I have
temporarily disabled the code to make iterating on this a bit easier.

The four things I needed to prepare:

1. The controller code in ``controller/``.
2. The page that it will render in ``html_templates/``.
3. The call to the controller code in 1, at ``handle_req.nim``.
4. The renderer code that calls out to 2, at ``render.nim``.

The ``tb_user`` table already exists, this form will just fill it out.

But the problem with forms like this is that
`CSRF <https://owasp.org/www-community/attacks/csrf>`_ is a thing. The
common way around it is generating a sort of token that is unique for each
time the form is brought up, and for each user even. How this will work,
roughly:

1. Server generates a token, throws it into some shared memory or database
   to be recalled later.
2. This token is to be valid for only a few minutes before it can't be used.
3. The token is associated with a unique user session.
4. Said token is thrown into the form as a hidden value, in the hopes that
   the same value will be returned to the server within a certain time frame.

Since ACSRF tokens are associated with sessions, I might as well implement
a session system anyway. My sessions would consist of:

1. randomized user session ID
2. user ID (NULL if logged out)
3. expiry timestamp (will delete if timestamp is on or after)

And thus the tokens will be:

1. which session ID
2. ACSRF token
3. expiry timestamp
