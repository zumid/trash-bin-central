==========
Code style
==========

Imports
-------

1. Imports, from most-preferred to least-preferred (and ordered in reverse):
    a. **Qualified imports**, e.g. ``from something as sm import nil``
    b. **Partially-qualified imports**, e.g. ``from something as sm import `/```
    c. **Unqualified imports**, e.g. ``import something``
2. Unqualified, partially-qualified, and qualified imports are one "section" each, separated by one blank line.
3. For prototyping, unqualified imports are acceptable.
4. Each section should be ordered, from top to bottom:
    a. **Stdlib imports**, e.g. ``std/something``
    b. **Dependency imports**, e.g. ``print``
    c. **Local imports**, e.g. ``./config``, ``../utils/testing``
5. Qualified imports may be **graduated** to partially-qualified imports, if such qualifications make the code too "noisy", or if they are only used for a couple of functions.

Commonly-used unqualified imports in this project include:

1. ``std/strutils`` - a wrapper around ``nim-results`` providing some additional functions.
2. ``util/resutils`` - a wrapper around ``nim-results`` providing some additional functions.
3. ``util/log`` - a wrapper around ``chronicles`` providing a custom log format. It is imported unqualified due to the way it uses macros and templates such that when done otherwise the compiler will complain about undefined symbols.
4. ``util/dbmanager`` - some common tools to deal with databases both with Postgres and SQLite, provides the names ``psql`` and ``sqlite`` to work with each.
5. ``util/datatypes`` - self-explanatory

.. note::

  Unqualified imports in Nim are `idiomatic and very much unlike Python's "star"-import <https://narimiran.github.io/2019/07/01/nim-import.html>`_ — however,
  it may cause the origins of a function, template or other to be unclear unless you use an IDE to help.

  On the other hand, as explained in the linked article, qualified imports like in Python, Go, or indeed any other language
  increases code "noise" and makes algorithms a little less readable. When applied
  to Nim, it basically negates UFCS. UFCS, of course, being what makes Nim (and D)
  stand out.

  But "explicit is better than implicit," so people say. :P


Includes
--------

Should be avoided.


Formatting
----------

`nph <https://github.com/arnetheduck/nph>`_ is the only real game in town, so you
may as well just use that.
