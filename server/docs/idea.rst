====
Idea
====

.. contents::

Principles
----------

1. Simple and plain, built-for-purpose addon sharing site.

2. No forums needed, attempts at integration not only messy but also
   demonstrably harmful.

3. No messy categorization system mandated by XenForo software,
   no strict file naming rules, no bullshit judges. Because we have
   a purpose-built software, we can `analyze WAD/PK3 files <analysis.html>`_, because
   the game assumes a certain structure and mods will not work without
   said structure.

4. ``Open Assets`` software: AGPL v3.

Excuses as to why it's taking so long
-------------------------------------

1. I'm doing this in Nim—a language which barely has an ecosystem—in the spirit of experimentation.

2. Add-ons can be huge. What regular HTTP servers do is that they
   load the entire file into memory before they process the file,
   which is just a Funny Op™ waiting to happen since someone can
   just load a 13GB file into the server and make it crash.
   This can be solved just by putting Nginx in front of the whole
   thing, but it's not very pretty.

3. There is a framework which properly solves the problem above,
   but not everything I like about it, which is why I'm "vendoring"
   a part of the thing.

4. Said framework is also, unfortunately, extremely barebones.
   So, things like: routing, safe handling of strings, etc. All of
   those need to be done *manually*.

5. UX customization takes a long-ass time. It may be a long while
   yet before this comes to fruition, but I may as well make
   something that sucks but ultimately works.

6. I currently have a full-time job as of Sept '24. Even then I still somehow rope myself into a billion other things. Nuff said.

What I need help with
---------------------

What do **you** need? What would **you** want?

How should everything be organized?
