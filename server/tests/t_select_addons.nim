import unittest2
from dotenv as env import nil
import patty
import src/util/datatypes
import src/util/resopt
from src/util/db2object/addons import nil
from src/util/dbmanager import nil

const whichId = 5

suite "Select addons":
  echo "Will not check anything other than \"did it error or not?\""

  setup:
    env.load()
    let s = dbmanager.connectToPgDb().unwrap()

  teardown:
    dbmanager.justCloseTheDb(s)

  test "Select addon versions":
    let addonQuery = Addon(id: Opt[int].some(whichId))
    match addons.getAddonVersions(s, addonQuery):
      Ok(v):
        for i in v:
          echo $i
        check true
      Err(e):
        echo e
        check false

  test "Select levels from addon":
    let addonQuery = Addon(id: Opt[int].some(whichId))
    match addons.getLevelsFromAddonLatest(s, addonQuery):
      Ok(v):
        for i in v:
          echo $i
        check true
      Err(e):
        echo e
        check false

  test "Select all addons":
    match addons.getAllAddons(s):
      Ok(v):
        for i in v:
          echo $i
        check true
      Err(e):
        echo e
        check false

  test "Select addon by ID":
    match addons.getAddonById(s, whichId):
      Ok(v):
        echo $v
        check true
      Err(e):
        echo e
        check false

  test "Select addon by name":
    match addons.simpleAddonSearch(s, "emerald"):
      Ok(v):
        echo $v
        check true
      Err(e):
        echo e
        check false
