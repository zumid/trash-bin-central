import unittest2
import src/util/parse/bbcode

suite "BBcode parsing":
  test "Basic tags parsing":
    check:
      bbCode2Html("[b]Test[/b] test") == "<b>Test</b> test"

  test "Safe escape":
    check:
      bbCode2Html("<b>WELCOM 2 MY PAGE</b>") == "&lt;b&gt;WELCOM 2 MY PAGE&lt;/b&gt;"

  test "More to come":
    skip()
