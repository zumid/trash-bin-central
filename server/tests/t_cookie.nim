import unittest2
import src/util/parse/cookies

suite "Basic cookie parsing":
  test "parse one cookie":
    let p = parse("sessionId=abc123")
    check:
      p.isOk()
      p.unwrap().len == 1
      p.unwrap().getOrDefault("sessionId") == "abc123"

  test "parse multiple cookies":
    let p = parse("sessionId=abc123; userId=456def; theme=light")
    check:
      p.isOk()
      p.unwrap().len == 3
      p.unwrap().getOrDefault("sessionId") == "abc123"
      p.unwrap().getOrDefault("userId") == "456def"
      p.unwrap().getOrDefault("theme") == "light"

  test "cookies overwriting":
    let p = parse("sessionId=abc123; userId=456def; userId=19a198")
    check:
      p.isOk()
      p.unwrap().len == 2
      p.unwrap().getOrDefault("sessionId") == "abc123"
      p.unwrap().getOrDefault("userId") == "19a198"

  test "cookies are case SENSITIVE":
    let p = parse("sessionId=abc123; sessionid=456def")
    check:
      p.isOk()
      p.unwrap().len == 2
      p.unwrap().getOrDefault("sessionId") == "abc123"
      p.unwrap().getOrDefault("sessionid") == "456def"

  test "parse multiple cookies 2":
    let p = parse("user_name=JohnDoe; max_age=3600; path=/")
    check:
      p.isOk()
      p.unwrap().len == 3
      p.unwrap().getOrDefault("user_name") == "JohnDoe"
      p.unwrap().getOrDefault("max_age") == "3600"
      p.unwrap().getOrDefault("path") == "/"

  test "parse multiple cookies 3":
    let p = parse(
      "sessionToken=xyz789; userPrefs=darkMode; cartItems=1%2c2%2c3%2c4; language=en-US; appVersion=1.0.0"
    )
    check:
      p.isOk()
      p.unwrap().len == 5
      p.unwrap().getOrDefault("sessionToken") == "xyz789"
      p.unwrap().getOrDefault("userPrefs") == "darkMode"
      p.unwrap().getOrDefault("cartItems") == "1,2,3,4"
      p.unwrap().getOrDefault("language") == "en-US"
      p.unwrap().getOrDefault("appVersion") == "1.0.0"

  test "cookie with spaces":
    let p =
      parse("weaksauce=not%20so%20weaksauce; postels-law=\"Maybe not so acceptable\"")
    check:
      p.isOk()
      p.unwrap().len == 2
      p.unwrap().getOrDefault("weaksauce") == "not so weaksauce"
      p.unwrap().getOrDefault("postels-law") == "Maybe not so acceptable"

  test "optional whitespace between pairs":
    # RFC 6265 doesn't appear to accept this? I think? But enough
    # people do it so let's just take it anyway...
    let p = parse("a=b; c=d;e=f;g=h")
    check:
      p.isOk()
      p.unwrap().len == 4
      p.unwrap().getOrDefault("a") == "b"
      p.unwrap().getOrDefault("c") == "d"
      p.unwrap().getOrDefault("e") == "f"
      p.unwrap().getOrDefault("g") == "h"

  test "characters you didn't even know possible...":
    let p = parse("lol!=lmao~#; #$'**=-./1-.&{%20%20}`abc")
    check:
      p.isOk()
      p.unwrap().len == 2
      p.unwrap().getOrDefault("lol!") == "lmao~#"
      p.unwrap().getOrDefault("#$'**") == "-./1-.&{  }`abc"

suite "Malformed cookies":
  test "empty values":
    check:
      # sessionId
      parse("sessionId=; userId=456def; theme=light").isOk() == false
      # userId
      parse("sessionId=abc123; userId; theme=light").isOk() == false
      # user
      parse("user; theme=light").isOk() == false
      # cookie_name
      parse("cookie-name=; another_cookie=valid_value").isOk() == false
  test "empty keys":
    check:
      parse("=abc123; userId=456def").isOk() == false
      parse("sessionId=abc123; userId; theme=light").isOk() == false
      parse("user; theme=light").isOk() == false
  test "invalid characters":
    check:
      parse("test=1,2,3,4,5; bcde=1").isOk() == false
      parse("bcde=1;aaaa=1,2,3,4").isOk() == false
  test "unmatched quotes":
    check:
      parse("test=1,2,3,4,5; bcde=\"1").isOk() == false
  test "missing semicolon separator":
    check:
      parse("cookie-name-1=validValue1 cookie-name-2=validValue2").isOk() == false
  test "invalid cookie name":
    check:
      parse("userId=456def; theme=light; = ").isOk() == false
