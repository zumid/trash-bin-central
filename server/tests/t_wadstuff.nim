import src/process/analyze_wad
import src/util/datatypes
import std/paths
import unittest2
from stew/byteutils import toBytes

suite "Level numbers":
  test "00":
    check:
      decodeLevelNumber("00") == 0
  test "10":
    check:
      decodeLevelNumber("10") == 10
  test "99":
    check:
      decodeLevelNumber("99") == 99
  test "A0":
    check:
      decodeLevelNumber("A0") == 100
  test "A9":
    check:
      decodeLevelNumber("A9") == 109
  test "aa":
    check:
      decodeLevelNumber("aa") == 110
  test "Az":
    check:
      decodeLevelNumber("Az") == 135
  test "gZ":
    check:
      decodeLevelNumber("gZ") == 351
  test "ZQ":
    check:
      decodeLevelNumber("ZQ") == 1026
  test "zz":
    check:
      decodeLevelNumber("zz") == 1035
  test "123":
    check:
      decodeLevelNumber("123") == 123
  test "13":
    check:
      decodeLevelNumber("13") == 13
  test "1024":
    check:
      decodeLevelNumber("1024") == 1024

suite "Convert level select images":
  setup:
    const
      levSel1 = staticRead("../test_material/MAPS2P.png").toBytes()
      levSel2 = staticRead("../test_material/MAPS3P.lmp").toBytes()

  test "Convert png to png":
    let a = open("test_output/ex_maps2p.png", fmWrite)
    defer:
      a.close()
    let conv = levSel1.toPng()
    discard a.writeBytes(conv, 0, len(conv))

  test "Convert lmp to png":
    let a = open("test_output/ex_maps3p.png", fmWrite)
    defer:
      a.close()
    let conv = levSel2.toPng()
    discard a.writeBytes(conv, 0, len(conv))

suite "Verify magic numbers":
  setup:
    # File names
    let
      testWadFile = Path("~/wads/srb2/2.2/dimglaber.wad").expandTilde().string.open()
      testPk3File =
        Path("~/wads/srb2/2.2/OLDC_2024Round1-v1.pk3").expandTilde().string.open()
    # Buffers
    var
      wadFirstBuf = newSeq[byte](1000)
      pk3FirstBuf = newSeq[byte](1000)
    # Open buffers
    discard testWadFile.readBuffer(wadFirstBuf[0].addr, 1000)
    testWadFile.close()
    discard testPk3File.readBuffer(pk3FirstBuf[0].addr, 1000)
    testPk3File.close()
  test "WAD":
    let verifyResult = wadFirstBuf.verifyMagicNumber()
    check:
      verifyResult.isOk()
      verifyResult.unwrap() == Wad
  test "PK3":
    let verifyResult = pk3FirstBuf.verifyMagicNumber()
    check:
      verifyResult.isOk()
      verifyResult.unwrap() == Pk3

suite "Analyze files":
  setup:
    # File names
    let
      testWadFile = Path("~/wads/srb2/2.2/dimglaber.wad").expandTilde()
      testPk3File = Path("~/wads/srb2/2.2/OLDC_2024Round1-v1.pk3").expandTilde()
      testPk3File2 = Path("~/wads/srb2/2.2/srb2tp.pk3").expandTilde()
  test "WAD":
    let xx = analyzeFile(testWadFile, Wad)
    check:
      xx.isOk()
      xx.unwrap().levels.len() > 0
      xx.unwrap().levelPics.len() > 0
      # discounting title level and hidden levels
      xx.unwrap().levels.len() - 4 == xx.unwrap().levelPics.len()
  test "PK3":
    let xx = analyzeFile(testPk3File, Pk3)
    check:
      xx.isOk()
      xx.unwrap().levels.len() > 0
      xx.unwrap().levelPics.len() > 0
      # discounting title level
      xx.unwrap().levels.len() - 1 == xx.unwrap().levelPics.len()
  test "PK3-2":
    let xx = analyzeFile(testPk3File2, Pk3)
    check:
      xx.isOk()
    # echo xx.unwrap().levels.len()
    # echo xx.unwrap().levelPics.len()
