import unittest2
import patty
import src/util/datatypes
import std/paths
import src/util/resopt

from src/process/analyze_wad as analyze import nil
from src/process/patch_image import nil

proc saveImageAs(picture: Picture, newName: string) =
  # Save the image
  let exportName = Path("test_output") / Path(newName & ".png")
  var f = open(string(exportName), fmWrite)
  let c = patch_image.pictureToPng(picture)
  discard f.writeBytes(c, 0, len(c))
  f.close()

suite "Compose level select previews":
  setup:
    let
      test1File = Path("test_material/wadimage1.wad")
      test2File = Path("test_material/wadimage2.wad")
      test3File = Path("test_material/wadimage3.pk3")
      test4File = Path("test_material/wadimage4.pk3")
      test5File = Path("test_material/wadimage5.pk3")

    let
      test1Res = analyze.analyzeFile(test1File, Wad)
      test2Res = analyze.analyzeFile(test2File, Wad)
      test3Res = analyze.analyzeFile(test3File, Pk3)
      test4Res = analyze.analyzeFile(test4File, Pk3)
      test5Res = analyze.analyzeFile(test5File, Pk3)

    check:
      test1Res.isOk()
      test2Res.isOk()
      test3Res.isOk()
      test4Res.isOk()
      test5Res.isOk()

    let
      test1 = test1Res.unwrap()
      test2 = test2Res.unwrap()
      test3 = test3Res.unwrap()
      test4 = test4Res.unwrap()
      test5 = test5Res.unwrap()

  test "1 level image":
    let t1r = patch_image.createAddonPreview(test1.levelPics)
    check:
      t1r.isOk()
    match (t1r):
      Ok(pic):
        pic.saveImageAs("t1")
      Err:
        discard

  test "2 level images":
    let t2r = patch_image.createAddonPreview(test2.levelPics)
    check:
      t2r.isOk()
    match (t2r):
      Ok(pic):
        pic.saveImageAs("t2")
      Err:
        discard

  test "3 level images":
    let t3r = patch_image.createAddonPreview(test3.levelPics)
    check:
      t3r.isOk()
    match (t3r):
      Ok(pic):
        pic.saveImageAs("t3")
      Err:
        discard

  test "4 level images":
    let t4r = patch_image.createAddonPreview(test4.levelPics)
    check:
      t4r.isOk()
    match (t4r):
      Ok(pic):
        pic.saveImageAs("t4")
      Err:
        discard

  test "5 level images":
    let t5r = patch_image.createAddonPreview(test5.levelPics)
    check:
      t5r.isOk()
    match (t5r):
      Ok(pic):
        pic.saveImageAs("t5")
      Err:
        discard
