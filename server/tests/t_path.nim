import unittest2
import src/util/parse/http_path

suite "Basic parsing":
  test "root":
    let
      p = parse("/")
      expected: seq[string] = @[]
    check:
      p.isOk()
      p.unwrap().path == expected
      p.unwrap().query.len == 0

  test "no blank paths allowed":
    check:
      parse("").isOk() == false

  test "must have slash at the beginning":
    check:
      parse("something").isOk() == false

  test "should expand path":
    let p = parse("/test/1/2/abc")
    check:
      p.isOk()
      p.unwrap().path == @["test", "1", "2", "abc"]
      p.unwrap().query.len == 0

  test "should ignore multiple slashes":
    let p = parse("/lol/a//////b/c/////d////e/f")
    check:
      p.isOk()
      p.unwrap().path == @["lol", "a", "b", "c", "d", "e", "f"]
      p.unwrap().query.len == 0

  test "should parse uppercase hex":
    let p = parse("/article/%52%4F%43%4B%45%54")
    check:
      p.isOk()
      p.unwrap().path == @["article", "ROCKET"]
      p.unwrap().query.len == 0

  test "should parse lowercase hex":
    let p = parse("/article/%52%4f%43%4b%45%54")
    check:
      p.isOk()
      p.unwrap().path == @["article", "ROCKET"]
      p.unwrap().query.len == 0

  test "should ignore invalid hex characters":
    let p = parse("/article/%52%kc%43%4b%45%54")
    check:
      p.isOk()
      p.unwrap().path == @["article", "RcCKET"]
      p.unwrap().query.len == 0

  test "should ignore underspecified hex characters":
    let p = parse("/article/%52%kc%43%4b%45%54")
    check:
      p.isOk()
      p.unwrap().path == @["article", "RcCKET"]
      p.unwrap().query.len == 0
    check:
      parse("/article/%52%0%43%4b%45%54").unwrap().path == @["article", "RCKET"]

  test "should process spaces":
    let p = parse("/wiki/Main%20Page")
    check:
      p.isOk()
      p.unwrap().path == @["wiki", "Main Page"]
      p.unwrap().query.len == 0

  test "should NOT process ACTUAL spaces":
    let p = parse("/wiki/Main Page")
    check:
      p.isOk() == false

  test "should handle trailing slash":
    let p = parse("/blog/2024/06/03/1/")
    check:
      p.isOk()
      p.unwrap().path == @["blog", "2024", "06", "03", "1"]
      p.unwrap().query.len == 0

  test "should represent a file well":
    let p = parse("/assets/style.css")
    check:
      p.isOk()
      p.unwrap().path == @["assets", "style.css"]
      p.unwrap().query.len == 0

suite "Query parameters":
  test "basic get request":
    let p = parse("/watch?v=dQw4w9WgXcQ")
    check:
      p.isOk()
      p.unwrap().path == @["watch"]
      p.unwrap().query.len == 1
      p.unwrap().query.getOrDefault("v") == "dQw4w9WgXcQ"

  test "basic get request but with trailing slash":
    let p = parse("/watch/?v=dQw4w9WgXcQ")
    check:
      p.isOk()
      p.unwrap().path == @["watch"]
      p.unwrap().query.len == 1
      p.unwrap().query.getOrDefault("v") == "dQw4w9WgXcQ"

  test "basic get request but with trailing slash (2)":
    check:
      parse("/watch/?v=dQw4w9WgXcQ/").isOk() == false

  test "multiple queries":
    let p = parse("/watch?v=dQw4w9WgXcQ&t=104&hq=1")
    check:
      p.isOk()
      p.unwrap().path == @["watch"]
      p.unwrap().query.len == 3
      p.unwrap().query.getOrDefault("v") == "dQw4w9WgXcQ"
      p.unwrap().query.getOrDefault("t") == "104"
      p.unwrap().query.getOrDefault("hq") == "1"

  test "multiple queries but no arguments":
    let p = parse("/watch?v=&t=&hq=")
    check:
      p.isOk()
      p.unwrap().path == @["watch"]
      p.unwrap().query.len == 0
      p.unwrap().query.getOrDefault("v") == ""
      p.unwrap().query.getOrDefault("t") == ""
      p.unwrap().query.getOrDefault("hq") == ""

  test "multiple queries but nothing":
    let p = parse("/watch?v&t&hq")
    check:
      p.isOk()
      p.unwrap().path == @["watch"]
      p.unwrap().query.len == 0
      p.unwrap().query.getOrDefault("v") == ""
      p.unwrap().query.getOrDefault("t") == ""
      p.unwrap().query.getOrDefault("hq") == ""

  test "should alias multiple ? to &":
    let
      p = parse("/watch?v=dQw4w9WgXcQ?n=1")
      q = parse("/watch?v=dQw4w9WgXcQ&n=1")
    check:
      p.isOk()
      q.isOk()
      p.unwrap().path == @["watch"]
      p.unwrap().path == q.unwrap().path
      p.unwrap().query.len == 2
      p.unwrap().query.len == q.unwrap().query.len
      p.unwrap().query.getOrDefault("v") == "dQw4w9WgXcQ"
      p.unwrap().query.getOrDefault("n") == "1"
      p.unwrap().query.getOrDefault("v") == q.unwrap().query.getOrDefault("v")
      p.unwrap().query.getOrDefault("n") == q.unwrap().query.getOrDefault("n")

  test "should take the same hex stuff as before":
    let p = parse("/me/article?title=Test%20Track")
    check:
      p.isOk()
      p.unwrap().path == @["me", "article"]
      p.unwrap().query.len == 1
      p.unwrap().query.getOrDefault("title") == "Test Track"

  test "should accept spaces both in key and value":
    let p = parse("/me/article?article%20author=Mike%20Jones")
    check:
      p.isOk()
      p.unwrap().path == @["me", "article"]
      p.unwrap().query.len == 1
      p.unwrap().query.getOrDefault("article author") == "Mike Jones"

  test "should handle bruh moments":
    let p = parse("/watch?v=1234&t=5678&&&&&&&&&&&&&&&n=1")
    check:
      p.isOk()
      p.unwrap().path == @["watch"]
      p.unwrap().query.len == 3
      p.unwrap().query.getOrDefault("v") == "1234"
      p.unwrap().query.getOrDefault("t") == "5678"
      p.unwrap().query.getOrDefault("n") == "1"

  test "should handle bruh moments (2)":
    let p = parse("/watch?v=1234&t=5678&?&??&?&?&&?&?&?&&?&?&n=1")
    check:
      p.isOk()
      p.unwrap().path == @["watch"]
      p.unwrap().query.len == 3
      p.unwrap().query.getOrDefault("v") == "1234"
      p.unwrap().query.getOrDefault("t") == "5678"
      p.unwrap().query.getOrDefault("n") == "1"

  test "what happens if there's a question mark at the end?":
    let p = parse("/watch?")
    check:
      p.isOk()
      p.unwrap().path == @["watch"]
      p.unwrap().query.len == 0

  test "are null bytes a thing?":
    let p = parse("/path/%00/lel")
    check:
      p.isOk()
      p.unwrap().path == @["path", "\x00", "lel"]
      p.unwrap().query.len == 0

  test "non-escaped non-ascii":
    let p = parse("/path/\x11\x22\xff/\xea1245")
    check:
      p.isOk() == false

  test "the Funny path traversal 1":
    let p = parse("/say-cheese/%2e%2e/%2e%2e/%2e%2e/etc/passwd")
    check:
      p.isOk()
      p.unwrap().path == @["say-cheese", "..", "..", "..", "etc", "passwd"]
      p.unwrap().query.len == 0

  test "the Funny path traversal 2":
    let p = parse("/sneed/%25%32%65%25%32%65%25%32%65")
    check:
      p.isOk()
      # does not do double URI decoding
      p.unwrap().path == @["sneed", "%2e%2e%2e"]
      p.unwrap().query.len == 0

  test "the Funny path traversal 3":
    let p = parse("/..%2f..%2fetc%2fpasswd")
    check:
      p.isOk()
      p.unwrap().path == @["../../etc/passwd"]
      p.unwrap().query.len == 0

suite "Standalone queries":
  test "empty query":
    let p = parseParamsFrom("")
    check:
      p.isOk()
      len(p.unwrap()) == 0

  test "not-a-query":
    let p = parseParamsFrom("test")
    check:
      p.isOk()
      len(p.unwrap()) == 0

  test "a few queries":
    let p = parseParamsFrom("test=123&abc=xyz")
    check:
      p.isOk()
      len(p.unwrap()) == 2
      p.unwrap().getOrDefault("test") == "123"
      p.unwrap().getOrDefault("abc") == "xyz"

  test "overwritten values":
    let p = parseParamsFrom("test=123&abc=xyz&test=988")
    check:
      p.isOk()
      len(p.unwrap()) == 2
      p.unwrap().getOrDefault("test") == "988"
      p.unwrap().getOrDefault("abc") == "xyz"

  test "application/x-www-form-urlencoded":
    let p =
      parseParamsFrom("a+b+c=d+e+f&lolll=aaa+++ccc&actual+plus=lol+%2b++%2b%2b%2b+kek")
    check:
      p.isOk()
      len(p.unwrap()) == 3
      p.unwrap().getOrDefault("a b c") == "d e f"
      p.unwrap().getOrDefault("lolll") == "aaa   ccc"
      p.unwrap().getOrDefault("actual plus") == "lol +  +++ kek"
