import unittest2
import src/util/resopt
import src/util/dbmanager

suite "Plain":
  test "Basic case":
    let arr = @["Test", "A", "1", "12", "123"]
    let expected = """{"Test","A","1","12","123"}"""
    check arr.toPgDbArray() == expected

  test "Should escape quotes":
    let arr = @["Let's", "do", "something", "with", "\"quotes\""]
    let expected = """{"Let's","do","something","with","\"quotes\""}"""
    check arr.toPgDbArray() == expected

suite "With optionals":
  test "Basic case":
    let arr: seq[Opt[string]] =
      @[
        Opt[string].some("Test"),
        Opt[string].some("A"),
        Opt[string].some("C"),
        Opt[string].some("NULL"),
        Opt[string].none(),
        Opt[string].some("NULL"),
      ]
    let expected = """{"Test","A","C","NULL",NULL,"NULL"}"""
    check arr.toPgDbArray() == expected

  test "Should escape quotes":
    let arr: seq[Opt[string]] =
      @[
        Opt[string].some("Let's"),
        Opt[string].some("do"),
        Opt[string].some("something"),
        Opt[string].some("NULL"),
        Opt[string].some("or maybe"),
        Opt[string].none(),
        Opt[string].some("but with"),
        Opt[string].some("\"quotes\""),
      ]
    let expected =
      """{"Let's","do","something","NULL","or maybe",NULL,"but with","\"quotes\""}"""
    check arr.toPgDbArray() == expected
