import unittest2
import src/util/parse/http_ranges

suite "Basic cases":
  test "Full range":
    let l = rangeFromRequest("bytes=0-499")
    check:
      l.isOk()
      l.unwrap().partStart.isSome()
      l.unwrap().partStart.unwrap() == 0
      l.unwrap().partEnd.isSome()
      l.unwrap().partEnd.unwrap() == 499
  test "Start from":
    let l = rangeFromRequest("bytes=500-")
    check:
      l.isOk()
      l.unwrap().partStart.isSome()
      l.unwrap().partStart.unwrap() == 500
      not l.unwrap().partEnd.isSome()
  test "Last N":
    let l = rangeFromRequest("bytes=-500")
    check:
      l.isOk()
      not l.unwrap().partStart.isSome()
      l.unwrap().partEnd.isSome()
      l.unwrap().partEnd.unwrap() == 500
  test "One byte":
    let l = rangeFromRequest("bytes=300-300")
    check:
      l.isOk()
      l.unwrap().partStart.isSome()
      l.unwrap().partStart.unwrap() == 300
      l.unwrap().partEnd.isSome()
      l.unwrap().partEnd.unwrap() == 300
  test "Two bytes":
    let l = rangeFromRequest("bytes=300-301")
    check:
      l.isOk()
      l.unwrap().partStart.isSome()
      l.unwrap().partStart.unwrap() == 300
      l.unwrap().partEnd.isSome()
      l.unwrap().partEnd.unwrap() == 301
  test "Full range (extra spaces)":
    let l = rangeFromRequest(" bytes =     0-       499")
    check:
      l.isOk()
      l.unwrap().partStart.isSome()
      l.unwrap().partStart.unwrap() == 0
      l.unwrap().partEnd.isSome()
      l.unwrap().partEnd.unwrap() == 499
  test "Start from (extra spaces)":
    let l = rangeFromRequest("bytes   = 500 - ")
    check:
      l.isOk()
      l.unwrap().partStart.isSome()
      l.unwrap().partStart.unwrap() == 500
      not l.unwrap().partEnd.isSome()
  test "Last N (extra spaces)":
    let l = rangeFromRequest("bytes =        - 500")
    check:
      l.isOk()
      not l.unwrap().partStart.isSome()
      l.unwrap().partEnd.isSome()
      l.unwrap().partEnd.unwrap() == 500
  test "Invalid range":
    let l = rangeFromRequest("bytes=400-300")
    check:
      not l.isOk()
  test "Invalid unit":
    let l = rangeFromRequest("megabytes=400-300")
    check:
      not l.isOk()
  test "Empty range 1":
    let l = rangeFromRequest("bytes=-")
    check:
      not l.isOk()
  test "Empty range 2":
    let l = rangeFromRequest("bytes=")
    check:
      not l.isOk()
