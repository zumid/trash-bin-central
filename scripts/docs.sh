#!/bin/bash

#rm -rfv server/htmlDocs

pushd server && \
nimble makedocs && \
popd && \
\
python3 -m http.server 8199 -d server/htmlDocs
