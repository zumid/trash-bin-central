#!/bin/bash

. server/.env

rm -fv server/_previews/*
rm -fv server/_uploads/*
psql -f scripts/delete-all.sql "${USE_CONNSTRING}"

psql -f server/migrations/001-initial.sql "${USE_CONNSTRING}"
psql -f server/migrations/002-init-tags.sql "${USE_CONNSTRING}"
psql -f server/migrations/003-user-dateinfo_20250205.sql "${USE_CONNSTRING}"
