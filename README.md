# Trash Bin Central

This is an experiment into making a dedicated add-on site primarily for SRB2 and its
derivations like SRB2 Kart and Dr. Robotnik's Ring Racers.

The [SRB2 Message Board](https://mb.srb2.org) and [SRB2 Workshop](https://srb2workshop.org/)
are both websites based on XenForo, a commercial forum platform. The staff of both websites
have regularly run into roadblocks and inefficiencies in their attempts at hacking in an add-on
hosting platform to the software. Examples include having to use category-based system,
burdening the author to name their files correctly, and also this:

> Please be aware that all updates to addons will push them to the front of the Addons & More
> page, regardless of whether we or you perform the update. This is unavoidable due to how
> this platform functions.

Okay, that last bit may or may not happen here as well. But this is exactly the point of
this project—to build dedicated add-on platform software, where we're free to determine
exactly *how* everything works.

**EXPERIMENTAL, LIKELY NON-FUNCTIONAL AT THE MOMENT**

## Architecture

The website endpoints are prioritized at the moment.

The JSON API might be under `/api/json/v1/`.

LiquidMS-specific APIs might be under `/api/lqms/v1/`.

Details still TBA.

## Features (so far)

* Anon Add-on Create/Read
* Anon Add-on review Create/Read
* Anon Add-on review reply Create/Read
* Automatic thumbnails for add-ons
* Add-on thumbnail override (from file)
* Multi-version add-on support
* Add-on analysis, such as level information, number of custom sprites/SOCs/Lua/etc.
* Automatic file renaming based on add-on analysis (why rename it yourself when the file itself can tell you??)
* "Basic" search of addon name or level name (needs some work...)

## Stack/Requires

* PostgreSQL
* SQLite
* Nim
* libsodium.23
* esbuild

## Differences to v0.1

"v0.1" is a prior iteration of the project based on the [Prologue](https://planety.github.io/prologue/) framework.

* The whole hybrid phpBB forum system has been done away with. It's a cool (if not weird and dangerous)
  proof of concept, but it is actually more trouble than it's worth, practically speaking.
  Consequently, the whole "upload addon = new forum thread" thing is dropped. Instead, reviews are put
  directly on the addon info page.
* The framework has switched to [Guildenstern](https://github.com/olliNiinivaara/GuildenStern), which is a lot more barebones and necessitating a
  lot of self-inventing (which takes up time better spent on features of course), however offers
  streaming upload which at least prevents a DoS through ridiculously-sized files. Still, other ways
  of DoSing this thing is possible.
* MinIO isn't the knee-jerk storage solution, at least for now. This version uses plain file upload,
  however other storage methods like MinIO may be considered (as soon as I figure out how to make
  the API flexible enough...)
* Postgres (main) and SQLite (session) instead of MySQL. I understand that this kneecaps my chances
  of hosting it with a shared web hosting provider, but I have a "weird setup" already anyway. If
  I wanted that, I should have just picked PHP.
* Doing things anonymously is possible. User registration is still planned, however.
* Generating add-on thumbnails. Yes.

## Add-on Thumbnail Info

All information needed to create a thumbnail preview for the add-on—as in, the image
in the addon listings and such—are completely provided **inside of the add-on**.

Here's the gist of it copied from the upload page:

* Thumbnails are generated from up to 4 randomly-picked level select pictures.
* Thumbnails will not be generated if the add-on doesn't have any level select pictures.
* You can specify your own add-on thumbnail using a lump named TBCTHUMB.
* PNG is preferred as the format for TBCTHUMB.
* If TBCTHUMB is provided, automatic thumbnails will not be generated, TBCTHUMB will be used instead.
* Recommended to place the TBCTHUMB lump before any level select pictures, if you plan to have both.
