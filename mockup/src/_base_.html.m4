divert(-1)

changequote(«, »)

define(«BASE_TEMPLATE», «

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Home page - Trash Bin Central</title>
    <meta name="viewport" content="initial-width=device-width,initial-scale=1">
    <style type="text/css" media="screen,projection,tv">
        include(«screen.css»)
    </style>
</head>

<body>
    <div id="site-container">
        <header id="banner-container">
            <div>
                <h1><a href="index.html">Trash Bin Central</a> <span>BEYTAH</span></h1>
                <ul class="button-container">
                    <li><a href="log_in.html" class="button button_outline">Log in</a></li>
                    <li><a href="upload.html" class="button button_cta" id="upload-button">Upload</a></li>
                    <li class="hide"><a href="#main-menu" class="accessibility-tip">Skip to navigation</a></li>
                    <li class="hide"><a href="#main-content" class="accessibility-tip">Skip to content</a></li>
                </ul>
            </div>
            <nav id="banner-search-container">
                <hr>
                <a href="#advanced-search-form" id="advanced-search-button" class="button button_cta">Advanced Search</a>
                <form method="get" action="search.html" id="banner-search">
                    <fieldset>
                        <input type="text" placeholder="Search for Addons&hellip;"></input>
                    </fieldset>
                    <fieldset>
                        <input type="submit" value="Search"></input>
                    </fieldset>
                </form>
            </nav>
            <div id="login-indicator">
                <span>
                Logged in as <b>root</b>
                </span>
            </div>
        </header>

        <div id="main-container">
            <nav id="main-menu">
                <hr>
                <ul class="menu-button-container">
                    <!--li><a href="#" class="button">News</a></li-->
                    <li><a href="addon_list.html" class="button">Addons</a></li>
                    <li><a href="review_list.html" class="button">Reviews</a></li>
                    <!--li><a href="#" class="button">Level Contest</a></li-->
                </ul>
            </nav>
            <main id="main-content">
                <section id="toasts">
                    <hr>
                    <ul id="toast-contents">
                        <li>Test</li>
                        <li>Test 1</li>
                        <li>Test 2</li>
                    </ul>
                </section>

                $1
            </main>
        </div>

        <footer id="main-footer">
            <hr>
            <span>
                2025 Trash Bin Central.
                <br>
                This website is <a href="https://gitlab.com/zumid/trash-bin-central">reusable</a>.
            </span>
        </footer>
    </div>

    <form method="post" action="advanced_search.html" id="advanced-search-form">
        <input type="hidden" name="type" value="advanced-search">
        <div>
        <h2>Advanced Search</h2>
        <p>
            Any empty fields is taken to mean "any".
            <br>
            Press the back button <b>on your browser</b> to close this form.
        </p>
        <fieldset>
            <div>
            <label for="advs1">Name</label>
            <input type="text" id="advs1" name="advanced-search-term" placeholder="Green Hill Zone"></input>
            <select id="query-type" name="advanced-search-term-type">
                <option value="addon">Add-on</option>
                <option value="level">Level</option>
                <option value="addon-or-level">Both</option>
            </select>
            </div>

            <div>
            <label for="advs2">Author</label>
            <input type="text" id="advs2" name="advanced-search-author" placeholder="Foo McBarFace"></input>
            <label for="advs2">Only one author may be specified at a time</label>
            </div>

            <div>
            <label for="advs3">Tags</label>
            <input type="text" id="advs3" name="advanced-search-tags" placeholder="srb2.2, ctf, single"></input>
            <label for="advs3">Comma-separated</label>
            </div>

            <div>
            <label for="advs4">Results</label>
            <input type="text" inputmode="numeric" id="advs4" name="advanced-search-num-of-results" placeholder="10"></input>
            <label for="advs4">Number of results per page</label>
            </div>
        </fieldset>
        <fieldset>
            <input type="hidden" name="token" placeholder="CSRF"></input>
            <input type="submit"></input>
        </fieldset>
        </div>
    </form>
</body>

</html>

»)
