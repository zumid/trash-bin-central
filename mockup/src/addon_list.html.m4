divert(-1)

include(`_base_.html.m4')
include(«addon_blurb.partial.html.m4»)
include(«mini_addon_blurb.partial.html.m4»)

divert(0)

BASE_TEMPLATE(
«

<h2>Latest Add-ons</h2>

<div class="addon-showcase">
    MINI_ADDON_BLURB(
        «FRIGOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO»,
        «0.7»,
        «Sandwichface»
    )
    MINI_ADDON_BLURB(
        «Mid Simulator», «1.0», «Peak»
    )
    MINI_ADDON_BLURB(
        «Ran out of placeholders», «1.0», «Derp»
    )
    MINI_ADDON_BLURB(
        «Mid Simulator», «1.0», «Peak»
    )
    MINI_ADDON_BLURB(
        «Ran out of placeholders», «1.0», «Derp»
    )
    MINI_ADDON_BLURB(
        «Mid Simulator», «1.0», «Peak»
    )
    MINI_ADDON_BLURB(
        «Ran out of placeholders», «1.0», «Derp»
    )
    MINI_ADDON_BLURB(
        «Mid Simulator», «1.0», «Peak»
    )
    MINI_ADDON_BLURB(
        «Ran out of placeholders», «1.0», «Derp»
    )
</div>

<h2>Highest Rated Add-ons</h2>

<div class="addon-showcase">
    MINI_ADDON_BLURB(
        «FRIGOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO»,
        «0.7»,
        «Sandwichface»
    )
    MINI_ADDON_BLURB(
        «Mid Simulator», «1.0», «Peak»
    )
    MINI_ADDON_BLURB(
        «Ran out of placeholders», «1.0», «Derp»
    )
    MINI_ADDON_BLURB(
        «Mid Simulator», «1.0», «Peak»
    )
    MINI_ADDON_BLURB(
        «Ran out of placeholders», «1.0», «Derp»
    )
    MINI_ADDON_BLURB(
        «Mid Simulator», «1.0», «Peak»
    )
    MINI_ADDON_BLURB(
        «Ran out of placeholders», «1.0», «Derp»
    )
    MINI_ADDON_BLURB(
        «Mid Simulator», «1.0», «Peak»
    )
    MINI_ADDON_BLURB(
        «Ran out of placeholders», «1.0», «Derp»
    )
</div>

»)
