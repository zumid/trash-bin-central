divert(-1)

include(`_base_.html.m4')

divert(0)

BASE_TEMPLATE(
«
<div id="login-signup-container">
<div id="login">
<h2>Log in</h2>
<form method="post">
    <fieldset>
        <div>
        <label for="uname">User name</label>
        <input type="text" id="uname" name="mxnvun" />
        </div>

        <div>
        <label for="pword">Password</label>
        <input type="password" id="pword" name="xmnxpw" />
        </div>

        <div>
        <input type="checkbox" id="remem" name="remem" />
        <label for="remem">Keep me logged in until I log myself out</label>
        </div>
    </fieldset>
    <fieldset>
        <input type="hidden" name="csrf" value="{{ acsrf }}" />
        <input type="submit" value="Submit" />
    </fieldset>
</form>
<span>Don't have an account? Might want to <a href="#signup">register...</a></span>
</div>

<div id="signup">
<h2>Sign up</h2>
<form method="post">
    <fieldset>
        <div>
        <label for="uname">User name</label>
        <input type="text" id="uname" name="mxnvun" />
        </div>

        <div>
        <label for="pword">Password</label>
        <input type="password" id="pword" name="xmnxpw" />
        </div>

        <div>
        <label for="cpwd">Confirm password</label>
        <input type="password" id="cpwd" name="xmnxcpw" />
        </div>
    </fieldset>
    <fieldset>
        <input type="hidden" name="csrf" value="{{ acsrf }}" />
        <input type="submit" value="Submit" />
    </fieldset>
</form>
<span>Already have an account? <a href="#">Login</a> instead...</span>
</div>
</div>
»)
