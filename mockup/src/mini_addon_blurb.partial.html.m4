divert(-1)

define(«MINI_ADDON_BLURB», «

<article class="mini-addon-blurb">
	<h3>
		<a href="addon_detail.html"><span>$1</span></a> <span>$2</span> <br>
		<span>by <a href="#">$3</a></span>
	</h3>
	<figure>
		<img src="1.png" width="160" height="100" alt="Blue bricks">
		<figcaption>
		<dl>
			<div>
				<dt>Rating</dt>
				<dd>4.7 (10)</dd>
			</div>
			<div>
				<dt>Updated</dt>
				<dd>3mo</dd>
			</div>
		</dl>
		</figcaption>
	</figure>
</article>

»)
