divert(-1)

include(`_base_.html.m4')
include(«review.partial.html.m4»)

divert(0)

BASE_TEMPLATE(
«
<section id="addon-info">
    <header>
        <div>
            <h2>Oops, All Emerald Coast by <a href="#">Tracker</a></h2>
            
            <h3 class="hide">Synopsis</h3>
            <p class="synopsis">
                Totally your favorite level and not
                done like 4 times already…
            </p>
        </div>
        <div>
            <a href="/download/9f7843251a45526c" id="addon-download-button" class="button button_cta">Download (43 MiB)</a>
        </div>
    </header>
    
    <h3 class="hide">Tags</h3>
    <ul class="button-container">
        <li><a href="#" class="button button_tag">srb2-2.2</a></li>
        <li><a href="#" class="button button_tag">single</a></li>
        <li><a href="#" class="button button_tag">lua</a></li>
    </ul>

    <div id="addon-info-content">
        <div>
            <h3>Description</h3>
            <p>
                <i>No description available</i>
            </p>

            <h3>Readme</h3>
            <pre>
                I wrote this for hackers that like
                to snoopingas usual in WADs.

                Guess it's in Trash Bin now. Oops!
            </pre>

            <h3>Levels</h3>
            <ul class="proper-list">
                <li>Emerald Coast (2 acts)</li>
                <li>Emerald Coast Encore (1 act)</li>
            </ul>

            <h3>All Versions</h3>

            <div class="table-container">
            <table>
                <tr>
                    <th>Version</th>
                    <th>Release Date</th>
                    <th>Size</th>
                    <th>Download</th>
                </tr>
                <tr>
                    <td>v2.2.13</td>
                    <td><time datetime="2024-12-02T04:51:35+07:00">Mon. 2 Dec 2024</time></td>
                    <td>43.196MiB</td>
                    <td><a href="/download/9f7843251a45526c">Download</a></td>
                </tr>
                <tr>
                    <td>v2.2.11</td>
                    <td><time datetime="2024-12-02T04:51:35+07:00">Mon. 2 Dec 2024</time></td>
                    <td>43.196MiB</td>
                    <td><a href="/download/9f7843251a45526c">Download</a></td>
                </tr>
            </table>
            </div>
            <a href="addon_updates.html">View change log</a>
        </div>
        <div class="addon-infobox">
            <h3 class="hide">Add-on Info</h3>
            <img src="1.png" width="160" height="100" alt="Level preview icon" />
            <dl>
                <div>
                    <dt>Add-on of</dt>
                    <dd>SRB2 2.2</dd>
                </div>
                <div>
                    <dt>Format</dt>
                    <dd>PK3</dd>
                </div>
                <div>
                    <dt>Latest version</dt>
                    <dd>v2.2.13</dd>
                </div>
                <div>
                    <dt>Statistics</dt>
                    <dd>
                        <ul class="proper-list">
                            <li>Flats: 0</li>
                            <li>Textures: 57</li>
                            <li>Sprites: 50</li>
                            <li>SOCs: 1</li>
                            <li>Lua: 7</li>
                            <li>Maps: 3</li>
                            <li>GFXs: 0</li>
                            <li>Sounds: 0</li>
                            <li>Songs: 8</li>
                        </ul>
                    </dd>
                </div>
                <div>
                    <dt>First uploaded</dt>
                    <dd><time datetime="2024-12-02T04:51:35+07:00">Mon. 2 Dec 2024</time></dd>
                </div>
                <div>
                    <dt>Last updated</dt>
                    <dd><time datetime="2024-12-02T04:51:35+07:00">Mon. 2 Dec 2024</time></dd>
                </div>
                <div>
                    <dt>Rating</dt>
                    <dd>3.0 <small>(of 3 reviews)</small></dd>
                </div>
                <div>
                    <dt>Downloads</dt>
                    <dd>28</dd>
                </div>
            </dl>
        </div>
    </div>

    <h3>Reviews</h3>

    <form method="post" id="add-review-form">
        <input type="hidden" name="type" value="review">
        <fieldset id="review-box-container">
            <label for="review-box">
                Add yours (min. 10 words):
            </label>
            <br>
            <textarea
                id="review-box"
                name="review-content"
                placeholder="Say something about this addon..."
            ></textarea>
        </fieldset>
        <div id="rating-and-submit-container">
            <fieldset>
                <legend>Rating</legend>
                <div id="review-form-rating-container">
                    <input type="radio" id="r1" name="review-rating" value="1">
                    <label for="r1">1 star</label>
                    <input type="radio" id="r2" name="review-rating" value="2">
                    <label for="r2">2 stars</label>
                    <input type="radio" id="r3" name="review-rating" value="3">
                    <label for="r3">3 stars</label>
                    <input type="radio" id="r4" name="review-rating" value="4">
                    <label for="r4">4 stars</label>
                    <input type="radio" id="r5" name="review-rating" value="5">
                    <label for="r5">5 stars</label>
                    <div id="review-form-rating"></div>
                </div>
            </fieldset>
            <fieldset>
                <input type="hidden" name="token" placeholder="CSRF">
                <input type="submit" value="Send">
            </fieldset>
        </div>
    </form>
    
    REVIEW(
        «Anonymous», «2024-12-05»,
        «1»,
        «This mod sucks ass, boring ahh adventure port...»,
        «
            REVIEW_REPLY(
                «Tracker», «2024-12-06»,
                «ok YOU suck then»
            )
        »
    )
    
    REVIEW(
        «Anonymous», «2024-12-01»,
        «5»,
        «I love this mod date me pls»,
        «
            REVIEW_REPLY(
                Tracker, 2024-12-02,
                uhh wtf????
            )
            REVIEW_REPLY(
                Anonymous, 2024-12-02,
                pls????
            )
        »
    )

    REVIEW(
        «Anonymous», «2024-11-30»,
        «3»,
        «
            Not good, but not bad either.
            Do keep up the work though!
        »,
        «»
    )

    <h3>Administrative</h3>

    <ul class="button-container">
        <li><a href="addon_edit.html" class="button">Edit this addon</a></li>
        <li><a href="addon_add_version.html" class="button">Upload a new version</a></li>
    </ul>
</section>
»)
