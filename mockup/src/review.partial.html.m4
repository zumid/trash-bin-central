divert(-1)

define(«REVIEW», «

<article class="review">
    <header>
        <h4>
            <a href="#1">
                <span class="review_author">$1</span>
                <span>&mdash;</span>
                <time datetime="$2">$2</time>
            </a>
            <span class="review_rating" style="--rating: $3">&mdash; $3 stars</span>
        </h4>
    </header>
    <div>
        <p>
            $4
        </p>
    </div>

    <details>
        <summary>Reply to this message...</summary>
        <form method="post" class="review-reply-form">
            <input type="hidden" name="type" value="reply">
            <input type="hidden" name="reply-to" value="ID">
            <fieldset>
                <textarea name="reply-content"></textarea>
            </fieldset>
            <fieldset>
                <input type="hidden" name="token" placeholder="CSRF">
                <input type="submit" value="Reply">
            </fieldset>
        </form>
    </details>

    $5
</article>

»)

define(«REVIEW_REPLY», «

<article class="review review_reply">
    <header>
        <h4>
            <a href="#1">
                <span class="review_author">$1</span>
                <span>&mdash;</span>
                <time datetime="$2">$2</time>
            </a>
        </h4>
    </header>
    <div>
        <p>
            $3
        </p>
    </div>
</article>

»)

define(«REVIEW_NO_REPLY», «

<article class="review">
    <header>
        <h4>
            <a href="#1">
                <span class="review_author">$1</span> on [ADDON NAME]
                <span>&mdash;</span>
                <time datetime="$2">$2</time>
            </a>
            <span class="review_rating" style="--rating: $3">&mdash; $3 stars</span>
        </h4>
    </header>
    <div>
        <p>
            $4
        </p>
    </div>
</article>

»)