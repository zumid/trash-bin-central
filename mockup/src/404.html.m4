divert(-1)

include(`_base_.html.m4')

divert(0)

BASE_TEMPLATE(
«
<section id="error-page">

<h2>Page not found</h2>

<p>
    The specified page is not found
</p>

</section>
»)
