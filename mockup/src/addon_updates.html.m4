divert(-1)

include(`_base_.html.m4')

divert(0)

BASE_TEMPLATE(
«
<h2>Add-on Updates</h2>

<article>
    <header>
        <h3><a href="#">v2.2.13</a></h3>
    </header>
    <div>
    <p>Our double whammy is now a triple whammy. Guess we needed one more release candidate, but we were just so excited to launch that we forgot. Here’s an update that makes netgames work correctly again, plus some bonus bug fixes and optimizations.</p><ul class="proper-list">
<li><strong>Fixed renderhitbox causing netgame instability</strong></li><li>Fixed floorsprite rendering causing a possible crash if the sprite had a size of 1×1</li><li>Fixed hitbox interpolation when scaling mobjs</li><li>Optimized sprite rendering in the software renderer</li><li>Exposed skin.supername to Lua</li></ul><p>More details about this changelog are on our <a href="https://git.do.srb2.org/STJr/SRB2/-/wikis/Changelogs/2.2.13">GitLab</a>. Grab the update from <a href="https://srb2.org/download" data-type="URL" data-id="https://srb2.org/download">the usual place</a>. See you soon with the REAL conclusion!</p>
    </div>
</article>

<article>
    <header>
        <h3><a href="#">v2.2.11</a></h3>
    </header>
    <div>
    <p>Hello everybody! We’re happy to inform you that another fantastic SRB2 update has landed on your doorstep. Here are the key changes in SRB2 v2.2.11:</p><ul class="proper-list">
<li>Removed the framerate limit: Using interpolation, SRB2 can now run at far more than 35fps, allowing for smoother looking 60fps gameplay (or much higher on supported monitors). You can set a custom limit at the bottom of Video Options.</li><li>Fixed error 503: This prevented some players from accessing the ingame Master Server browser. If you’ve been having trouble using the Master Server, give it another try.</li><li>Support larger characters in ERZ: Addon characters with large collision boxes, such as <a rel="noreferrer noopener" href="https://mb.srb2.org/addons/ryder-the-falcon-the-rock-solid-dark-destroyer.3596/" data-type="URL" data-id="https://mb.srb2.org/addons/ryder-the-falcon-the-rock-solid-dark-destroyer.3596/" target="_blank">Ryder the Falcon</a>, can now complete ERZ. We introduced some new gravity-related elements you can add to your maps as a part of this fix. Read about them <a href="https://git.do.srb2.org/STJr/SRB2/-/merge_requests/1808" data-type="URL" data-id="https://git.do.srb2.org/STJr/SRB2/-/merge_requests/1808" target="_blank" rel="noreferrer noopener">here</a>.</li><li>Added support for the Universal Doom Map Format (UDMF): UDMF is a more flexible and powerful map format that allows for easier editing and more complex level designs. This long-awaited feature is a game-changer for mappers, opening up new possibilities for custom levels.<ul class="wp-block-list">
<li>The implementation isn’t set in stone. We intend to make changes to it based on mappers’ feedback if necessary.</li><li>The UDMF map editor is not complete. Until it is complete, you can find the latest build pinned in #udmf on <a rel="noreferrer noopener" href="https://srb2.org/discord" data-type="URL" data-id="https://srb2.org/discord" target="_blank">our Discord server</a>. </li><li><a rel="noreferrer noopener" href="https://wiki.srb2.org/wiki/User:MascaraSnake/UDMF#Can_I_convert_existing_binary_maps_to_UDMF?" target="_blank">You can convert your existing map to the new format</a>, but older levels should continue to work as normal (let us know if they don’t!)</li></ul><ul class="wp-block-list">
<li>UDMF maps are allowed to be submitted to the <a rel="noreferrer noopener" href="https://mb.srb2.org/threads/official-level-design-collab-rules-deadlines-revised-4-19-2023.31365/" target="_blank">OLDC</a>!</li></ul>
</li><li>Better rendering in large levels: Levels that used to break the renderer by the sheer size of their rooms should be far more visually stable now.</li><li>Improved Lua command/cvar safety and added basic rate limitation for Lua file access: Helps prevent addons from writing too much data to hard disk, among other things.</li></ul><p>There are plenty of other bug fixes and other improvements. Check out <a rel="noreferrer noopener" href="https://git.do.srb2.org/STJr/SRB2/-/wikis/Changelogs/2.2.11" data-type="URL" data-id="https://git.do.srb2.org/STJr/SRB2/-/wikis/Changelogs/2.2.11" target="_blank">the changelog</a> for a complete list.</p><p>We appreciate your continued support and feedback as we work on our favorite project. You would probably appreciate heading over to our <a href="https://www.srb2.org/download/">downloads page</a> to get the new patch and start playing. Enjoy!</p>
    </div>
</article>
»)
