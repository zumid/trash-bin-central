divert(-1)

include(`_base_.html.m4')

divert(0)

BASE_TEMPLATE(
«
<h2>Edit add-on metadata</h2>
<form method="post" id="upload-addon-form" enctype="multipart/form-data">
<fieldset>
    <legend>Basic information</legend>

    <div>
    <label for="title">Add-on name:</label>
    <input type="text" id="title" name="title" placeholder="Scrimblo McBrimblo Adventure" value="">
    <label for="title">
        Title of your add-on. Required.
    </label>
    </div>

    <div>
    <label for="author">Author:</label>
    <input type="text" id="author" name="author" placeholder="Your name Here" value="">
    <label for="author">
        Who created the add-on. Required, if you are not signed in.
    </label>
    </div>

    <div>
    <label for="compat">This add-on is for…</label>
    <select name="compat" id="compat">
        <optgroup label="Sonic Robo Blast 2">
        <option value="srb2-2.2">SRB2 2.2.x</option>
        <option value="srb2-2.1">SRB2 2.1.x</option>
        <option value="srb2-2.0">SRB2 2.0.x</option>
        <option value="srb2-1.09">SRB2 1.09.x</option>
        <option value="srb2-1.07">SRB2 1.07</option>
        <option value="srb2-1.01-1.04">SRB2 1.01–1.04</option>
        <option value="srb2-demo4">SRB2 Demo 4</option>
        <option value="srb2-demo3">SRB2 Demo 3</option>
        <option value="srb2-demo2">SRB2 Demo 2</option>
        <option value="srb2-demo1">SRB2 Demo 1</option>
        </optgroup>
        <optgroup label="Sonic Robo Blast 2 Kart">
        <option value="drrr-2.0">Dr Robotnik's Ring Racers 2.x</option>
        <option value="srb2k-1.0">SRB2K 1.x</option>
        </optgroup>
        <optgroup label="Cross">
            <option value="cross">SRB2 + SRB2K (X will automatically be appended)</option>
        </optgroup>
    </select>
    <label for="compat">
        The intended game this add-on is for. Required.
    </label>
    </div>
    
    <div>
    <label for="synopsis">Synopsis:</label>
    <input type="text" id="synopsis" name="synopsis" placeholder="Some kinda mod" value="">
    <label for="synopsis">
        A short description of your add-on. Required.
    </label>
    </div>

    <div>
    <label for="desc">Description</label>
    <textarea name="desc" id="desc" placeholder="ABC"></textarea>
    <label for="desc">
        A full description of your add-on.
    </label>
    </div>
</fieldset>

<fieldset id="upload-addon-form-confirm">
    <legend>Upload</legend>
    <div>
    <input type="checkbox" name="agree" id="agree">
    <label for="agree">I understand the <a href="guidelines.html" target="_blank">submission guidelines</a></label>
    </div>
    <div>
    <label for="edit-code">Edit code:</label>
    <input type="text" id="edit-code" name="edit-code" placeholder="woeijfoiweijfi" value="">
    </div>
    <div>
    <input type="hidden" name="csrf" value="WJp-wMyKRqQb!Mb0pnbhzDU-s1N0Zk5i34QkV+bz$U1+pM2iP+H!1SC.BwzXat~Q">
    <input type="submit" value="Submit">
    </div>
</fieldset>
</form>
»)
