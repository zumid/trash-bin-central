divert(-1)

include(`_base_.html.m4')

divert(0)

BASE_TEMPLATE(
«
<h2>Upload a release</h2>
<form method="post" id="upload-addon-form" enctype="multipart/form-data">
<fieldset>
    <legend>File information</legend>

    <div>
    <label for="mainfile">File</label>
    <input type="file" id="mainfile" name="main-file">
    <label for="mainfile">
        The file to upload. Required.
    </label>
    </div>

    <div>
    <label for="vers">Version:</label>
    <input type="text" id="vers" name="vers" placeholder="1.0" value="">
    <label for="vers">
        The version number of the add-on you're uploading. Required.
    </label>
    </div>

    <div>
    <label for="changelog">Release notes:</label>
    <textarea name="changelog" id="changelog" placeholder="ABC"></textarea>
    <label for="changelog">
        Things you want to say (if any).
    </label>
    </div>
</fieldset>

<fieldset id="upload-addon-form-confirm">
    <legend>Upload</legend>
    <div>
    <input type="checkbox" name="agree" id="agree">
    <label for="agree">I understand the <a href="guidelines.html" target="_blank">submission guidelines</a></label>
    </div>
    <div>
    <label for="edit-code">Edit code:</label>
    <input type="text" id="edit-code" name="edit-code" placeholder="woeijfoiweijfi" value="">
    </div>
    <div>
    <input type="hidden" name="csrf" value="WJp-wMyKRqQb!Mb0pnbhzDU-s1N0Zk5i34QkV+bz$U1+pM2iP+H!1SC.BwzXat~Q">
    <input type="submit" value="Submit">
    </div>
</fieldset>
</form>
»)
